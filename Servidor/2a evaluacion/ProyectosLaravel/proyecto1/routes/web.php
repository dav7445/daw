<?php

use App\Http\Controllers\CalculaController;
use App\Http\Controllers\GraficasController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CalculaController::class, 'indice']);

Route::get('/calculadora', [CalculaController::class, 'calculadoraFormulario']);

Route::post('/calculadora', [CalculaController::class, 'calculadora'])->name('calculadoraAlias');

Route::get('/subirybajar', [GraficasController::class, 'subirybajarFormulario']);

Route::post('/subirybajar', [GraficasController::class, 'subirybajar'])->name('subirybajarAlias');

Route::get('/moverrecta', [GraficasController::class, 'moverrectaFormulario']);

Route::post('/moverrecta', [GraficasController::class, 'moverrecta'])->name('moverrectaAlias');

Route::get('/movercuadrado', [GraficasController::class, 'movercuadradoFormulario']);

Route::post('/movercuadrado', [GraficasController::class, 'movercuadrado'])->name('movercuadradoAlias');

Route::get('/votar', [GraficasController::class, 'votarFormulario']);

Route::post('/votar', [GraficasController::class, 'votar'])->name('votarAlias');

/*Route::get('/suma', [CalculaController::class, 'sumaFormulario'])->name('sumaFormularioAlias');

Route::post('/suma', [CalculaController::class, 'suma'])->name('sumaAlias');

Route::get('/suma/{valor1}/{valor2}', [CalculaController::class, 'suma']);

Route::get('/resta/{valor1}/{valor2}', [CalculaController::class, 'resta'])->name('restaAlias');

Route::get('/multiplicacion/{valor1}/{valor2}', [CalculaController::class, 'multiplicacion'])->name('multiplicacionAlias');

Route::get('/division/{valor1}/{valor2}', [CalculaController::class, 'division'])->name('divisionAlias');*/
