

<?php $__env->startSection('title'); ?>
    Subir y bajar
<?php $__env->stopSection(); ?>

<?php $__env->startSection('operacion'); ?>
    <form action="<?php echo e(route('subirybajarAlias')); ?>" method="post">
        <?php echo csrf_field(); ?>
        <button type="submit" name="accion" value="bajar">-</button>
        <span><?php echo e(session('numero')); ?></span>
        <button type="submit" name="accion" value="subir">+</button>
        <br><button type="submit" name="accion" value="cero">Cero</button>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Program Files\xampp\htdocs\daw\Servidor\2a evaluacion\ProyectosLaravel\proyecto1\resources\views/graficas/subirybajar.blade.php ENDPATH**/ ?>