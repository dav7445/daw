

<?php $__env->startSection('title'); ?>
    Mover punto en cuadrado
<?php $__env->stopSection(); ?>

<?php $__env->startSection('operacion'); ?>
    <h1>Mover un punto en dos dimensiones</h1>
    <form action="<?php echo e(route('movercuadradoAlias')); ?>" method="post">
        <?php echo csrf_field(); ?>
        <p>Haga clic en los botones para mover el punto:</p>
        <table>
            <tr>
                <td>
                    <table style="float: left">
                        <tr>
                            <th style="width:70px"><button type="submit" name="accion" value="arribaizquierda"
                                    style="font-size: 60px; line-height: 60px;transform: rotate(-45deg)">&#x1F446;</button>
                            </th>

                            <th style="width:70px"><button type="submit" name="accion" value="arriba"
                                    style="font-size: 60px; line-height: 60px;">&#x1F446;</button></th>

                            <th style="width:70px"><button type="submit" name="accion" value="arribaderecha"
                                    style="font-size: 60px; line-height: 60px;transform: rotate(45deg)">&#x1F446;</button>
                            </th>
                        </tr>
                        <tr>
                            <th><button type="submit" name="accion" value="izquierda"
                                    style="font-size: 60px; line-height: 60px;">&#x1F448;</button></th>
                            <th><button type="submit" name="accion" value="centro">Volver al<br>centro</button></th>
                            <th><button type="submit" name="accion" value="derecha"
                                    style="font-size: 60px; line-height: 60px;">&#x1F449;</button></th>
                        </tr>
                        <tr>
                            <th><button type="submit" name="accion" value="abajoizquierda"
                                    style="font-size: 60px; line-height: 60px;transform: rotate(45deg)">&#x1F447;</button>
                            </th>

                            <th><button type="submit" name="accion" value="abajo"
                                    style="font-size: 60px; line-height: 60px;">&#x1F447;</button></th>

                            <th><button type="submit" name="accion" value="abajoderecha"
                                    style="font-size: 60px; line-height: 60px;transform: rotate(-45deg)">&#x1F447;</button>
                            </th>
                        </tr>
                    </table>
                </td>
                <td>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="400" height="400"
                        viewbox="-200 -200 400 400" style="border: black 2px solid">
                        <circle cx="<?php echo e(session("x")); ?>" cy="<?php echo e(session("y")); ?>" r="8" fill="red" />';
                    </svg>
                </td>
            </tr>
        </table>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Program Files\xampp\htdocs\daw\Servidor\2a evaluacion\ProyectosLaravel\proyecto1\resources\views/graficas/movercuadrado.blade.php ENDPATH**/ ?>