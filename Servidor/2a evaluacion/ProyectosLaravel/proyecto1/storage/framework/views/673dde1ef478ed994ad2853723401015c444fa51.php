

<?php $__env->startSection('title'); ?>
    Division
<?php $__env->stopSection(); ?>

<?php $__env->startSection('operacion'); ?>
    <?php if($v2 == 0): ?>
        El divisor no puede ser 0
    <?php else: ?>
        La division de <?php echo e($v1); ?> / <?php echo e($v2); ?> es <?php echo e(($v1 / $v2)); ?>

    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\daw\Servidor\2a evaluacion\ProyectosLaravel\proyecto1\resources\views/operaciones/division.blade.php ENDPATH**/ ?>