

<?php $__env->startSection('title'); ?>
    Suma
<?php $__env->stopSection(); ?>

<?php $__env->startSection('operacion'); ?>
    <form action="<?php echo e(route('sumaAlias')); ?>" method="post">
        <?php echo csrf_field(); ?>
        <input type="number" name="num1">
        <input type="number" name="num2">
        <button type="submit" name="enviar">Enviar</button>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\daw\Servidor\2a evaluacion\ProyectosLaravel\proyecto1\resources\views/operaciones/sumaFormulario.blade.php ENDPATH**/ ?>