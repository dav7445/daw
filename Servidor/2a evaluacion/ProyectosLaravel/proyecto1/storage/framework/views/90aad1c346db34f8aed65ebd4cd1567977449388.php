

<?php $__env->startSection('title'); ?>
    Mover punto
<?php $__env->stopSection(); ?>

<?php $__env->startSection('operacion'); ?>
    <h1>Mover un punto a derecha e izquierda</h1>
    <form action="<?php echo e(route('moverrectaAlias')); ?>" method="post">
        <?php echo csrf_field(); ?>
        <p>Haga clic en los botones para mover el punto:</p>
        <table>
            <tr>
                <th>
                    <button type="submit" name="accion" value="izquierda"
                        style="font-size: 60px; line-height: 40px;">&#x261C;</button>
                    <button type="submit" name="accion" value="derecha"
                        style="font-size: 60px; line-height: 40px;">&#x261E;</button>
                <th>
            </tr>
            <tr>
                <th>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="600" height="20" viewbox="-300 0 600 20">
                        <line x1="-300" y1="10" x2="300" y2="10" stroke="black" stroke-width="5" />
                        <circle cx="<?php echo e(session('posicion')); ?>" cy="10" r="8" fill="red" />';
                    </svg>
                </th>
            </tr>
        </table>
        <p>
            <button type="submit" name="accion" value="centro">Volver al centro</button>
        </p>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Program Files\xampp\htdocs\daw\Servidor\2a evaluacion\ProyectosLaravel\proyecto1\resources\views/graficas/moverrecta.blade.php ENDPATH**/ ?>