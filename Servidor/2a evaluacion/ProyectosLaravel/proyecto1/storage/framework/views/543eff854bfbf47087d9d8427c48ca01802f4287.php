

<?php $__env->startSection('title'); ?>
    Calculadora
<?php $__env->stopSection(); ?>

<?php $__env->startSection('operacion'); ?>
    <form action="<?php echo e(route('calculadoraAlias')); ?>" method="post">
        <?php echo csrf_field(); ?>
        <input type="number" name="num1">
        <input type="number" name="num2">
        <br />
        <button type="submit" name="operacion" value="suma">Suma</button>
        <button type="submit" name="operacion" value="resta">Resta</button>
        <button type="submit" name="operacion" value="multiplicacion">Multiplicacion</button>
        <button type="submit" name="operacion" value="division">Division</button>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.base', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Program Files\xampp\htdocs\daw\Servidor\2a evaluacion\ProyectosLaravel\proyecto1\resources\views/operaciones/calculadora.blade.php ENDPATH**/ ?>