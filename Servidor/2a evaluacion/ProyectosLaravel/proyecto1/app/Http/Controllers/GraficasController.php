<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GraficasController extends Controller
{
    public function subirybajarFormulario()
    {
        if (session()->missing('numero')) {
            session()->put('numero', 0);
        }
        return view('graficas.subirybajar');
    }

    public function subirybajar(Request $request)
    {
        $accion = $request->accion;

        if ($accion == "cero") {
            session()->put('numero', 0);
        } elseif ($accion == "subir") {
            $request->session()->increment('numero');
        } elseif (session("numero") == 0) {
            return view('graficas.subirybajar');
        } elseif ($accion == "bajar") {
            $request->session()->decrement('numero');
        }

        return view('graficas.subirybajar');
    }

    //----------------------------------------------------------------------------------

    public function moverrectaFormulario()
    {
        if (session()->missing('posicion')) {
            session()->put('posicion', 0);
        }
        return view('graficas.moverrecta');
    }

    public function moverrecta(Request $request)
    {
        $accion = $request->accion;

        // Dependiendo de la acción recibida, modificamos el número guardado
        if ($accion == "centro") {
            session()->put('posicion', 0);
        } elseif ($accion == "izquierda") {
            session()->decrement('posicion', 20);
        } elseif ($accion == "derecha") {
            session()->increment('posicion', 20);
        }

        // Si el punto sale por un lado, entra por el otro
        if (session('posicion') > 300) {
            session()->put('posicion', -300);
        } elseif (session('posicion') < -300) {
            session()->put('posicion', 300);
        }

        return view('graficas.moverrecta');
    }

    //----------------------------------------------------------------------------------

    public function movercuadradoFormulario()
    {
        if (session()->missing('x') || session()->missing('y')) {
            session()->put("x", 0);
            session()->put("y", 0);
        }
        return view('graficas.movercuadrado');
    }

    public function movercuadrado(Request $request)
    {
        $accion = $request->accion;

        // Dependiendo de la acción recibida, modificamos el número guardado
        if ($accion == "centro") {
            session()->put("x", 0);
            session()->put("y", 0);
        }
        if ($accion == "izquierda" || $accion == "arribaizquierda" || $accion == "abajoizquierda") {
            session()->decrement('x', 20);
        }
        if ($accion == "derecha" || $accion == "arribaderecha" || $accion == "abajoderecha") {
            session()->increment('x', 20);
        }
        if ($accion == "arriba" || $accion == "arribaizquierda" || $accion == "arribaderecha") {
            session()->decrement('y', 20);
        }
        if ($accion == "abajo" || $accion == "abajoizquierda" || $accion == "abajoderecha") {
            session()->increment('y', 20);
        }

        // Si el punto sale por la izquierda o la derecha, entra por el otro lado
        if (session("x") > 200) {
            session()->put("x", -200);
        } elseif (session("x") < -200) {
            session()->put("x", 200);
        }

        // Si el punto sale por arriba o por abajo, entra por el otro lado
        if (session("y") > 200) {
            session()->put("y", -200);
        } elseif (session("y") < -200) {
            session()->put("y", 200);
        }

        return view('graficas.movercuadrado');
    }

    //----------------------------------------------------------------------------------

    public function votarFormulario()
    {
        if (session()->missing('a') || session()->missing('b')) {
            session()->put("a", 0);
            session()->put("b", 0);
        }
        return view('graficas.votar');
    }

    public function votar(Request $request)
    {
        $accion = $request->accion;

        // Dependiendo de la acción recibida, modificamos el número correspondiente
        if ($accion == "a") {
            session()->increment('a', 10);
        } elseif ($accion == "b") {
            session()->increment('b', 10);
        } elseif ($accion == "cero") {
            session()->put("a", 0);
            session()->put("b", 0);
        }

        return view('graficas.votar');
    }
}
