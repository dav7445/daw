<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalculaController extends Controller
{
    public function indice() {
        return view('operaciones.indice');
    }

    public function calculadoraFormulario() {
        return view('operaciones.calculadora');
    }

    public function calculadora(Request $request) {
        $v1 = $request->num1;
        $v2 = $request->num2;
        $operacion = $request->operacion;

        if ($operacion === "suma") {
            $operador = "+";
            $resultado = $v1 + $v2;

        } else if ($operacion === "resta") {
            if ($v2 > $v1) {
                return "$v2 es mayor que $v1";
            }
            $operador = "-";
            $resultado = $v1 - $v2;

        } else if ($operacion === "multiplicacion") {
            $operador = "*";
            $resultado = $v1 * $v2;

        } else if ($operacion === "division") {
            if ($v2 == 0) {
                return "Divisor no puede ser 0";
            }
            $operador = "/";
            $resultado = $v1 / $v2;
        }

        return view('operaciones.resultado',compact("operacion", "v1", "v2", "operador", "resultado"));
    }

    public function sumaFormulario() {
        return view('operaciones.sumaFormulario');
    }

    /*public function suma(Request $request) {
        $v1 = $request->num1;
        $v2 = $request->num2;
        $resultado = $v1 + $v2;
        return view('operaciones.suma',compact("v1", "v2", "resultado"));
    }

    public function resta($v1, $v2) {
        $resultado = $v1 - $v2;
        return view('operaciones.resta',compact("v1", "v2", "resultado"));
    }

    public function multiplicacion($v1, $v2) {
        $resultado = $v1 * $v2;
        return view('operaciones.multiplicacion',compact("v1", "v2", "resultado"));
    }

    public function division($v1, $v2) {
        return view('operaciones.division',compact("v1", "v2"));
    }*/
}
