@extends('layouts.base')

@section('title')
    Votar
@endsection

@section('operacion')
    <h1>Votar una opción</h1>
    <form action="{{ route('votarAlias') }}" method="post">
        @csrf
        <p>Haga clic en los botones para votar por una opción:</p>
        <table>
            <tr>
                <td style="vertical-align: top;"><button type="submit" name="accion" value="a"
                        style="font-size: 60px; line-height: 50px; color: hsl(200, 100%, 50%);">&#x2714;</button></td>
                <?php
                // Dibujamos la primera barra
                echo '<td>';
                echo '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="' . session('a') . '" height="50">';
                echo '<line x1="0" y1="25" x2="' . session('a') . '" y2="25" stroke="hsl(200, 100%, 50%)" stroke-width="50" />';
                echo '</svg>';
                echo '</td>';
                ?>

            </tr>
            <tr>
                <td><button type="submit" name="accion" value="b"
                        style="font-size: 60px; line-height: 50px; color: hsl(35, 100%, 50%)">&#x2714;</button></td>
                <?php
                // Dibujamos la segunda barra
                echo '<td>';
                echo '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="' . session('b') . '" height="50">';
                echo '<line x1="0" y1="25" x2="' . session('b') . '" y2="25" stroke="hsl(35, 100%, 50%)" stroke-width="50" />';
                echo '</svg>';
                echo '</td>';
                ?>
            </tr>
        </table>
        <p>
            <button type="submit" name="accion" value="cero">Poner a cero</button>
        </p>
    </form>
@endsection
