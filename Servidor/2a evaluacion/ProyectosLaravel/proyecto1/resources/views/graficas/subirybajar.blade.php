@extends('layouts.base')

@section('title')
    Subir y bajar
@endsection

@section('operacion')
    <form action="{{ route('subirybajarAlias') }}" method="post">
        @csrf
        <button type="submit" name="accion" value="bajar">-</button>
        <span>{{ session('numero') }}</span>
        <button type="submit" name="accion" value="subir">+</button>
        <br><button type="submit" name="accion" value="cero">Cero</button>
    </form>
@endsection
