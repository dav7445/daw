@extends('layouts.base')

@section('title')
    Division
@endsection

@section('operacion')
    @if ($v2 == 0)
        El divisor no puede ser 0
    @else
        La division de {{$v1}} / {{$v2}} es {{($v1 / $v2)}}
    @endif
@endsection
