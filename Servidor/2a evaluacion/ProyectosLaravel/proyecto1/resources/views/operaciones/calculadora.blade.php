@extends('layouts.base')

@section('title')
    Calculadora
@endsection

@section('operacion')
    <form action="{{route('calculadoraAlias')}}" method="post">
        @csrf
        <input type="number" name="num1">
        <input type="number" name="num2">
        <br />
        <button type="submit" name="operacion" value="suma">Suma</button>
        <button type="submit" name="operacion" value="resta">Resta</button>
        <button type="submit" name="operacion" value="multiplicacion">Multiplicacion</button>
        <button type="submit" name="operacion" value="division">Division</button>
    </form>
@endsection
