@extends('layouts.base')

@section('title')
    Resta
@endsection

@section('operacion')
    La resta de {{$v1}} - {{$v2}} es {{$resultado}}
@endsection
