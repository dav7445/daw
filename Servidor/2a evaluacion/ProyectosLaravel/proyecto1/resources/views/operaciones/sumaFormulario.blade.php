@extends('layouts.base')

@section('title')
    Suma
@endsection

@section('operacion')
    <form action="{{route('sumaAlias')}}" method="post">
        @csrf
        <input type="number" name="num1">
        <input type="number" name="num2">
        <button type="submit" name="enviar">Enviar</button>
    </form>
@endsection
