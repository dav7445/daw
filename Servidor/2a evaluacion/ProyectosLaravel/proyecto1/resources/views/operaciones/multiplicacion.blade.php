@extends('layouts.base')

@section('title')
    Multiplicacion
@endsection

@section('operacion')
    La multiplicacion de {{$v1}} x {{$v2}} es {{$resultado}}
@endsection
