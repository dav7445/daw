@extends('layouts.base')

@section('title')
    {{$operador}}
@endsection

@section('operacion')
    La {{$operacion}} de {{$v1}} {{$operador}} {{$v2}} es {{$resultado}}
@endsection
