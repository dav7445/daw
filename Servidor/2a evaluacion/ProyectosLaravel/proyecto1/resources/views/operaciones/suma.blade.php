@extends('layouts.base')

@section('title')
    Suma
@endsection

@section('operacion')
    La suma de {{$v1}} + {{$v2}} es {{$resultado}}
@endsection
