<nav class="navbar navbar-default">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo e(url('/home')); ?>">
                <span class="glyphicon glyphicon-tower" aria-hidden="true"></span>
                Videoclub
            </a>
        </div>


        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <?php if(Auth::check()): ?>
                <ul class="nav navbar-nav">
                    <li <?php echo e(Request::is('catalog*') && !Request::is('catalog/create') ? ' class=active' : ''); ?>>
                        <a href="<?php echo e(url('/catalog')); ?>">
                            <span class="glyphicon glyphicon-film" aria-hidden="true"></span>
                            Catálogo
                        </a>
                    </li>
                    <li <?php echo e(Request::is('catalog/create') ? ' class=active' : ''); ?>>
                        <a href="<?php echo e(url('/catalog/create')); ?>">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Nueva película
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="dropdown-item" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                            Cerrar sesión
                        </a>

                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                            <?php echo csrf_field(); ?>
                        </form>
                    </li>
                </ul>
            <?php elseif(Route::has('login')): ?>
                <ul class="nav navbar-nav navbar-right">
                    <li <?php echo e(Request::is('login') ? ' class=active' : ''); ?>>
                        <a href="<?php echo e(route('login')); ?>">
                            <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
                            Conectar
                        </a>
                    </li>
                    <?php if(Route::has('register')): ?>
                        <li <?php echo e(Request::is('register') ? ' class=active' : ''); ?>>
                            <a href="<?php echo e(route('register')); ?>">
                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                Registro
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</nav>
<?php /**PATH C:\Program Files\xampp\htdocs\daw\Servidor\2a evaluacion\ProyectosLaravel\videoclub-c\videoclub\resources\views/partials/navbar.blade.php ENDPATH**/ ?>