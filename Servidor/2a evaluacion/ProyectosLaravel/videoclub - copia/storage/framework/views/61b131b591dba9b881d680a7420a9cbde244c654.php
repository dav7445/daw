
<?php $__env->startSection('title','Videoclub'); ?>
<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <h1>Bienvenido/a al videoclub</h1>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><?php echo e(__('')); ?></div>

                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>

                    <?php echo e(__('¡Conectate!')); ?>

                </div>
                <form action="<?php echo e(route('auth.login')); ?>" method="GET">
                    <button type="submit" class="btn btn-success mx-3" style="width: 50%;">Iniciar sesion</button>
                </form>
                <br/>
                <h3>Si no estas registrado, puedes hacerlo aqui</h3>
                
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\videoclub\proyecto1\resources\views/inicio.blade.php ENDPATH**/ ?>