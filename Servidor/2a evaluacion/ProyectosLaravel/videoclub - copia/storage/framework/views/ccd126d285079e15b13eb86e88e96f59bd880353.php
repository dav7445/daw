<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $__env->yieldContent('title'); ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo e(url('/assets/bootstrap/css/bootstrap.min.css')); ?>">
  </head>
  <body>
    <?php echo $__env->make('partials.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

	<div class="container">
      <?php echo $__env->yieldContent('content'); ?>
      
    </div>
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php echo e(url('/assets/bootstrap/js/bootstrap.min.js')); ?>"></script>
  </body>
</html><?php /**PATH C:\xampp\htdocs\videoclub\proyecto1\resources\views/layouts/master.blade.php ENDPATH**/ ?>