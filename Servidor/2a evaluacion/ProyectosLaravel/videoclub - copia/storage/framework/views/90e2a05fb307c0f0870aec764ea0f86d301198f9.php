
<?php $__env->startSection('title','Mostrar pelicula'); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-sm-4">
        <img src="<?php echo $pelicula->poster; ?>" alt="Poster de <?php echo $pelicula->title; ?>" height="500">
    </div>
    <div class="col-sm-8">
        <h2><?php echo $pelicula->title . " (" . $pelicula->year . ")"; ?></h2>
        <p>Dirigida por <strong><?php echo $pelicula->director; ?></strong></p>
        <p><?php echo $pelicula->synopsis; ?></p>
            <form action="<?php echo e(url('catalog/edit/'.$pelicula->id)); ?>" method="GET">
                <?php echo csrf_field(); ?>
                <button type="submit" class="btn btn-warning mx-3" style="width: 50%;"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Editar película</button>
            </form>
            <br/>
            <form action="<?php echo e(url('catalog/delete/'.$pelicula->id)); ?>" method="POST">
                <?php echo csrf_field(); ?>
                <?php echo method_field('delete'); ?>
                <button type="submit" class="btn btn-danger mx-3" style="width: 50%;"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Eliminar pelicula</button>
            </form>
            <br/>
            <form action="<?php echo e(url('catalog')); ?>" method="GET">
                <?php echo csrf_field(); ?>
                <button type="submit" class="btn btn-primary mx-3" style="width: 50%;"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Volver al catálogo</button>
            </form>
            <br/>
        <form action="<?php echo e(route('catalog.alquiladevuelve', $pelicula)); ?>" method="POST">
            <?php echo csrf_field(); ?>
            <?php echo method_field('put'); ?>
            <?php if ($pelicula->rented == 0) { ?>
                <button type="submit" class="btn btn-success" style="width: 50%;"><span class="glyphicon glyphicon-download" aria-hidden="false"></span> Alquilar pelicula</button>
            <?php } else { ?>
                <button type="submit" class="btn btn-danger" style="width: 50%;"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span> Devolver pelicula</button>
            <?php } ?>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\videoclub\proyecto1\resources\views/catalog/show.blade.php ENDPATH**/ ?>