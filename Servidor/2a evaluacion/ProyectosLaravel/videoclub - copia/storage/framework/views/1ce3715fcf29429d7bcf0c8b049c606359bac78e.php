
<?php $__env->startSection('title','Modificar pelicula'); ?>
    
<?php $__env->startSection('content'); ?>

<form action="<?php echo e(route('catalog.update', $pelicula)); ?>" method="POST" style="width: 50%; margin: 0px auto;">
    <?php echo csrf_field(); ?>
    <?php echo method_field('put'); ?>
    <h2>Modificar "<i><?php echo $pelicula->title; ?></i>"</h2>
    <div class="form-group">
        <label for="title">Título</label>
        <input type="text" name="title" id="title" class="form-control" placeholder="Título película" required value="<?php echo e($pelicula->title); ?>">
    </div>
    <div class="form-group">
        <label for="year">Año</label>
        <input type="number" name="year" id="year" max="2020" class="form-control" placeholder="Año de estreno" required value="<?php echo e($pelicula->year); ?>">
    </div>
    <div class="form-group">
        <label for="director">Director</label>
        <input type="text" name="director" id="director" class="form-control" placeholder="Nombre del director" required value="<?php echo e($pelicula->director); ?>">
    </div>
    <div class="form-group">
        <label for="poster">Poster</label>
        <input type="url" name="poster" id="poster" class="form-control" placeholder="Pega la url del poster" required value="<?php echo e($pelicula->poster); ?>">
    </div>
    <div class="form-group">
        <label for="synopsis">Sinópsis</label>
        <textarea name="synopsis" id="synopsis" class="form-control" rows="5" style="resize: none;" required><?php echo e($pelicula->synopsis); ?></textarea>
    </div>
    <button type="submit" class="btn btn-success">Actualizar película</button>
    <a href="<?php echo e(url('catalog/show/'.$pelicula->id)); ?>" class="btn btn-danger">Cancelar</a>
</form>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\videoclub\proyecto1\resources\views/catalog/edit.blade.php ENDPATH**/ ?>