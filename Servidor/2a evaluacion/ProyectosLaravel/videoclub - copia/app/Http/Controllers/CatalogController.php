<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;

class CatalogController extends Controller
{

    public function getIndex()
    {
        $peliculas = Movie::all();
        return view('catalog.index', compact('peliculas'));
    }

    public function getShow(Movie $pelicula)
    {
        return view('catalog.show', compact('pelicula'));
    }

    public function getEdit(Movie $pelicula)
    {
        return view('catalog.edit', compact('pelicula'));
    }

    public function getCreate()
    {
        return view('catalog.create');
    }

    public function store(Request $request)
    {
        $p = new Movie;
        $p->title = $request['title'];
        $p->year = $request['year'];
        $p->director = $request['director'];
        $p->poster = $request['poster'];
        $p->rented = 0;
        $p->synopsis = $request['synopsis'];
        $p->save();
        return redirect()->route('catalog.getIndex');
    }

    public function update(Request $request, Movie $pelicula)
    {
        $pelicula->title = $request['title'];
        $pelicula->year = $request['year'];
        $pelicula->director = $request['director'];
        $pelicula->poster = $request['poster'];
        $pelicula->synopsis = $request['synopsis'];
        $pelicula->save();
        return redirect()->route('catalog.getShow', compact('pelicula'));
    }

    public function alquiladevuelve(Movie $pelicula) {
        if ($pelicula->rented==0) {
            $pelicula->rented = 1;
        } else {
            $pelicula->rented = 0;
        }
        $pelicula->save();
        return redirect()->route('catalog.getShow', compact('pelicula'));
    }

    public function destroy(Movie $pelicula)
    {
        $pelicula->delete();
        return redirect()->route('catalog.getIndex');
    }

}
