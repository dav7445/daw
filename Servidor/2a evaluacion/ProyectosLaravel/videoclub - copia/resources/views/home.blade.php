@extends('layouts.master')
@section('title', 'Videoclub')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <h1>Bienvenido/a al videoclub</h1>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Menu') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ __('¡Estas conectado al videoclub!') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
