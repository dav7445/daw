<nav class="navbar navbar-default">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/home') }}">
                <span class="glyphicon glyphicon-tower" aria-hidden="true"></span>
                Videoclub
            </a>
        </div>


        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            @if (Auth::check())
                <ul class="nav navbar-nav">
                    <li {{ Request::is('catalog*') && !Request::is('catalog/create') ? ' class=active' : '' }}>
                        <a href="{{ url('/catalog') }}">
                            <span class="glyphicon glyphicon-film" aria-hidden="true"></span>
                            Catálogo
                        </a>
                    </li>
                    <li {{ Request::is('catalog/create') ? ' class=active' : '' }}>
                        <a href="{{ url('/catalog/create') }}">
                            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                            Nueva película
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                            Cerrar sesión
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                </ul>
            @elseif (Route::has('login'))
                <ul class="nav navbar-nav navbar-right">
                    <li {{ Request::is('login') ? ' class=active' : '' }}>
                        <a href="{{ route('login') }}">
                            <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
                            Conectar
                        </a>
                    </li>
                    @if (Route::has('register'))
                        <li {{ Request::is('register') ? ' class=active' : '' }}>
                            <a href="{{ route('register') }}">
                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                Registro
                            </a>
                        </li>
                    @endif
                </ul>
            @endif
        </div>
    </div>
</nav>
