@extends('layouts.master')
@section('title', 'Crear pelicula')
@section('content')
    <h2>Añadir pelicula</h2>
    <form action="{{ route('catalog.store') }}" method="POST" style="width: 50%; margin: auto;">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="form-floating mb-3 col-lg-12">
            <label for="floatingInput">Título</label>
            <input type="text" name="title" id="title" class="form-control" placeholder="Título película" required>
        </div>

        <div class="form-floating mb-3 col-lg-12">
            <label for="floatingInput">Año</label>
            <input type="number" name="year" id="year" max="2020" class="form-control" placeholder="Año de estreno"
                required>
        </div>

        <div class="form-floating mb-3 col-lg-12">
            <label for="floatingInput">Director</label>
            <input type="text" name="director" id="director" class="form-control" placeholder="Nombre del director"
                required>
        </div>

        <div class="form-floating mb-3 col-lg-12">
            <label for="floatingInput">Poster</label>
            <input type="url" name="poster" id="poster" class="form-control" placeholder="Pega la url del poster" required>
        </div>

        <div class="form-floating mb-3 col-lg-12">
            <label for="floatingInput">Sinópsis</label>
            <textarea name="synopsis" id="synopsis" class="form-control" cols="30" rows="10" style="resize: none;"
                required></textarea>
        </div>

        @csrf
        <div class="form-floating mb-3 col-lg-12 text-center">
            <br />
            <button type="submit" class="btn btn-success">Crear película</button>
        </div>
    </form>
@endsection
