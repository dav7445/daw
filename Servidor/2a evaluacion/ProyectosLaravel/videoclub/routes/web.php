<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\CatalogController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::controller(HomeController::class)->group(function () {
    Route::get('/', 'getHome')->name('getHome');

    Route::get('login', 'getlogin')->name('auth.getlogin');

    Route::post('logout', 'getHome');
});

Route::controller(CatalogController::class)->group(function () {
    Route::get('catalog', 'getIndex')->name('catalog.getIndex');

    Route::get('search', 'search')->name('catalog.search');

    Route::get('catalog/estrenos', 'estrenos')->name('catalog.estrenos');

    Route::get('catalog/alquiladas', 'alquiladas')->name('catalog.alquiladas');

    Route::get('catalog/create', 'getCreate')->name('catalog.getCreate');
    Route::post('catalog/create', 'store')->name('catalog.store');

    Route::get('catalog/show/{pelicula}', 'getShow')->name('catalog.getShow');
    Route::put('catalog/show/{pelicula}', 'alquiladevuelve')->name('catalog.alquiladevuelve');

    Route::get('catalog/edit/{pelicula}', 'getEdit')->name('catalog.getEdit');

    Route::put('catalog/{pelicula}', 'update')->name('catalog.update');

    Route::delete('catalog/delete/{pelicula}', 'destroy')->name('catalog.destroy');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'getHome'])->name('home');
