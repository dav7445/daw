<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;

class CatalogController extends Controller
{
    public function getIndex()
    {
        $peliculas = Movie::all();
        return view('catalog.index', compact('peliculas'))->with('encabezado', 'Listado de peliculas');
    }

    public function search(Request $request)
    {
        $peliculas = Movie::where('title', 'like', '%' . $request['title'] . '%')->get();
        return view('catalog.index', compact('peliculas'))->with('encabezado', 'Busqueda: "' . $request['title'] . '"');
    }

    public function estrenos(Request $request)
    {
        $peliculas = Movie::where('year', '>=', date('Y', strtotime('-1 year')))->get();
        return view('catalog.index', compact('peliculas'))->with('encabezado', 'Últimos lanzamientos');
    }

    public function alquiladas(Request $request)
    {
        $peliculas = Movie::where('rented', '=', 1)->get();
        return view('catalog.index', compact('peliculas'))->with('encabezado', 'Peliculas alquiladas');
    }

    public function getShow(Movie $pelicula)
    {
        return view('catalog.show', compact('pelicula'));
    }

    public function getEdit(Movie $pelicula)
    {
        return view('catalog.edit', compact('pelicula'));
    }

    public function getCreate()
    {
        return view('catalog.create');
    }

    public function store(Request $request)
    {
        $p = new Movie;
        $p->title = $request['title'];
        $p->year = $request['year'];
        $p->director = $request['director'];
        $p->poster = $request['poster'];
        $p->rented = 0;
        $p->synopsis = $request['synopsis'];
        $p->save();
        return redirect()->route('catalog.getIndex');
    }

    public function update(Request $request, Movie $pelicula)
    {
        $pelicula->title = $request['title'];
        $pelicula->year = $request['year'];
        $pelicula->director = $request['director'];
        $pelicula->poster = $request['poster'];
        $pelicula->synopsis = $request['synopsis'];
        $pelicula->save();
        return redirect()->route('catalog.getShow', compact('pelicula'));
    }

    public function alquiladevuelve(Movie $pelicula)
    {
        $pelicula->rented = !$pelicula->rented;
        $pelicula->save();
        return redirect()->route('catalog.getShow', compact('pelicula'));
    }

    public function destroy(Movie $pelicula)
    {
        $pelicula->delete();
        return redirect()->route('catalog.getIndex');
    }
}
