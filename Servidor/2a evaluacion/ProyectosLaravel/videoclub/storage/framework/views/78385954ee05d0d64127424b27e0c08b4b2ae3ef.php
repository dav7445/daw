
<?php $__env->startSection('title', $pelicula->title); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-sm-4">
            <img src="<?php echo e($pelicula->poster); ?>" alt="Poster de <?php echo e($pelicula->title); ?>" class="img-fluid img-thumbnail w-100">
        </div>
        <div class="col-sm-8">
            <h2 class="display-5"><?php echo e($pelicula->title . ' (' . $pelicula->year . ')'); ?></h2>

            <p class="lead">Dirigida por <strong><?php echo e($pelicula->director); ?></strong></p>
            <p><?php echo e($pelicula->synopsis); ?></p>

            <form action="<?php echo e(route('catalog.alquiladevuelve', $pelicula)); ?>" method="POST" class="mb-3">
                <?php echo csrf_field(); ?>
                <?php echo method_field('put'); ?>
                <?php if($pelicula->rented == 0): ?>
                    <button type="submit" class="btn btn-success btn-lg col-12 col-md-auto"><i
                            class="bi bi-cart-plus-fill"></i> Alquilar</button>
                <?php else: ?>
                    <button type="submit" class="btn btn-danger btn-lg col-12 col-md-auto"><i
                            class="bi bi-cart-dash-fill"></i> Devolver</button>
                <?php endif; ?>
            </form>

            <a href="<?php echo e(route('catalog.getEdit', $pelicula)); ?>" class="btn btn-primary col-12 col-md-auto mb-3"><i
                    class="bi bi-pencil-square"></i> Editar</a>

            <form action="<?php echo e(route('catalog.destroy', $pelicula)); ?>" method="POST" class="mb-3">
                <?php echo csrf_field(); ?>
                <?php echo method_field('delete'); ?>
                <button type="submit" class="btn btn-danger col-12 col-md-auto"><i class="bi bi-trash-fill"></i>
                    Eliminar</button>
            </form>

            <a href="<?php echo e(route('catalog.getIndex')); ?>" class="btn btn-outline-secondary col-12 col-md-auto mb-3"><i
                    class="bi bi-arrow-left"></i>
                Volver al listado</a>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\daw\Servidor\2a evaluacion\ProyectosLaravel\videoclub\resources\views/catalog/show.blade.php ENDPATH**/ ?>