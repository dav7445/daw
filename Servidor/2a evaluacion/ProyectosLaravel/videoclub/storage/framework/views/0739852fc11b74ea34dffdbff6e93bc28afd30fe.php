
<?php $__env->startSection('title', 'Catalogo'); ?>

<?php $__env->startSection('content'); ?>
    <?php if(isset($encabezado)): ?>
        <h1 class="display-5 mb-3">
            <?php echo e($encabezado); ?>

        </h1>
    <?php endif; ?>
    <?php if($peliculas->isEmpty()): ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <i class="bi bi-exclamation-triangle-fill"></i> No se encontraron peliculas
        </div>
    <?php else: ?>
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-xl-4 g-3">
            <?php $__currentLoopData = $peliculas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pelicula): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col">
                    <a href="<?php echo e(url('/catalog/show/' . $pelicula->id)); ?>" class="text-decoration-none">

                        <div class="card shadow-sm h-100">

                            <img src="<?php echo e($pelicula->poster); ?>" class="d-block w-100 card-img-top">

                            <div class="card-body d-flex flex-column">
                                <h5 class="card-title text-black"><?php echo e($pelicula->title); ?> (<?php echo e($pelicula->year); ?>)</h5>
                                <p class="card-text text-truncate text-secondary"><?php echo e($pelicula->synopsis); ?></p>
                            </div>

                        </div>
                    </a>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\daw\Servidor\2a evaluacion\ProyectosLaravel\videoclub\resources\views/catalog/index.blade.php ENDPATH**/ ?>