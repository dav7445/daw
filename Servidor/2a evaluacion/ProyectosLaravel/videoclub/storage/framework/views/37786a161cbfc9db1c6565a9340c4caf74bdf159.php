<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $__env->yieldContent('title' , 'Videoclub'); ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('/assets/bootstrap/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('/assets/bootstrap/css/bootstrap-icons.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('/assets/style.css')); ?>" />
</head>

<body>
    <?php echo $__env->make('partials.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="container mb-3">
        <?php echo $__env->yieldContent('content'); ?>
    </div>

    <script src="<?php echo e(url('/assets/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
</body>

</html>
<?php /**PATH C:\xampp\htdocs\daw\Servidor\2a evaluacion\ProyectosLaravel\videoclub\resources\views/layouts/master.blade.php ENDPATH**/ ?>