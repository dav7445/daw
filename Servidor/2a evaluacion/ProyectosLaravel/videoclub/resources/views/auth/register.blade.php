@extends('layouts.master')
@section('title', 'Registrarse')

@section('content')
    <div class="row align-items-center g-lg-5 py-5">
        <div class="col-lg-7 text-center text-lg-start">
            <h1 class="display-4 fw-bold lh-1 mb-3">Videoclub</h1>
            <p class="col-lg-10 fs-4">Las mejores peliculas de la historia a tu alcance</p>
        </div>
        <div class="col-md-10 mx-auto col-lg-5">
            <form action="{{ route('register') }}" method="POST"
                class="p-4 p-md-5 mb-2 border rounded-3 bg-light needs-validation" novalidate="">
                @csrf
                <div class="row gy-3">
                    <div class="col-12">
                        <label for="name" class="form-label">Nombre</label>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                            value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-12">
                        <label for="email" class="form-label">Email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" required autocomplete="email">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-12">
                        <label for="password" class="form-label">Contraseña</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                            name="password" required autocomplete="new-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-12">
                        <label for="password-confirm" class="form-label">Repetir Contraseña</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                            required autocomplete="new-password">
                    </div>

                    <div class="col-12">
                        <button class="w-100 btn btn-lg btn-primary" type="submit">Registrarse</button>
                    </div>
                </div>
            </form>
            <p class="text-center">¿Ya tienes una cuenta? <a href="{{ route('login') }}"
                    class="text-decoration-none">Inicia sesión</a></p>
        </div>
    </div>
@endsection
