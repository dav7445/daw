@extends('layouts.master')
@section('title', 'Iniciar Sesion')

@section('content')
    <div class="row align-items-center g-lg-5 py-5">
        <div class="col-lg-7 text-center text-lg-start">
            <h1 class="display-4 fw-bold lh-1 mb-3">Videoclub</h1>
            <p class="col-lg-10 fs-4">Las mejores peliculas de la historia a tu alcance</p>
        </div>
        <div class="col-md-10 mx-auto col-lg-5">
            <form action="{{ route('login') }}" method="POST"
                class="p-4 p-md-5 mb-2 border rounded-3 bg-light needs-validation" novalidate="">
                @csrf
                <div class="row gy-3">
                    <div class="col-12">
                        <label for="email" class="form-label">Email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                            value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-12">
                        <label for="password" class="form-label">Contraseña</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                            name="password" required autocomplete="current-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="checkbox mb-2 col-12 col-md-6">
                                <label><input type="checkbox" name="remember" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}> Recuerdame</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <button class="w-100 btn btn-lg btn-primary" type="submit">Iniciar Sesión</button>
                    </div>
                </div>
            </form>
            <p class="text-center">¿No tienes una cuenta? <a href="{{ route('register') }}"
                    class="text-decoration-none">Registrate</a></p>
        </div>
    </div>
@endsection
