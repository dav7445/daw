<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
    <div class="container">
        <a class="navbar-brand" href="{{ route('getHome') }}">Videoclub</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar"
            aria-controls="navbar" aria-expanded="false">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar">
            @if (Auth::check())
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link{{ Request::is('catalog') && !Request::is('catalog/create') ? ' active' : '' }}"
                            href="{{ route('catalog.getIndex') }}">
                            <i class="bi bi-film"></i> Catálogo</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ Request::is('catalog/create') ? ' active' : '' }}"
                            href="{{ route('catalog.getCreate') }}">
                            <i class="bi bi-plus-circle"></i> Nueva Película</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ Request::is('catalog/alquiladas') ? ' active' : '' }}"
                            href="{{ route('catalog.alquiladas') }}">
                            <i class="bi bi-cart-fill"></i> Alquiladas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link{{ Request::is('catalog/estrenos') ? ' active' : '' }}"
                            href="{{ route('catalog.estrenos') }}">
                            <i class="bi bi-star-fill"></i> Estrenos</a>
                    </li>
                </ul>
                <form action="{{ route('catalog.search') }}" class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
                    <input type="search" class="form-control form-control-dark" placeholder="Buscar..."
                        aria-label="Search" name="title">
                </form>
                <form action="{{ route('logout') }}" method="post">
                    @csrf
                    <button class="btn btn-outline-secondary">Cerrar sesión</button>
                </form>
            @else
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('login') ? ' active' : '' }}"
                            href="{{ route('login') }}">
                            <i class="bi bi-person"></i> Iniciar sesión
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('register') ? ' active' : '' }}"
                            href="{{ route('register') }}">
                            <i class="bi bi-person-plus"></i> Registrarse
                        </a>
                    </li>
                </ul>
            @endif
        </div>
    </div>
</nav>
