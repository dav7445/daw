<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title' , 'Videoclub')</title>
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/bootstrap/css/bootstrap-icons.css') }}" />
    <link rel="stylesheet" href="{{ url('/assets/style.css') }}" />
</head>

<body>
    @include('partials.navbar')

    <div class="container mb-3">
        @yield('content')
    </div>

    <script src="{{ url('/assets/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
</body>

</html>
