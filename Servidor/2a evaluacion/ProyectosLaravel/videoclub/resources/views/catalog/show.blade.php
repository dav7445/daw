@extends('layouts.master')
@section('title', $pelicula->title)

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <img src="{{ $pelicula->poster }}" alt="Poster de {{ $pelicula->title }}" class="img-fluid img-thumbnail w-100">
        </div>
        <div class="col-sm-8">
            <h2 class="display-5">{{ $pelicula->title . ' (' . $pelicula->year . ')' }}</h2>

            <p class="lead">Dirigida por <strong>{{ $pelicula->director }}</strong></p>
            <p>{{ $pelicula->synopsis }}</p>

            <form action="{{ route('catalog.alquiladevuelve', $pelicula) }}" method="POST" class="mb-3">
                @csrf
                @method('put')
                @if ($pelicula->rented == 0)
                    <button type="submit" class="btn btn-success btn-lg col-12 col-md-auto"><i
                            class="bi bi-cart-plus-fill"></i> Alquilar</button>
                @else
                    <button type="submit" class="btn btn-danger btn-lg col-12 col-md-auto"><i
                            class="bi bi-cart-dash-fill"></i> Devolver</button>
                @endif
            </form>

            <a href="{{ route('catalog.getEdit', $pelicula) }}" class="btn btn-primary col-12 col-md-auto mb-3"><i
                    class="bi bi-pencil-square"></i> Editar</a>

            <form action="{{ route('catalog.destroy', $pelicula) }}" method="POST" class="mb-3">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger col-12 col-md-auto"><i class="bi bi-trash-fill"></i>
                    Eliminar</button>
            </form>

            <a href="{{ route('catalog.getIndex') }}" class="btn btn-outline-secondary col-12 col-md-auto mb-3"><i
                    class="bi bi-arrow-left"></i>
                Volver al listado</a>
        </div>
    </div>
@endsection
