@extends('layouts.master')
@section('title', 'Catalogo')

@section('content')
    @if (isset($encabezado))
        <h1 class="display-5 mb-3">
            {{ $encabezado }}
        </h1>
    @endif
    @if ($peliculas->isEmpty())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <i class="bi bi-exclamation-triangle-fill"></i> No se encontraron peliculas
        </div>
    @else
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-xl-4 g-3">
            @foreach ($peliculas as $pelicula)
                <div class="col">
                    <a href="{{ url('/catalog/show/' . $pelicula->id) }}" class="text-decoration-none">

                        <div class="card shadow-sm h-100">

                            <img src="{{ $pelicula->poster }}" class="d-block w-100 card-img-top">

                            <div class="card-body d-flex flex-column">
                                <h5 class="card-title text-black">{{ $pelicula->title }} ({{ $pelicula->year }})</h5>
                                <p class="card-text text-truncate text-secondary">{{ $pelicula->synopsis }}</p>
                            </div>

                        </div>
                    </a>
                </div>
            @endforeach
    @endif
    </div>
@endsection
