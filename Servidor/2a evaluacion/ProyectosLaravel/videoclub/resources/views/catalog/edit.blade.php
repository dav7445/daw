@extends('layouts.master')
@section('title', 'Modificar ' . $pelicula->title)

@section('content')
    <form action="{{ route('catalog.update', $pelicula) }}" method="POST">
        @csrf
        @method('put')
        <div class="row">
            <h2 class="display-5">Modificar "{{ $pelicula->title }}"</h2>

            <div class="col-12 col-md-6 mb-3">
                <label for="title">Título</label>
                <input type="text" name="title" id="title" class="form-control" placeholder="{{ $pelicula->title }}"
                    required value="{{ $pelicula->title }}">
            </div>

            <div class="col-12 col-md-6 mb-3">
                <label for="year">Año</label>
                <input type="number" name="year" id="year" max="{{ date("Y") }}" class="form-control"
                    placeholder="{{ $pelicula->year }}" required value="{{ $pelicula->year }}">
            </div>

            <div class="col-12 mb-3">
                <label for="director">Director</label>
                <input type="text" name="director" id="director" class="form-control"
                    placeholder="{{ $pelicula->director }}" required value="{{ $pelicula->director }}">
            </div>

            <div class="col-12 mb-3">
                <label for="poster">Poster</label>
                <input type="url" name="poster" id="poster" class="form-control" placeholder="{{ $pelicula->poster }}"
                    required value="{{ $pelicula->poster }}">
            </div>

            <div class="col-12 mb-3">
                <label for="synopsis">Sinópsis</label>
                <textarea name="synopsis" id="synopsis" class="form-control" rows="5" placeholder="{{ $pelicula->synopsis }}"
                    required>{{ $pelicula->synopsis }}</textarea>
            </div>

            <div class="col-12 mb-3">
                <button type="submit" class="btn btn-primary col-12 col-md-auto mb-3">Modificar</button>
                <a href="{{ url('catalog/show/' . $pelicula->id) }}"
                    class="btn btn-outline-secondary col-12 col-md-auto mb-3">Cancelar</a>
            </div>
        </div>
    </form>
@endsection
