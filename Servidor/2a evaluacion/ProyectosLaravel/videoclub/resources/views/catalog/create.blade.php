@extends('layouts.master')
@section('title', 'Crear pelicula')

@section('content')
    <form action="{{ route('catalog.store') }}" method="POST">
        @csrf
        <div class="row">
            <h2 class="display-5">Añadir pelicula</h2>

            <div class="col-12 col-md-6 mb-3">
                <label for="floatingInput">Título</label>
                <input type="text" name="title" id="title" class="form-control" placeholder="Título de la película" required>
            </div>

            <div class="col-12 col-md-6 mb-3">
                <label for="floatingInput">Año</label>
                <input type="number" name="year" id="year" max="{{ date("Y") }}" class="form-control" placeholder="Año de estreno" required>
            </div>

            <div class="col-12 mb-3">
                <label for="floatingInput">Director</label>
                <input type="text" name="director" id="director" class="form-control" placeholder="Nombre del director" required>
            </div>

            <div class="col-12 mb-3">
                <label for="floatingInput">Poster</label>
                <input type="url" name="poster" id="poster" class="form-control" placeholder="URL del poster" required>
            </div>

            <div class="col-12 mb-3">
                <label for="floatingInput">Sinópsis</label>
                <textarea name="synopsis" id="synopsis" class="form-control" cols="30" rows="10" placeholder="Sinópsis de la película" required></textarea>
            </div>
            <div class="col-12 mb-3">
                <button type="submit" class="btn btn-success col-12 col-md-auto mb-3">Añadir</button>
            </div>
        </div>
    </form>
@endsection
