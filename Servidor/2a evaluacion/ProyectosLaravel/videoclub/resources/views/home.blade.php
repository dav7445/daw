@extends('layouts.master')
@section('title', 'Videoclub')
@section('content')
    <div class="container">
        <div class="row text-center">
            <h1 class="display-5">Te damos la bienvenida al videoclub, {{ $user->name }}</h1>
        </div>
    </div>
@endsection
