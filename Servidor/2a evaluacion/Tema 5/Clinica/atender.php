<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="assets/bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="assets/bootstrap/bootstrap.min.js"></script>
</head>

<body>
	<?php
	session_start();
	if (!isset($_SESSION["dniUsu"]) || $_SESSION["usuTipo"] !== "Medico" || !isset($_SESSION["idCita"])) {
		header("Location: index.php");
	}

	$enlace = mysqli_connect("localhost", "medico", "medico", "consultas");

	if (!isset($_SESSION["pacienteAtenderNombre"])) {
		$pacienteNombre = mysqli_query($enlace, 'SELECT pacNombres, pacApellidos FROM citas, pacientes WHERE dniPac = citPaciente AND idCita = "' . $_SESSION["idCita"] . '"');
		$dato = mysqli_fetch_array($pacienteNombre);
		$_SESSION["pacienteAtenderNombre"] = $dato["pacNombres"] . " " . $dato["pacApellidos"];
	}

	$alertType = $alertHeading = $alertInfo = $observaciones  = "";

	if (isset($_POST["atender"])) {
		$observaciones = $_POST["observaciones"];

		$atenderCita = mysqli_query($enlace, 'UPDATE citas SET CitObservaciones = "' . $_POST["observaciones"] . '", citEstado = "Atendido" WHERE idCita = ' . $_SESSION["idCita"] . '');

		if ($atenderCita) {
			$alertType = "success";
			$alertHeading = "Cita Atendida";
			$alertInfo = "Observaciones: " . $_POST["observaciones"];
		} else {
			$alertType = "danger";
			$alertHeading = "Error al atender la cita";
		}
	} else if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location: index.php");
	} else if (isset($_POST['escoger'])) {
		header("Location: escoger.php");
	}
	?>

	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Se ha validado como <?= isset($_SESSION["usuTipo2"]) ? $_SESSION["usuTipo"] . " y " . $_SESSION["usuTipo2"] : $_SESSION["usuTipo"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="escoger">Opciones</button>
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>
	<form action="#" method="post">
		<div class="container py-5">
			<h3 class="mb-3 fw-normal">Paciente: <?= $_SESSION["pacienteAtenderNombre"] ?></h3>
			<div class="row">
				<?php
				if ($alertType != "") {
				?>
					<div class="alert alert-<?= $alertType ?>" role="alert">
						<h4 class="alert-heading"><?= $alertHeading ?></h4>
						<p class="mb-0"><?= $alertInfo ?></p>
					</div>
				<?php
				}
				?>

				<div class="mb-3 col-12">
					<label class="form-label">Observaciones</label>
					<textarea type="text" class="form-control" name="observaciones" required><?= $observaciones ?></textarea>
				</div>

				<div>
					<button type="submit" class="col-12 col-sm-auto btn btn-primary" name="atender">Atender</button>
				</div>

			</div>
		</div>
	</form>
</body>

</html>