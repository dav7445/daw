<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="assets/bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="assets/bootstrap/bootstrap.min.js"></script>
</head>

<body>
	<?php
	session_start();
	if (!isset($_SESSION["dniUsu"])) {
		header("Location: index.php");
	}
	?>
	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Se ha validado como <?= isset($_SESSION["usuTipo2"]) ? $_SESSION["usuTipo"] . " y " . $_SESSION["usuTipo2"] : $_SESSION["usuTipo"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>
	<div class="px-4 py-5 my-5 text-center">
		<div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
			<form action="#" method="post">
				<?php
				if ($_SESSION["usuTipo"] === "Administrador") {
				?>
					<h1>Administrador</h1>
					<button type="submit" class="btn btn-outline-primary btn-lg px-4 col-12 col-lg-auto mb-1 mb-lg-0" name="altapaciente">Alta Paciente</button>
					<button type="submit" class="btn btn-outline-primary btn-lg px-4 col-12 col-lg-auto mb-1 mb-lg-0" name="altamedico">Alta Medico</button>
				<?php
				} else if ($_SESSION["usuTipo"] === "Asistente") {
				?>
					<h1>Asistente</h1>
					<button type="submit" class="btn btn-outline-primary btn-lg px-4 col-12 col-lg-auto mb-1 mb-lg-0" name="citasatendidas">Citas Atendidas</button>
					<button type="submit" class="btn btn-outline-primary btn-lg px-4 col-12 col-lg-auto mb-1 mb-lg-0" name="nuevacita">Nueva Cita</button>
					<button type="submit" class="btn btn-outline-primary btn-lg px-4 col-12 col-lg-auto mb-1 mb-lg-0" name="altapaciente">Alta Paciente</button>
					<button type="submit" class="btn btn-outline-primary btn-lg px-4 col-12 col-lg-auto mb-1 mb-lg-0" name="medicos">Médicos</button>
					<button type="submit" class="btn btn-outline-primary btn-lg px-4 col-12 col-lg-auto mb-1 mb-lg-0" name="pacientes">Pacientes</button>
					<?php
				} else {
					if ($_SESSION["usuTipo"] === "Medico") {
					?>
						<h1>Médico</h1>
						<button type="submit" class="btn btn-outline-primary btn-lg px-4 col-12 col-lg-auto mb-1 mb-lg-0" name="citasatendidas">Citas Atendidas</button>
						<button type="submit" class="btn btn-outline-primary btn-lg px-4 col-12 col-lg-auto mb-1 mb-lg-0" name="citaspendientes">Citas Pendientes</button>
						<button type="submit" class="btn btn-outline-primary btn-lg px-4 col-12 col-lg-auto mb-1 mb-lg-0" name="pacientes">Pacientes</button>
					<?php
					}
					if ($_SESSION["usuTipo"] === "Paciente" || isset($_SESSION["usuTipo2"])) {
					?>
						<h1>Paciente</h1>
						<button type="submit" class="btn btn-outline-primary btn-lg px-4 col-12 col-md-auto mb-1 mb-md-0" name="citas">Citas Pendientes</button>
				<?php
					}
				}
				?>
			</form>
		</div>
	</div>
	<?php
	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location: index.php");
	} else if (isset($_POST["altapaciente"])) {
		$_SESSION["tipoRegistro"] = "altapaciente";
		header("Location: registro.php");
	} else if (isset($_POST["altamedico"])) {
		$_SESSION["tipoRegistro"] = "altamedico";
		header("Location: registro.php");
	} else if (isset($_POST["citasatendidas"])) {
		$_SESSION["verCitas"] = "atendidas";
		header("Location: citas.php");
	} else if (isset($_POST["citaspendientes"])) {
		$_SESSION["verCitas"] = "pendientes";
		header("Location: citas.php");
	} else if (isset($_POST["citas"])) {
		$_SESSION["verCitas"] = "paciente";
		header("Location: citas.php");
	} else if (isset($_POST["medicos"])) {
		$_SESSION["verUsuarios"] = "medicos";
		header("Location: usuarios.php");
	} else if (isset($_POST["pacientes"])) {
		$_SESSION["verUsuarios"] = "pacientes";
		header("Location: usuarios.php");
	} else if (isset($_POST["nuevacita"])) {
		header("Location: nuevaCita.php");
	}
	?>
</body>

</html>