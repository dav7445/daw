<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="assets/bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="assets/bootstrap/bootstrap.min.js"></script>
</head>

<body>
	<?php
	session_start();
	if (!isset($_SESSION["dniUsu"]) || !isset($_SESSION["verCitas"]) || ($_SESSION["usuTipo"] !== "Medico" && $_SESSION["usuTipo"] !== "Asistente" && $_SESSION["usuTipo"] !== "Paciente")) {
		header("Location: index.php");
	}

	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location: index.php");
	} else if (isset($_POST['escoger'])) {
		header("Location: escoger.php");
	} else if (isset($_POST['atender'])) {
		$_SESSION["idCita"] = $_POST['atender'];
		header("Location: atender.php");
	}

	if ($_SESSION["usuTipo"] === "Medico" && $_SESSION["verCitas"] === "atendidas") {
		$enlace = mysqli_connect("localhost", "medico", "medico", "consultas");
		$consultarCitas = mysqli_query($enlace, 'SELECT DISTINCT idCita, citFecha, citHora, pacNombres, pacApellidos, conNombre, CitObservaciones FROM citas, pacientes, medicos, consultorios WHERE citEstado = "Atendido" AND dniPac = citPaciente AND idConsultorio = citConsultorio AND citMedico = "' . $_SESSION["dniUsu"] . '" ORDER BY citFecha DESC, citHora DESC');
	} else if ($_SESSION["usuTipo"] === "Medico" && $_SESSION["verCitas"] === "pendientes") {
		$enlace = mysqli_connect("localhost", "medico", "medico", "consultas");
		$consultarCitas = mysqli_query($enlace, 'SELECT DISTINCT idCita, citFecha, citHora, pacNombres, pacApellidos, conNombre, citPaciente FROM citas, pacientes, medicos, consultorios WHERE citEstado = "Asignado" AND dniPac = citPaciente AND idConsultorio = citConsultorio AND citMedico = "' . $_SESSION["dniUsu"] . '" ORDER BY citFecha ASC, citHora ASC');
	} else if ($_SESSION["usuTipo"] === "Asistente" && $_SESSION["verCitas"] === "atendidas") {
		$enlace = mysqli_connect("localhost", "asistente", "asistente", "consultas");
		$consultarCitas = mysqli_query($enlace, 'SELECT citFecha, citHora, pacNombres, pacApellidos, conNombre, medNombres, medApellidos, CitObservaciones FROM citas, pacientes, medicos, consultorios WHERE citEstado = "Atendido" AND dniPac = citPaciente AND citMedico = dniMed AND idConsultorio = citConsultorio ORDER BY citFecha DESC, citHora DESC');
	} else {
		$enlace = mysqli_connect("localhost", "paciente", "paciente", "consultas");
		$consultarCitas = mysqli_query($enlace, 'SELECT citFecha, citHora, pacNombres, pacApellidos, conNombre, medNombres, medApellidos FROM citas, pacientes, medicos, consultorios WHERE citEstado = "Asignado" AND dniPac = citPaciente AND citMedico = dniMed AND citPaciente = "' . $_SESSION["dniUsu"] . '" AND idConsultorio = citConsultorio ORDER BY citFecha ASC, citHora ASC');
	}

	?>
	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Se ha validado como <?= isset($_SESSION["usuTipo2"]) ? $_SESSION["usuTipo"] . " y " . $_SESSION["usuTipo2"] : $_SESSION["usuTipo"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="escoger">Opciones</button>
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>
	<div class="container py-5">
		<h3 class="mb-3 fw-normal">Listado de citas <?= $_SESSION["verCitas"] ?></h3>
		<table class="table table-striped table-hover align-middle">
			<thead>
				<tr>
					<th>Fecha</th>

					<th>Hora</th>

					<?php
					if ($_SESSION["verCitas"] !== "paciente") {
					?>
						<th>Paciente</th>
					<?php
					}
					if ($_SESSION["usuTipo"] === "Asistente" || $_SESSION["verCitas"] === "paciente") {
					?>
						<th>Médico</th>
					<?php
					}
					?>

					<th>Consultorio</th>

					<?php
					if ($_SESSION["verCitas"] === "atendidas") {
					?>
						<th>Observaciones</th>
					<?php
					}
					?>

					<?php
					if ($_SESSION["verCitas"] === "pendientes") {
					?>
						<th>Atender</th>
					<?php
					}
					?>
				</tr>
			</thead>
			<tbody>
				<?php
				while ($fila = mysqli_fetch_array($consultarCitas)) {
				?>
					<tr>
						<td><?= $fila["citFecha"] ?></td>
						<td><?= $fila["citHora"] ?></td>
						<?php
						if ($_SESSION["verCitas"] !== "paciente") {
						?>
							<td><?= $fila["pacNombres"] ?> <?= $fila["pacApellidos"] ?></td>
						<?php
						}
						if ($_SESSION["usuTipo"] === "Asistente" || $_SESSION["verCitas"] === "paciente") {
						?>
							<td><?= $fila["medNombres"] ?> <?= $fila["medApellidos"] ?></td>
						<?php
						}
						?>
						<td><?= $fila["conNombre"] ?></td>
						<?php
						if ($_SESSION["verCitas"] === "atendidas") {
						?>
							<td><?= $fila["CitObservaciones"] ?></td>
						<?php
						}
						?>
						<?php
						if ($_SESSION["verCitas"] === "pendientes") {
						?>
							<td>
								<form action="#" method="POST">
									<button type="submit" class="btn btn-sm btn-primary" name="atender" value="<?= $fila["idCita"] ?>">Atender</button>
								</form>
							</td>
						<?php
						}
						?>
					</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>
	<?php
	?>
</body>

</html>