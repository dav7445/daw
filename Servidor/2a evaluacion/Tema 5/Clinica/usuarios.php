<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="assets/bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="assets/bootstrap/bootstrap.min.js"></script>
</head>

<body>
	<?php
	session_start();
	if (!isset($_SESSION["dniUsu"]) || ($_SESSION["usuTipo"] !== "Medico" && $_SESSION["usuTipo"] !== "Asistente")) {
		header("Location: index.php");
	}

	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:index.php");
	} else if (isset($_POST['escoger'])) {
		header("Location:escoger.php");
	} else if (isset($_POST['estado'])) {
		$tipo = $_SESSION["verUsuarios"] === "pacientes" ? "Paciente" : "Medico";
		$enlace = mysqli_connect("localhost", "asistente", "asistente", "consultas");
		if ($_POST['estado'] === "inactivar") {
			mysqli_query($enlace, 'UPDATE usuarios SET usuEstado = "Inactivo" WHERE usutipo = "' . $tipo . '" AND dniUsu = "' . $_POST["dniUsuario"] . '"');
		} else {
			mysqli_query($enlace, 'UPDATE usuarios SET usuEstado = "Activo" WHERE usutipo = "' . $tipo . '" AND dniUsu = "' . $_POST["dniUsuario"] . '"');
		}
	}

	if ($_SESSION["usuTipo"] === "Medico") {
		$enlace = mysqli_connect("localhost", "medico", "medico", "consultas");
		$consultar = mysqli_query($enlace, 'SELECT DISTINCT pacientes.* FROM pacientes, medicos, citas WHERE citPaciente = dniPac AND citMedico = "' . $_SESSION["dniUsu"] . '"');
	} else {
		$enlace = mysqli_connect("localhost", "asistente", "asistente", "consultas");
		if ($_SESSION["verUsuarios"] === "medicos") {
			$consultar = mysqli_query($enlace, 'SELECT medicos.*, usuEstado FROM medicos, usuarios WHERE dniMed = dniUsu AND usuTipo = "Medico"');
		} else {
			$consultar = mysqli_query($enlace, 'SELECT pacientes.*, usuEstado FROM pacientes, usuarios WHERE dniPac = dniUsu AND usuTipo = "Paciente"');
		}
	}
	?>
	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Se ha validado como <?= isset($_SESSION["usuTipo2"]) ? $_SESSION["usuTipo"] . " y " . $_SESSION["usuTipo2"] : $_SESSION["usuTipo"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="escoger">Opciones</button>
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>
	<div class="container py-5">
		<h3 class="mb-3 fw-normal">Listado de <?= $_SESSION["verUsuarios"] ?></h3>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<td>DNI</td>
					<td>Nombre</td>
					<?php
					if ($_SESSION["verUsuarios"] === "pacientes") {
					?>
						<td>Fecha de Nacimiento</td>
						<td>Sexo</td>
					<?php
					} else {
					?>
						<td>Especialidad</td>
						<td>Teléfono</td>
						<td>Correo</td>
					<?php
					}
					if ($_SESSION["usuTipo"] === "Asistente") {
					?>
						<td>Estado</td>
						<td>Cambiar Estado</td>
					<?php
					}
					?>

				</tr>
			</thead>
			<tbody>
				<?php
				while ($fila = mysqli_fetch_array($consultar)) {
				?>
					<tr>
						<?php
						if ($_SESSION["verUsuarios"] === "pacientes") {
						?>
							<td><?= $fila["dniPac"] ?></td>
							<td><?= $fila["pacNombres"] . " " . $fila["pacApellidos"] ?></td>
							<td><?= $fila["pacFechaNacimiento"] ?></td>
							<td><?= $fila["pacSexo"] ?></td>
						<?php
						} else {
						?>
							<td><?= $fila["dniMed"] ?></td>
							<td><?= $fila["medNombres"] . " " . $fila["medApellidos"] ?></td>
							<td><?= $fila["medEspecialidad"] ?></td>
							<td><?= $fila["medTelefono"] ?></td>
							<td><?= $fila["medCorreo"] ?></td>
						<?php
						}
						if ($_SESSION["usuTipo"] === "Asistente") {
						?>
							<td><?= $fila["usuEstado"] ?></td>
							<td>
								<form action="#" method="post">
									<input type="hidden" name="dniUsuario" value="<?= $_SESSION["verUsuarios"] === "pacientes" ? $fila["dniPac"] : $fila["dniMed"] ?>">
									<?php
									if ($fila["usuEstado"] === "Activo") {
									?>
										<button type="submit" class="btn btn-outline-danger btn-sm px-4 w-100" name="estado" value="inactivar">Inactivar</button>
									<?php
									} else {
									?>
										<button type="submit" class="btn btn-outline-primary btn-sm px-4 w-100" name="estado" value="activar">Activar</button>
									<?php
									}
									?>
								</form>
							</td>
					</tr>
			<?php
						}
					}
			?>
			</tbody>
		</table>
	</div>
	<?php
	?>
</body>

</html>