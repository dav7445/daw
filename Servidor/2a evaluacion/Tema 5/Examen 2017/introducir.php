<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="bootstrap/bootstrap.min.js"></script>
</head>

<body>
	<?php
	session_start();
	if (!isset($_SESSION["nombre"]) || $_SESSION["rol"] !== "director") {
		header("Location: index.php");
	}

	$alertType = $alertHeading = $alertInfo = $user = $apellidos = $localidad = "";

	if (isset($_POST["darAlta"])) {
		$user = $_POST["nombre"];
		$apellidos = $_POST["apellidos"];
		$localidad = $_POST["localidad"];

		if ($_POST["password"] !== $_POST["password2"]) {
			$alertType = "danger";
			$alertHeading = "Las contraseñas no coinciden";
		} else {
			$enlace = mysqli_connect("localhost", "director", "director", "cifp");
			$comprobarUsuario = 'SELECT `login`, `password` FROM `usuarios` WHERE login="' . $_POST["nombre"] . '" AND password="' . $_POST["password"] . '"';
			$dato = mysqli_query($enlace, $comprobarUsuario);
			if (mysqli_num_rows($dato) === 0) {
				$darAlta = "INSERT INTO `usuarios` (id, nombre, apellidos, login, password, rol, localidad) VALUES (NULL, '{$_POST["nombre"]}', '{$_POST["apellidos"]}', '{$_POST["nombre"]}', '{$_POST["password"]}', 'alumno', '{$_POST["localidad"]}')";
				mysqli_query($enlace, $darAlta);

				$dato2 = mysqli_query($enlace, $comprobarUsuario);
				if (mysqli_num_rows($dato2) !== 0) {
					$alertType = "success";
					$alertHeading = "Usuario Registrado";
					$alertInfo = "Usuario: " . $_POST["nombre"] . " " . $_POST["apellidos"] . " Localidad: " . $_POST["localidad"] . " Contraseña: " . $_POST["password"] . " Rol: alumno";
				}
			} else {
				$alertType = "danger";
				$alertHeading = "Usuario ya existe";
			}
		}
	} else if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:index.php");
	}
	?>

	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Buenos días <?php echo $_SESSION["nombre"] ?>, se ha validado como <?php echo $_SESSION["rol"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>
	<form action="#" method="post">
		<div class="container py-5">
			<div class="row">
				<?php
				if ($alertType != "") {
				?>
					<div class="alert alert-<?php echo $alertType ?>" role="alert">
						<h4 class="alert-heading"><?php echo $alertHeading ?></h4>
						<p class="mb-0"><?php echo $alertInfo ?></p>
					</div>
				<?php
				}
				?>

				<div class="mb-3 col-sm-6">
					<label for="nombre" class="form-label">Nombre</label>
					<input type="text" class="form-control" name="nombre" value="<?php echo $user ?>">
				</div>

				<div class="mb-3 col-sm-6">
					<label class="form-label">Apellidos</label>
					<input type="text" class="form-control" name="apellidos" value="<?php echo $apellidos ?>">
				</div>

				<div class="mb-3">
					<label class="form-label">Localidad</label>
					<input type="text" class="form-control" name="localidad" value="<?php echo $localidad ?>">
				</div>

				<div class="mb-3">
					<label class="form-label">Contraseña</label>
					<input type="password" class="form-control" name="password">
				</div>

				<div class="mb-3">
					<label class="form-label">Repetir Contraseña</label>
					<input type="password" class="form-control" name="password2">
				</div>

				<div>
					<button type="submit" class="btn btn-primary" name="darAlta">Dar de Alta</button>
				</div>

			</div>
		</div>
	</form>
</body>

</html>