-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-02-2022 a las 13:25:28
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cifp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

CREATE TABLE `notas` (
  `alumno` int(11) NOT NULL,
  `asignatura` varchar(20) NOT NULL,
  `fecha` date NOT NULL,
  `nota` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `notas`
--

INSERT INTO `notas` (`alumno`, `asignatura`, `fecha`, `nota`) VALUES
(1, 'Lengua', '2016-11-01', '8.00'),
(2, 'Lengua', '2016-11-01', '7.00'),
(2, 'Matemáticas', '2016-11-10', '3.00'),
(3, 'Lengua', '2016-11-01', '6.00'),
(3, 'Matemáticas', '2016-11-10', '4.00'),
(4, 'Lengua', '2016-11-01', '7.00'),
(4, 'Matemáticas', '2016-11-10', '6.00'),
(5, 'Lengua', '2016-11-01', '6.00'),
(5, 'Matemáticas', '2016-11-10', '9.00'),
(6, 'Lengua', '2016-11-01', '6.00'),
(6, 'Matemáticas', '2016-11-10', '9.00'),
(7, 'Lengua', '2016-11-01', '3.00'),
(7, 'Matemáticas', '2016-11-10', '9.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apellidos` varchar(40) NOT NULL,
  `login` varchar(12) DEFAULT NULL,
  `password` varchar(12) DEFAULT NULL,
  `rol` longtext NOT NULL,
  `localidad` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellidos`, `login`, `password`, `rol`, `localidad`) VALUES
(1, 'Juan', 'Pérez', 'Juan', '123', 'alumno', 'Oviedo'),
(2, 'Yolanda', 'López', 'Yolanda', '123', 'alumno', 'Oviedo'),
(3, 'María', 'López', 'María', '123', 'alumno', 'Oviedo'),
(4, 'Luis', 'Alvarez', 'Luis', '123', 'alumno', 'Oviedo'),
(5, 'Teresa', 'Ramírez', 'Teresa', '123', 'alumno', 'Oviedo'),
(6, 'Cristina', 'Álvarez', 'Cristina', '123', 'alumno', 'Gijón'),
(7, 'Sandra ', 'Menéndez', 'Sandra', '123', 'alumno', 'Gijón'),
(8, 'Lucia ', 'Menéndez', 'Lucia', '123', 'director', 'Gijón');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`alumno`,`asignatura`,`fecha`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `notas`
--
ALTER TABLE `notas`
  ADD CONSTRAINT `notas_ibfk_1` FOREIGN KEY (`alumno`) REFERENCES `usuarios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
