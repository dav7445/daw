<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<?php
	session_start();
	$sessionErr = "";
	if ($_SESSION["rol"] === "administrador") {
		$_SESSION["connection"] = mysqli_connect("localhost", "administrador", "administrador", "ventas");	
	} else if ($_SESSION["rol"] === "consultor") {
		$_SESSION["connection"] = mysqli_connect("localhost", "consultor", "consultor", "ventas");
	} else {
		$sessionErr = "No puedes consultar";
	}
	$consulta = 'SELECT * FROM `articulos`';
	$consultaArticulo = mysqli_query($_SESSION["connection"], $consulta);
	echo '<div class="container">';
	echo '<div class="consulta">';
	echo '<p>Conectado el usuario ' . $_SESSION["usuario"] . ' con el rol ' . $_SESSION["rol"] . '</p>';
	echo '<p class="error">' . $sessionErr . '</p>';
	echo '
		<table>
			<thead>
				<th>ID</th>
				<th>Descripcion</th>
				<th>Precio</th>
				<th>Caracteristicas</th>
			</thead>
			<tbody>
	';
	while ($fila = mysqli_fetch_array($consultaArticulo)) {
		echo '
			<tr>
				<td> ' . $fila["id_articulo"] . '</td>
				<td> ' . $fila["descripcion"] . '</td>
				<td> ' . $fila["precio"] . '</td>
				<td> ' . $fila["caracteristicas"] . '</td>
			</tr>
		';
	}
	echo '</tbody></table>';
	echo '</div>';
	echo '</div>';
	?>
</body>