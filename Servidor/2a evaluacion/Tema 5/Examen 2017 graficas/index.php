<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link href="bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="bootstrap/bootstrap.min.js"></script>
	<style>
		html,
		body {
			height: 100%;
		}

		body {
			display: flex;
			align-items: center;
			padding-top: 40px;
			padding-bottom: 40px;
			background-color: #f5f5f5;
		}

		.form-signin {
			width: 100%;
			max-width: 330px;
			padding: 15px;
			margin: auto;
		}

		.form-signin input[type="text"] {
			margin-bottom: -1px;
			border-bottom-right-radius: 0;
			border-bottom-left-radius: 0;
		}

		.form-signin input[type="password"] {
			margin-bottom: 10px;
			border-top-left-radius: 0;
			border-top-right-radius: 0;
		}
	</style>
</head>

<?php
session_start();
$userErr = "";
if (isset($_POST["iniciar"])) {
	$_SESSION["usuario"] = $_POST["user"];

	$enlace = mysqli_connect("localhost", "alumno", "alumno", "cifp");
	$usuario = 'SELECT `id`, `nombre`, `apellidos`, `login`, `password`, `rol` FROM `usuarios` WHERE login = "' . $_SESSION["usuario"] . '" AND password="' . $_POST["password"] . '"';
	$dato = mysqli_query($enlace, $usuario);
	$fila = mysqli_fetch_assoc($dato);
	if (isset($fila)) {
		$_SESSION["rol"] = $fila["rol"];
	}
	if (mysqli_num_rows($dato) === 0) {
		$userErr = "Usuario no existe o contraseña incorrecta";
	} else {
		$_SESSION["id"] = $fila["id"];
		$_SESSION["nombre"] = $fila["nombre"];
		$_SESSION["apellidos"] = $fila["apellidos"];
		header("Location: escoger.php");
	}
	mysqli_close($enlace);
}
?>

<body class="text-center">
	<main class="form-signin">
		<form action="#" method="post">
			<h1 class="h3 mb-3 fw-normal">Inicia Sesión</h1>

			<div class="form-floating">
				<input type="text" class="form-control" id="floatingInput" name="user" placeholder="user" />
				<label for="floatingInput">Usuario</label>
			</div>
			<div class="form-floating">
				<input type="password" class="form-control" id="floatingPassword" name="password" placeholder="Password" />
				<label for="floatingPassword">Contraseña</label>
			</div>

			<button class="w-100 btn btn-lg btn-primary" type="submit" name="iniciar">Iniciar Sesión</button>
			<p class="text-danger"><?php echo $userErr ?></p>
		</form>
	</main>
</body>

</html>