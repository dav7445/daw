<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="bootstrap/bootstrap.min.js"></script>
</head>


<body>
	<?php
	session_start();
	if (!isset($_SESSION["nombre"])) {
		header("Location: index.php");
	}
	?>
	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Buenos días <?php echo $_SESSION["nombre"] ?>, se ha validado como <?php echo $_SESSION["rol"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>
	<div class="px-4 py-5 my-5 text-center">
		<h1 class="display-5 fw-bold">Buenos días <?php echo $_SESSION["nombre"] . " " . $_SESSION["apellidos"] ?></h1>
		<div class="col-lg-6 mx-auto">
			<div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
				<form action="#" method="post">
					<?php
					if ($_SESSION["rol"] === "alumno") {
					?>
						<button type="submit" class="btn btn-outline-primary btn-lg px-4 gap-3" name="consultarnotas">Consultar Notas</button>
					<?php
					} else if ($_SESSION["rol"] === "director") {
					?>
						<button type="submit" class="btn btn-outline-primary btn-lg px-4 gap-3" name="introducir">Insertar Alumnos</button>
						<button type="submit" class="btn btn-outline-primary btn-lg px-4" name="consultar">Mostrar Notas</button>
						<button type="submit" class="btn btn-outline-primary btn-lg px-4" name="consultarMedias">Mostrar Medias Asignaturas</button>
					<?php
					}
					?>
				</form>
			</div>
		</div>
	</div>
	<?php
	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:index.php");
	} else if (isset($_POST["consultarnotas"])) {
		header("Location: consultarNotas.php");
	} else if (isset($_POST["introducir"])) {
		header("Location: introducir.php");
	} else if (isset($_POST["consultar"])) {
		header("Location: consultarAsignaturas.php");
	} else if (isset($_POST["consultarMedias"])) {
		header("Location: consultarMediasAsignaturas.php");
	}
	?>
</body>

</html>