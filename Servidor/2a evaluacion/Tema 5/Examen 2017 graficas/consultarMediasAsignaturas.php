<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="bootstrap/bootstrap.min.js"></script>
</head>

<body>
	<?php
	session_start();
	if (!isset($_SESSION["nombre"]) || $_SESSION["rol"] !== "director") {
		header("Location: index.php");
	}

	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:index.php");
	}
	?>
	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Buenos días <?php echo $_SESSION["nombre"] ?>, se ha validado como <?php echo $_SESSION["rol"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>
	<div class="container py-5">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>Asignatura</th>
					<th>Nota Media</th>
					<th>Grafico Nota Media</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$enlace = mysqli_connect('localhost', "director", "director", 'cifp');
				$selectMediasAsignaturas = "SELECT asignatura, AVG(nota) AS media from notas GROUP BY asignatura";
				$consultarMediasAsignaturas = mysqli_query($enlace, $selectMediasAsignaturas);
				while ($fila = mysqli_fetch_array($consultarMediasAsignaturas)) {
				?>
					<tr>
						<td><?php echo $fila["asignatura"] ?></td>
						<td><?php echo $fila["media"] ?></td>
						<td>
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" style="stroke: red" width="<?php echo ($fila["media"] * 100) ?>" height="20">
								<line x1="0" y1="10" x2="<?php echo ($fila["media"] * 100) ?>" y2="10" stroke-width="20">
							</svg>
						</td>
					</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>
</body>

</html>