<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="bootstrap/bootstrap.min.js"></script>
</head>

<body>
	<?php
	session_start();
	if (!isset($_SESSION["nombre"]) || $_SESSION["rol"] !== "alumno") {
		header("Location: index.php");
	}

	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:index.php");
	}
	?>
	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Buenos días <?php echo $_SESSION["nombre"] ?>, se ha validado como <?php echo $_SESSION["rol"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>
	<div class="container py-5">
		<?php
		$enlace = mysqli_connect("localhost", "alumno", "alumno", "cifp");
		$selectNotasAsignatura = 'SELECT `asignatura`, `fecha`, `nota` FROM `notas` WHERE alumno="' . $_SESSION["id"] . '"';
		$consultarNotasAsignatura = mysqli_query($enlace, $selectNotasAsignatura);

		$selectAsignaturasSuspensas = 'SELECT COUNT(DISTINCT IF(`nota` < 5, `asignatura`, NULL)) AS suspensas FROM `notas` WHERE alumno="' . $_SESSION["id"] . '"';
		$consultarAsignaturasSuspensas = mysqli_query($enlace, $selectAsignaturasSuspensas);
		$asignaturasSuspensas = mysqli_fetch_array($consultarAsignaturasSuspensas);
		?>
		<h5>Ha suspendido: <?php echo $asignaturasSuspensas["suspensas"] ?></h5>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<td>Asignatura</td>
					<td>Fecha</td>
					<td>Nota</td>
				</tr>
			</thead>
			<tbody>
				<?php
				while ($fila = mysqli_fetch_array($consultarNotasAsignatura)) {
				?>
					<tr>
						<td><?php echo $fila["asignatura"] ?></td>
						<td><?php echo $fila["fecha"] ?></td>
						<td><?php echo $fila["nota"] ?></td>
					</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>
	<?php
	?>
</body>

</html>