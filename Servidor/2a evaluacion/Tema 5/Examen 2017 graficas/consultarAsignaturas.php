<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="bootstrap/bootstrap.min.js"></script>
</head>

<body>
	<?php
	session_start();
	if (!isset($_SESSION["nombre"]) || $_SESSION["rol"] !== "director") {
		header("Location: index.php");
	}

	$alertType = $alertHeading = $alertInfo = $asignatura = "";
	$mostrarNotas = false;

	$enlace = mysqli_connect("localhost", "director", "director", "cifp");

	if (isset($_POST["mostrarNotas"])) {
		$asignatura = $_POST["asignatura"];
		$comprobarAsignatura = 'SELECT `asignatura` FROM `notas` WHERE asignatura="' . $_POST["asignatura"] . '"';
		$dato = mysqli_query($enlace, $comprobarAsignatura);
		if (mysqli_num_rows($dato) == 0) {
			$alertType = "danger";
			$alertHeading = "No existe esa asignatura";
		} else {
			$mostrarNotas = true;
		}
	}

	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:index.php");
	}
	?>
	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Buenos días <?php echo $_SESSION["nombre"] ?>, se ha validado como <?php echo $_SESSION["rol"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>
	<div class="container py-5">
		<form action="#" method="post">
			<div class="row">
				<?php
				if ($alertType != "") {
				?>
					<div class="alert alert-<?php echo $alertType ?>" role="alert">
						<h4 class="alert-heading"><?php echo $alertHeading ?></h4>
						<p class="mb-0"><?php echo $alertInfo ?></p>
					</div>
				<?php
				}
				?>

				<div class="mb-3">
					<select class="form-select" name="asignatura">
						<option selected>Escoge una asignatura</option>
						<?php
						$selectAsignaturas = 'SELECT DISTINCT `asignatura` FROM `notas`';
						$consultarAsignaturas = mysqli_query($enlace, $selectAsignaturas);
						while ($fila = mysqli_fetch_array($consultarAsignaturas)) {
						?>
							<option value="<?php echo $fila["asignatura"] ?>"><?php echo $fila["asignatura"] ?></option>
						<?php
						}
						?>
					</select>
				</div>

				<div>
					<button type="submit" class="btn btn-primary mb-3" name="mostrarNotas">Mostrar Notas</button>
				</div>

			</div>
		</form>



		<?php
		if ($mostrarNotas) {
			$selectNotasAsignatura = 'SELECT `alumno`, `fecha`, `nota` FROM `notas` WHERE asignatura="' . $_POST["asignatura"] . '"';
			$consultarNotasAsignatura = mysqli_query($enlace, $selectNotasAsignatura);

			$selectDetallesAsignatura = 'SELECT AVG(nota) AS media, MAX(nota) AS alta, MIN(nota) AS baja, COUNT(DISTINCT(alumno)) AS total FROM `notas` WHERE asignatura="' . $_POST["asignatura"] . '"';
			$consultarDetallesAsignatura = mysqli_query($enlace, $selectDetallesAsignatura);
			$detallesAsignatura = mysqli_fetch_array($consultarDetallesAsignatura)
		?>
			<h4>Resultados de la asignatura: <?php echo $_POST["asignatura"] ?></h4>
			<h5>La nota media es: <?php echo $detallesAsignatura["media"] ?></h5>
			<h5>La nota más alta es: <?php echo $detallesAsignatura["alta"] ?></h5>
			<h5>La nota más baja es: <?php echo $detallesAsignatura["baja"] ?></h5>
			<h5>El total de alumnos es: <?php echo $detallesAsignatura["total"] ?></h5>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>ID Alumno</th>
						<th>Fecha</th>
						<th>Nota</th>
					</tr>
				</thead>
				<tbody>
					<?php
					while ($fila = mysqli_fetch_array($consultarNotasAsignatura)) {
					?>
						<tr>
							<td><?php echo $fila["alumno"] ?></td>
							<td><?php echo $fila["fecha"] ?></td>
							<td><?php echo $fila["nota"] ?></td>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
	</div>
<?php
		}
?>
</body>

</html>