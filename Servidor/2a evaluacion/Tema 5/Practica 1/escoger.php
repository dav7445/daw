<?php
session_start();
?>

<!DOCTYPE html>
<html lang="es">

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
	<?php
	echo 'Conectado el usuario ' . $_SESSION["usuario"] . ' con el rol ' . $_SESSION["rol"];
	?>
	<form action="#" method="post">
		<button type="submit" name="introducir">Introducir</button>
		<button type="submit" name="consultar">Consultar</button>
	</form>
	<?php
	
	if (isset($_POST["introducir"])) {
		header("Location: introducir.php");
	} else if (isset($_POST["consultar"])) {
		header("Location: consultar.php");
	}
	?>
</body>

</html>