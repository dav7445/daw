<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<?php
	session_start();
	$sessionErr = "";
	if ($_SESSION["rol"] === "administrador") {
		$_SESSION["connection"] = mysqli_connect("localhost", "administrador", "administrador", "ventas");
	} else if ($_SESSION["rol"] === "consultor") {
		$_SESSION["connection"] = mysqli_connect("localhost", "consultor", "consultor", "ventas");
	} else {
		$sessionErr = "No puedes consultar";
	}
	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:index.php");
	}
	$consulta = 'SELECT * FROM `articulos`';
	$consultaArticulo = mysqli_query($_SESSION["connection"], $consulta);
	echo '
		<div class="container">
			<div class="consulta">
				<form action="#" method="post">
					<div class="flex space-between">
						<p>Conectado el usuario ' . $_SESSION["usuario"] . ' con el rol ' . $_SESSION["rol"] . '</p>
						<button type="submit" class="cerrar" name="cerrarsesion">Cerrar sesion</button>
					</div>
					<p class="error">' . $sessionErr . '</p>
					<table>
						<thead>
							<th>ID</th>
							<th>Descripcion</th>
							<th>Precio</th>
							<th>Caracteristicas</th>
						</thead>
						<tbody>
	';
	while ($fila = mysqli_fetch_array($consultaArticulo)) {
		echo '
							<tr>
								<td> ' . $fila["id_articulo"] . '</td>
								<td> ' . $fila["descripcion"] . '</td>
								<td> ' . $fila["precio"] . ' €</td>
								<td> ' . $fila["caracteristicas"] . '</td>
							</tr>
		';
	}
	echo '
						</tbody>
					</table>
				</form>
			</div>
		</div>
	';
	?>
</body>