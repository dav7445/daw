<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="bootstrap/bootstrap.min.js"></script>
</head>

<body>
	<?php
	session_start();
	if (!isset($_SESSION["rol"]) || $_SESSION["rol"] !== "alumno") {
		header("Location: ejercicio1.php");
	}

	$alertType = $alertHeading = $alertInfo = "";

	if (isset($_POST["matricularse"])) {
		$alertInfo = "Debes introducir: ";
		if (empty($_POST["pruebaA"])) {
			$alertType = "danger";
			$alertInfo .= "DNI ";
		}
		if (empty($_POST["codcurso"])) {
			$alertType = "danger";
			$alertInfo .= "codigo";
		}
		if (empty($_POST["pruebaA"])) {
			$alertType = "danger";
			$alertInfo .= "prueba A ";
		}
		if (empty($_POST["pruebaB"])) {
			$alertType = "danger";
			$alertInfo .= "prueba B ";
		}
		if (empty($_POST["tipo"])) {
			$alertType = "danger";
			$alertInfo .= "tipo";
		}
		if (empty($_POST["inscripcion"])) {
			$alertType = "danger";
			$alertInfo .= "inscripcion";
		}
		if ($alertType === "") {
			$enlace = mysqli_connect("localhost", "alumno", "alumno", "oposicion");
			$selectComprobarCurso = 'SELECT codigocurso FROM `curso` WHERE codigocurso =  "' . $_POST["codcurso"] . '"';
			$comprobarCurso = mysqli_query($enlace, $selectComprobarCurso);
			if (mysqli_num_rows($comprobarCurso) === 0) {
				$alertType = "danger";
				$alertHeading = "Curso no existe";
			} else {
				$insertMatricular = "INSERT INTO `matricula` (dnialumno, codcurso, pruebaA, pruebaB, tipo, inscripcion) VALUES ('{$_SESSION["dni"]}', '{$_POST["codcurso"]}', {$_POST["pruebaA"]}, {$_POST["pruebaB"]}, '{$_POST["tipo"]}', '{$_POST["inscripcion"]}')";
				mysqli_query($enlace, $insertMatricular);

				$selectComprobarMatricula = 'SELECT dnialumno FROM `matricula` WHERE dnialumno="' . $_POST["dni"] . '" AND codcurso="' . $_POST["codcurso"] . '" AND pruebaA="' . $_POST["pruebaA"] . '" AND pruebaB="' . $_POST["pruebaB"] . '" AND tipo="' . $_POST["tipo"] . '" AND inscripcion="' . $_POST["inscripcion"] . '"';
				$comprobarMatricula = mysqli_query($enlace, $selectComprobarMatricula);
				if (mysqli_num_rows($comprobarMatricula) !== 0) {
					$alertType = "success";
					$alertHeading = "Usuario Matriculado";
					$alertInfo = "DNI: " . $_SESSION["dni"] . " Codigo Curso: " . $_POST["codcurso"] . " Prueba A: " . $_POST["pruebaA"] . " Prueba B: " . $_POST["pruebaB"] . " Tipo: " . $_POST["tipo"] . " Inscripcion: " . $_POST["inscripcion"];
				} else {
					$alertType = "danger";
					$alertHeading = "ERROR";
					$alertInfo = "Ha ocurrido un error, usuario no matriculado";
				}
			}
		}
	} else if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location: ejercicio1.php");
	}
	?>

	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Alumno: <?php echo $_SESSION["dni"] ?> Nombre: <?php echo $_SESSION["nombre"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>
	<form action="#" method="post">
		<div class="container py-5">
			<div class="row">
				<?php
				if ($alertType !== "") {
				?>
					<div class="alert alert-<?php echo $alertType ?>" role="alert">
						<h4 class="alert-heading"><?php echo $alertHeading ?></h4>
						<p class="mb-0"><?php echo $alertInfo ?></p>
					</div>
				<?php
				}
				?>

				<div class="mb-3">
					<label for="nombre" class="form-label">DNI</label>
					<input type="text" class="form-control" name="dni" required readonly value="<?php echo $_SESSION["dni"] ?>">
				</div>

				<div class="mb-3">
					<label class="form-label">Codigo Curso</label>
					<input type="text" class="form-control" required name="codcurso">
				</div>

				<div class="mb-3 col-sm-6">
					<label class="form-label">Prueba A</label>
					<input type="text" class="form-control" required name="pruebaA">
				</div>

				<div class="mb-3 col-sm-6">
					<label class="form-label">Prueba B</label>
					<input type="text" class="form-control" required name="pruebaB">
				</div>

				<div class="mb-3">
					<label class="form-label">Tipo</label>
					<input type="text" class="form-control" required name="tipo">
				</div>

				<div class="mb-3">
					<label class="form-label">Fecha Inscripcion</label>
					<input type="text" class="form-control" required name="inscripcion">
				</div>

				<div>
					<button type="submit" class="btn btn-primary" required name="matricularse">Matricularse</button>
				</div>

			</div>
		</div>
	</form>
</body>

</html>