<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="bootstrap/bootstrap.min.js"></script>
</head>

<body>
	<?php
	session_start();
	if (!isset($_SESSION["rol"]) || $_SESSION["rol"] !== "profesor") {
		header("Location: ejercicio1.php");
	}

	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location: ejercicio1.php");
	}
	?>
	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Profesor: <?php echo $_SESSION["dni"] ?> Nombre: <?php echo $_SESSION["nombre"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>

	<div class="container py-5">
		<?php
		$enlace = mysqli_connect("localhost", "profesor", "profesor", "oposicion");
		$selectCursos = 'SELECT codigocurso, nombrecurso, maxalumnos, fechaini, fechafin, numhoras, profesor FROM `curso` WHERE profesor="' . $_SESSION["dni"] . '"';
		$consultaCursos = mysqli_query($enlace, $selectCursos);

		$selectHorasImpartidas = 'SELECT SUM(numhoras) AS horastotales FROM `curso` WHERE profesor="' . $_SESSION["dni"] . '"';
		$consultarHorasImpartidas = mysqli_query($enlace, $selectHorasImpartidas);
		$horasImpartidas = mysqli_fetch_array($consultarHorasImpartidas);
		?>
		<h5>Total de horas impartidas: <?php echo $horasImpartidas["horastotales"] ?></h5>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>Codigo Curso</th>
					<th>Nombre Curso</th>
					<th>Max Alumnos</th>
					<th>Fecha Inicio</th>
					<th>Fecha Fin</th>
					<th>Numero Horas</th>
					<th>Profesor</th>
				</tr>
			</thead>
			<tbody>
				<?php
				while ($fila = mysqli_fetch_array($consultaCursos)) {
				?>
					<tr>
						<td><?php echo $fila["codigocurso"] ?></td>
						<td><?php echo $fila["nombrecurso"] ?></td>
						<td><?php echo $fila["maxalumnos"] ?> </td>
						<td><?php echo $fila["fechaini"] ?></td>
						<td><?php echo $fila["fechafin"] ?></td>
						<td><?php echo $fila["numhoras"] ?></td>
						<td><?php echo $fila["profesor"] ?></td>
					</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>
	<?php
	?>
</body>

</html>