<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link href="bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="bootstrap/bootstrap.min.js"></script>
	<style>
		html,
		body {
			height: 100%;
		}
		body {
			display: flex;
			align-items: center;
			padding-top: 40px;
			padding-bottom: 40px;
			background-color: #f5f5f5;
		}
		.form-signin {
			width: 100%;
			max-width: 330px;
			padding: 15px;
			margin: auto;
		}
	</style>
</head>

<?php
session_start();
$userErr = "";
if (isset($_POST['entrar'])) {
	$enlace = mysqli_connect("localhost", "root", "", "oposicion");
	$usuario = 'SELECT dniA, dniP, nombreA, nombreP FROM alumno, profesor WHERE alumno.dniA="' . $_POST["dni"] . '" OR profesor.dniP="' . $_POST["dni"] . '"';
	$dato = mysqli_query($enlace, $usuario);
	$fila = mysqli_fetch_assoc($dato);

	if (mysqli_num_rows($dato) === 0) {
		$userErr = "El DNI introducido no existe.";
	} else if ($fila["dniA"] === $_POST["dni"]) {
		$_SESSION["dni"] = $fila["dniA"];
		$_SESSION["nombre"] = $fila["nombreA"];
		$_SESSION['rol'] = "alumno";
		header("Location:ejercicio3.php");
	} else if ($fila["dniP"] === $_POST["dni"]) {
		$_SESSION["dni"] = $fila["dniP"];
		$_SESSION["nombre"] = $fila["nombreP"];
		$_SESSION['rol'] = "profesor";
		header("Location:ejercicio2.php");
	}
}
?>

<body class="text-center">
	<main class="form-signin">
		<form action="#" method="post">
			<h1 class="h3 mb-3 fw-normal">Inicia Sesión</h1>

			<div class="form-floating mb-3">
				<input type="text" class="form-control" id="floatingInput" name="dni" placeholder="DNI" />
				<label for="floatingInput">DNI</label>
			</div>

			<button class="w-100 btn btn-lg btn-primary" type="submit" name="entrar">Entrar</button>
			<p class="text-danger"><?php echo $userErr ?></p>
		</form>
	</main>
</body>

</html>