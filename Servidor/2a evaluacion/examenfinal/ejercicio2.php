<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="bootstrap/bootstrap.min.js"></script>
</head>

<body>
	<?php
	session_start();
	if (!isset($_SESSION["nombre"])) {
		header("Location: ejercicio1.php");
	}

	$titulo = $age = $director = $poster = $alquilada = $sinopsis = "";

	if (!isset($_SESSION["estrellas"])) {
		$_SESSION["estrellas"] = 0;
	}

	$alertType = $alertHeading = $alertInfo = "";
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$titulo = $_POST["titulo"];
		$age = $_POST["age"];
		$director = $_POST["director"];
		$poster = $_POST["poster"];
		$alquilada = $_POST["alquilada"];
		$sinopsis = $_POST["sinopsis"];
	}
	if (isset($_POST["votar"])) {
		if ($_SESSION["estrellas"] < 10) {
			$_SESSION["estrellas"] += 1;
		}
	} else if (isset($_POST["enviar"])) {
		if ($_POST["alquilada"] < 0 || $_POST["alquilada"] > 1) {
			$alertType = "danger";
			$alertInfo .= "Alquilada debe ser 0 o 1";
		}
		if (empty($alertType)) {
			$enlace = mysqli_connect("localhost", "ACCESO", "", "video");

			$insertPelicula = "INSERT INTO `peliculas` (id, title, year, director, poster, rented, synopsis, nota) VALUES (NULL, '{$_POST["titulo"]}', '{$_POST["age"]}', '{$_POST["director"]}', '{$_POST["poster"]}', {$_POST["alquilada"]}, '{$_POST["sinopsis"]}', {$_POST["puntuacion"]})";
			mysqli_query($enlace, $insertPelicula);

			$selectComprobarRegistro = 'SELECT id FROM `peliculas` WHERE title="' . $_POST["titulo"] . '" AND year="' . $_POST["age"] . '" AND director="' . $_POST["director"] . '" AND poster="' . $_POST["poster"] . '" AND rented="' . $_POST["alquilada"] . '" AND synopsis="' . $_POST["sinopsis"] . '" AND nota="' . $_POST["puntuacion"] . '"';
			$comprobarRegistro = mysqli_query($enlace, $selectComprobarRegistro);
			if (mysqli_num_rows($comprobarRegistro) !== 0) {
				$alertType = "success";
				$alertHeading = "Pelicula insertada";
			} else {
				$alertType = "danger";
				$alertHeading = "ERROR";
				$alertInfo = "Ha ocurrido un error, pelicula no insertada";
			}
		}
	} else if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location: ejercicio1.php");
	}
	?>

	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Buenos días <?php echo $_SESSION["nombre"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>
	<form action="#" method="post">
		<div class="container py-5">
			<div class="row">
				<?php
				if ($alertType !== "") {
				?>
					<div class="alert alert-<?php echo $alertType ?>" role="alert">
						<h4 class="alert-heading"><?php echo $alertHeading ?></h4>
						<p class="mb-0"><?php echo $alertInfo ?></p>
					</div>
				<?php
				}
				?>

				<div class="mb-3">
					<label class="form-label">Titulo</label>
					<input type="text" class="form-control" required name="titulo" value="<?php echo $titulo?>">
				</div>

				<div class="mb-3">
					<label class="form-label">Año</label>
					<input type="number" class="form-control" required name="age" value="<?php echo $age?>">
				</div>

				<div class="mb-3">
					<label class="form-label">Director</label>
					<input type="text" class="form-control" required name="director" value="<?php echo $director?>">
				</div>

				<div class="mb-3">
					<label class="form-label">Poster</label>
					<input type="text" class="form-control" required name="poster" value="<?php echo $poster?>">
				</div>

				<div class="mb-3">
					<label class="form-label">Alquilada</label>
					<input type="number" class="form-control" required name="alquilada" value="<?php echo $alquilada?>">
				</div>

				<div class="mb-3">
					<label class="form-label">Sinopsis</label>
					<input type="text" class="form-control" required name="sinopsis" value="<?php echo $sinopsis?>">
				</div>

				<div class="mb-3">
					<form action="" method="post">
						<label class="form-label">Puntuacion</label>
						<?php
						for ($i = 0; $i < $_SESSION["estrellas"]; $i++) {
						?>
							<img src="estrella.jpg" style="width: 20px; height: 20px">
						<?php
						}
						?>
						<button type="submit" class="btn btn-primary" name="votar">Votar</button>
						<input type="hidden" class="form-control" name="puntuacion" value="<?php echo $_SESSION["estrellas"]?>">
					</form>
				</div>

				<div>
					<button type="submit" class="btn btn-primary" name="enviar">Enviar</button>
				</div>

			</div>
		</div>
	</form>
</body>

</html>