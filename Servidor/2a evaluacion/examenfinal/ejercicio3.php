<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="bootstrap/bootstrap.min.js"></script>
</head>

<body>
	<?php
	session_start();
	if (!isset($_SESSION["nombre"])) {
		header("Location:ejercicio1.php");
	}

	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:ejercicio1.php");
	}
	?>
	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Buenos días <?php echo $_SESSION["nombre"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>
	<div class="container py-5">
		<table class="table table-striped">
			<tbody>
				<?php
				$enlace = mysqli_connect('localhost', "ACCESO", "", 'video');
				$selectPeliculasOrdenadas = "SELECT * from peliculas WHERE rented='1' ORDER BY nota DESC";
				$consultarPeliculasOrdenadas = mysqli_query($enlace, $selectPeliculasOrdenadas);
				while ($fila = mysqli_fetch_array($consultarPeliculasOrdenadas)) {
				?>
					<tr>
						<td>
							<table>
								<tbody>
									<tr>
										<td>Titulo</td>
										<td><?php echo $fila["title"] ?></td>
									</tr>
									<tr>
										<td>Año</td>
										<td><?php echo $fila["year"] ?></td>
									</tr>
									<tr>
										<td>Director</td>
										<td><?php echo $fila["director"] ?></td>
									</tr>
									<tr>
										<td>Nota</td>
										<td>
											<svg version="1.1" xmlns="http://www.w3.org/2000/svg" style="stroke: red" width="<?php echo ($fila["nota"] * 10) ?>" height="20">
												<line x1="0" y1="10" x2="<?php echo ($fila["nota"] * 10) ?>" y2="10" stroke-width="20">
											</svg>
											<?php echo $fila["nota"] ?>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>
</body>

</html>