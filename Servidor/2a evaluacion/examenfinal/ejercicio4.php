<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="bootstrap/bootstrap.min.js"></script>
</head>

<body>
	<?php
	session_start();
	if (!isset($_SESSION["nombre"])) {
		header("Location:ejercicio1.php");
	}

	$alertType = $alertHeading = $alertInfo = "";
	$mostrarNotas = false;

	$enlace = mysqli_connect("localhost", "ACCESO", "", "video");

	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:ejercicio1.php");
	}
	?>
	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Buenos días <?php echo $_SESSION["nombre"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>
	<div class="container py-5">
		<?php
		$selectPeliculasOrdenadas = 'SELECT `title`, `year` FROM `peliculas`';
		$consultarPeliculasOrdenadas = mysqli_query($enlace, $selectPeliculasOrdenadas);

		$selectNuevaPelicula = 'SELECT title FROM `peliculas` ORDER BY year DESC';
		$consultarNuevaPelicula = mysqli_query($enlace, $selectNuevaPelicula);
		$nuevaPelicula = mysqli_fetch_array($consultarNuevaPelicula);

		$selectAntiguaPelicula = 'SELECT title FROM `peliculas` ORDER BY year';
		$consultarAntiguaPelicula = mysqli_query($enlace, $selectAntiguaPelicula);
		$antiguaPelicula = mysqli_fetch_array($consultarAntiguaPelicula)
		?>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Pelicula más nueva</th>
					<th>Pelicula más antigua</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php echo $nuevaPelicula["title"] ?></td>
					<td><?php echo $antiguaPelicula["title"] ?></td>
				</tr>
			</tbody>
		</table>

		<?php
		while ($fila = mysqli_fetch_array($consultarPeliculasOrdenadas)) {
		?>
			<div class="mb-3">
				<p class="mb-0"><?php echo $fila["title"] ?></p>
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" style="stroke: red" width="<?php echo ($fila["year"] - 1900) ?>" height="20">
					<line x1="0" y1="10" x2="<?php echo ($fila["year"] - 1900) ?>" y2="10" stroke-width="20">
				</svg>
				<?php echo $fila["year"] ?>
			</div>
		<?php
		}
		?>
	</div>
</body>

</html>