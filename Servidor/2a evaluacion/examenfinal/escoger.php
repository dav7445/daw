<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/bootstrap.min.css" rel="stylesheet" />
	<script src="bootstrap/bootstrap.min.js"></script>
</head>


<body>
	<?php
	session_start();
	if (!isset($_SESSION["nombre"])) {
		header("Location: ejercicio1.php");
	}
	?>
	<nav class="navbar navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand">Buenos días <?php echo $_SESSION["nombre"] ?></a>
			<form class="d-flex" action="#" method="post">
				<button class="btn btn-secondary me-2" type="submit" name="cerrarsesion">Cerrar Sesión</button>
			</form>
		</div>
	</nav>
	<div class="px-4 py-5 my-5 text-center">
		<div class="col-lg-6 mx-auto">
			<div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
				<form action="#" method="post">
					<button type="submit" class="btn btn-outline-primary btn-lg px-4 gap-3" name="insertar">Insertar Pelicula</button>
					<button type="submit" class="btn btn-outline-primary btn-lg px-4" name="disponibles">Peliculas disponibles</button>
					<button type="submit" class="btn btn-outline-primary btn-lg px-4" name="ordenadas">Peliculas ordenadas</button>
				</form>
			</div>
		</div>
	</div>
	<?php
	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:ejercicio1.php");
	} else if (isset($_POST["insertar"])) {
		header("Location: ejercicio2.php");
	} else if (isset($_POST["disponibles"])) {
		header("Location: ejercicio3.php");
	} else if (isset($_POST["ordenadas"])) {
		header("Location: ejercicio4.php");
	}
	?>
</body>

</html>