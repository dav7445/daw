<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<?php
	$primero = fopen("primero.txt","r") or die("Unable to open file!");
	$segundo = fopen("segundo.txt","r") or die("Unable to open file!");
	
	for ($i = 0; $i < 2; $i++) {
		for ($j = 0; $j < 9; $j++) {
			$valorActual = fgetc($primero);
			if ($i == 1) {
				$valorActual = fgetc($segundo);
			}
			$array[$i][$j] = $valorActual;
		}
	}
	
	for ($i = 0; $i < 2; $i++) {
		if ($i == 0) {
			echo "El primer fichero contiene: ";
		} else {
			echo "El segundo fichero contiene: ";
		}
		for ($j = 0; $j < 9; $j++) {
			echo $array[$i][$j].",";
		}
		echo "<br>";
	}
	
	echo "Los ficheros intercalados contienen: ";
	
	for ($i = 0; $i < 9; $i++) {
		for ($j = 0; $j < 2; $j++) {
			echo $array[$j][$i].",";
		}
	}
	
	fclose($primero);
	fclose($segundo);
	
?>
</html>
