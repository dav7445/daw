<!DOCTYPE html>
<html lang="es">
<head>
	<style>
		img {
			height: 100px;
		}
	</style>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
<?php
session_start();

if (!isset($_SESSION["monedas"])) {
	$_SESSION["monedas"] = 0;
	$_SESSION["puntos"] = 0;
	$_SESSION["aviso"] = "";
	$_SESSION["fruta1"] = rand(1, 8);
	$_SESSION["fruta2"] = rand(1, 8);
	$_SESSION["fruta3"] = rand(1, 8);
}

echo '
<form action="tragaperras2.php" method="post">
<table>
<tbody>
<tr>
	<td>
		<img src="img/frutas/'.$_SESSION["fruta1"].'.svg">
	</td>
	<td>
		<img src="img/frutas/'.$_SESSION["fruta2"].'.svg">
	</td>
	<td>
		<img src="img/frutas/'.$_SESSION["fruta3"].'.svg">
	</td>
</tr>
</tbody>
</table>
';

echo '
	<p><button type="submit" name="accion" value="moneda">Meter moneda</button></p>
	Monedas: <input value="'.$_SESSION["monedas"].'">
	<p><button ';
	if ($_SESSION["monedas"] == 0) echo "disabled";
	echo ' type="submit" name="accion" value="jugar">Jugar</button></p>
	Puntos: <input value="'.$_SESSION["puntos"].'">
';
echo '<span>'.$_SESSION["aviso"].'</span>';
echo '
	<p><button ';
	if ($_SESSION["monedas"] == 0 && $_SESSION["puntos"] == 0) echo "disabled";
	echo ' type="submit" name="accion" value="reiniciar">Reiniciar Juego</button></p>
	</form>
';
?>
</body>
</html>
