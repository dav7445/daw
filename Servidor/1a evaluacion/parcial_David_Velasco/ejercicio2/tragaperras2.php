<?php
session_start();

if (!isset($_SESSION["monedas"])) {
	$_SESSION["monedas"] = 0;
	$_SESSION["puntos"] = 0;
	$_SESSION["fruta1"] = rand(1, 8);
	$_SESSION["fruta2"] = rand(1, 8);
	$_SESSION["fruta3"] = rand(1, 8);
}

$accion = $_POST["accion"];
$_SESSION["aviso"] = "";

if ($accion == "reiniciar") {
	session_destroy();
	header("Location:tragaperras.php");
}

if ($accion == "moneda") {
	$_SESSION["monedas"]++;
} else if ($accion == "jugar" && $_SESSION["monedas"] > 0) {
	$_SESSION["fruta1"] = rand(1, 8);
	$_SESSION["fruta2"] = rand(1, 8);
	$_SESSION["fruta3"] = rand(1, 8);

	$_SESSION["monedas"]--;

	// tres iguales
	if ($_SESSION["fruta1"] == $_SESSION["fruta2"] &&
		$_SESSION["fruta1"] == $_SESSION["fruta3"]) {
		$puntos = 5;
		$_SESSION["puntos"] += $puntos;
	// dos iguales
	} else if (
		$_SESSION["fruta1"] == $_SESSION["fruta2"] ||
		$_SESSION["fruta2"] == $_SESSION["fruta3"] ||
		$_SESSION["fruta1"] == $_SESSION["fruta3"]) {
		$puntos = 2;
		$_SESSION["puntos"] += $puntos;
	// todas distintas
	} else {
		$_SESSION["puntos"] += 0;
	}
	
	if ($puntos > 0) {
		$_SESSION["aviso"] = 'ENHORABUENA! Ha ganado: '.$puntos.' puntos';
	}
	if ($_SESSION["monedas"] == 0) {
		$_SESSION["aviso"] = 'No tiene saldo, pulse boton reiniciar juego o introduzca mas monedas';
	}
}

header("Location:tragaperras.php");
