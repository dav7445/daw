<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<?php
	session_name("Ejercicio1");
	session_start();
	
	function recoge($var) {
		$tmp = (isset($_REQUEST[$var]))
		? trim(htmlspecialchars($_REQUEST[$var], ENT_QUOTES, "UTF-8"))
		: "";
		return $tmp;
	}
	
	$accion = recoge("accion");
	var_dump($accion);
	$accionOk = false;
	
	if($accion != "cero" && $accion != "subir" && $accion != "bajar") {
		header("Location:Ejercicio1.2.php");
		exit;
	} else {
		$accionOk = true;
	}
	
	if($accionOk) {
		if($accion == "cero") {
			$_SESSION["numero"] = 0;
		} elseif ($accion == "subir" ) {
			$_SESSION["numero"]++;
		} elseif ($_SESSION["numero"] == 0) {
			header("Location:Ejercicio1.2.php");
		} elseif ($accion == "bajar" ) {
			$_SESSION["numero"]--;
		}
		header("Location:Ejercicio1.2.php");
		exit;
	}
?>
</body>
</html>
