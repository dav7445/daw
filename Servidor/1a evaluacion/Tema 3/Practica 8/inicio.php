<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<?php
	session_start();
	if(isset($_POST["nombre"])) {
		$_SESSION["nombre"] = $_POST["nombre"];
	}
	
	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:index.html");
	}
	
	if(isset($_POST["quitar"])) {
		$_SESSION["productos"][$_POST["quitar"]]["cantidad"]--;
	}
	
	if(isset($_POST["añadir"])) {
		$_SESSION["productos"][$_POST["añadir"]]["cantidad"]++;
	}
	
	echo '
		<div class="contenedor">
	';
	
	echo '
		<header>
			<h1>Bienvenido/a '.$_SESSION["nombre"].'</h1>
			<form action="#" method="POST">
				<button type="submit" class="cerrar" name="cerrarsesion">Cerrar sesion</button>
			</form>
		</header>
		<div class="productos">
	';
	if(!isset($_SESSION["productos"])) {
		$_SESSION["productos"] = array (
			"tv" => array (
				"type" => "Television",
				"name" => "Sony KD-75X9000H",
				"desc" => "75 pulgadas",
				"prec" => 1700,
				"cantidad" => 0,
			),
			"mv" => array (
				"type" => "Movil",
				"name" => "POCO X3 NFC",
				"desc" => "215g",
				"prec" => 229,
				"cantidad" => 0,
			),
			"mp4" => array (
				"type" => "Mp4",
				"name" => "iPod 4a Gen",
				"desc" => "8GB",
				"prec" => 60,
				"cantidad" => 0,
			),
			"rat" => array (
				"type" => "Raton",
				"name" => "Logitech G502 HERO",
				"desc" => "25600dpi",
				"prec" => 50,
				"cantidad" => 0,
			),
			"alf" => array (
				"type" => "Alfombrilla",
				"name" => "Corsair Tapis de Souris RGB",
				"desc" => "Negra",
				"prec" => 50,
				"cantidad" => 0,
			),
			"usb" => array (
				"type" => "USB",
				"name" => "SanDisk Ultra Flair",
				"desc" => "16GB",
				"prec" => 7,
				"cantidad" => 0,
			),
		);
	}
	$prod_keys = array_keys($_SESSION["productos"]);
	echo'
		<form action="#" method="post">
	';
	foreach ($_SESSION["productos"] as $indice => $p) {
		echo '
			<div class="elemento">
				<div>
					<img src="img/'.$indice.'.jpg"></img>
				</div>
				<span>'.$_SESSION["productos"][$indice]["type"].'</span>
				<p class="name">'.$_SESSION["productos"][$indice]["name"].'</p>
				<p class="desc">'.$_SESSION["productos"][$indice]["desc"].'</p>
				<p class="price">'.$_SESSION["productos"][$indice]["prec"].'€</p>
				<button type="submit" name="añadir" value="'.$indice.'">Añadir</button>
			</div>
		';
	}
	echo '
		</form>
	';
	
	echo '
		<div class="carrito">
		<form action="#" method="post">
		<div class="productosCarrito">
	';
	$cant = 0;
	$total = 0;
	$vacio = true;
	foreach($_SESSION["productos"] as $indice => $producto) {
		if ($_SESSION["productos"][$indice]["cantidad"] > 0) {
			$vacio = false;
			echo '
				<div class="prodCarrito">
				<img src="img/'.$prod_keys[$cant].'.jpg"></img>
			';
			echo "
				<p class='carritoName'>".$_SESSION["productos"][$indice]["name"]."</p>
				<div class='carritoDesc'>
				<p>".$_SESSION["productos"][$indice]["cantidad"]." unidades</p>
				<p>".($_SESSION["productos"][$indice]["prec"]* $_SESSION["productos"][$indice]["cantidad"])."€</p>
				</div>
				<div class='carritoDesc'>
			";
			echo '
				<button type="submit" name="quitar" value="'.$indice.'">Quitar</button>
				</div>
			';
			
			$total += $_SESSION["productos"][$indice]["prec"] * $_SESSION["productos"][$indice]["cantidad"];
			
			echo '
				</div>
				<hr>
			';
		}
		$cant++;
	}
	echo '
		</div>
		</form>
	';
	
	echo '
		<div class="total">
	';
	if ($vacio) {
		echo '
			<p>Carrito vacio</p>
		';
	} else {
		echo '
			<p>Precio total: '.$total.'€</p>
			<form action="confirmar.php" method="post">
				<button type="submit" name="submit">Continuar</button>
			</form>
		';
	}
	
	echo '
		</div>
	';
	
	echo '
		</div>
		</div>
		</div>
	';
?>
</body>
</html>
