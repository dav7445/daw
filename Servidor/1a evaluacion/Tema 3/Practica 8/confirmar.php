<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<?php
	session_start();
	
	if (isset($_POST['cerrarsesion'])) {
		session_destroy();
		header("Location:index.html");
	}
	
	echo '
		<div class="contenedor">
	';
	
	echo '
		<header>
			<h1>Felicidades '.$_SESSION["nombre"].'</h1>
			<form action="#" method="POST">
				<button type="submit" class="cerrar" name="cerrarsesion">Cerrar sesion</button>
			</form>
		</header>
	';
	$prod_keys = array_keys($_SESSION["productos"]);
	echo '
		<h2>Usted acaba de adquirir</h2>
		<br>
	';
	
	echo '
		<div class="carrito">
		<div class="productosCarrito">
	';
	$cant = 0;
	$total = 0;
	foreach($_SESSION["productos"] as $indice => $producto) {
		if ($_SESSION["productos"][$indice]["cantidad"] > 0) {
			echo '
				<div class="prodCarrito">
				<img src="img/'.$prod_keys[$cant].'.jpg"></img>
			';
			echo "
				<p class='carritoName'>".$_SESSION["productos"][$indice]["name"]."</p>
				<div class='carritoDesc'>
				<p>".$_SESSION["productos"][$indice]["cantidad"]." unidades</p>
				<p>".($_SESSION["productos"][$indice]["prec"]* $_SESSION["productos"][$indice]["cantidad"])."€</p>
				</div>
			";
			
			$total += $_SESSION["productos"][$indice]["prec"] * $_SESSION["productos"][$indice]["cantidad"];
			
			echo '
				</div>
				<hr>
			';
		}
		$cant++;
	}
	echo '
		</div>
		<div class="total">
			<p>Precio total: '.$total.'€</p>
		</div>
		</div>
		</div>
		</div>
	';
?>
</body>
</html>
