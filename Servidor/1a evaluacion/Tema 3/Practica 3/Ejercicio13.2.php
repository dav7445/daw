<html>
<head>
	<style>
	body {
		background-color: #dddddd;
	}
	.error {
		color: red;
	}
	.container {
		background-color: #efefef;
		font-family: sans-serif;
		width: 450px;
		padding: 8px;
		margin: auto;
		border-radius: 4px;
		box-shadow: 0 0 10px #0000004a;
	}
	h1 {
		margin-top: 0
	}
	textarea {
		width: 100%;
		height: 80px;
		resize: none
	}
	</style>
</head>
<body>
<div class="container">
<h1>PHP Form Validation Example</h1>

<?php
	$name = $nameErr = $email = $emailErr = $url = $urlErr = $comment = $gender = $genderErr = "";
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		
		// var_dump($_POST["comment"]);
		
		// NOMBRE
		$name = $_POST["name"];
		if (empty($name)) {
			$nameErr = "Name required";
		} else {
			if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
				$nameErr = "Invalid name format";
			}
		}
		
		// EMAIL
		$email = $_POST["email"];
		if (empty($email)) {
			$emailErr = "Email required";
		} else {
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$emailErr = "Invalid email format";
			}
		}
		
		// WEBSITE
		$url = $_POST["url"];
		if ((!filter_var($url, FILTER_VALIDATE_URL)) && !empty($url)) {
			$urlErr = "Invalid URL format";
		}
		
		// COMMENT
		(isset($_POST["comment"])) ? $comment = ($_POST["comment"]) : $comment;
		
		// GENDER
		(isset($_POST["gender"])) ? $gender = $_POST["gender"] : $gender = "";
		if (empty($gender)) {
			$genderErr = "Gender Required";
		}
		
	}
		
	echo '
	<span class="error"> * required field</span>
	<br>
	<form action="#" method="post">
		Name: <input type="text" name="name" value="'.$name.'"><span class="error"> * '.$nameErr.'</span>
		<br><br>
		
		E-mail: <input type="text" name="email" value="'.$email.'"><span class="error"> * '.$emailErr.'</span>
		<br><br>
		
		Website: <input type="text" name="url" value="'.$url.'"><span class="error"> '.$urlErr.'</span>
		<br><br>
		
		Comment: <textarea name="comment" rows="4" cols="50">'.$comment.'</textarea>
		<br><br>
		
		Gender: <input type="radio" name="gender"
	';
	if (isset($gender) && $gender == "female") {
		echo " checked";
	}
	echo '
		value="female"> Female
	';
	echo '
		<input type="radio" name="gender"
	';
	if (isset($gender) && $gender == "male") {
		echo " checked";
	}
	echo '
		value="male"> Male
		<span class="error">* '.$genderErr.'</span>
	';
	echo '
		<br><br>
		<p>
			<input type="submit" name="submit" value="Submit">
		</p>
		</form>
	';
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		echo "
			<h1>Your Input:</h1>
			Name: $name<br>
			E-mail: $email<br>
			Website: $url<br>
			Comment: $comment<br>
			Gender: $gender<br>
		";
	}
	
?>
</div>
</body>
</html>