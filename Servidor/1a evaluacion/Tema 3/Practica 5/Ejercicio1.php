<!DOCTYPE html>
<html>
<body>
	<form action="#" method="post" enctype="multipart/form-data">
		Selecciona un txt para subir:
		<input type="file" name="fileToUpload" id="fileToUpload">
		<br>
		<input type="submit" value="Upload" name="submit">
	</form>
	<?php
		if(isset($_POST["submit"])) {
			
			$target_dir = "uploads/";
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			
			// Check if file already exists
			if (file_exists($target_file)) {
				echo "El fichero ya existe";
				$uploadOk = 0;
			}
			
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 300000) {
				echo "Fichero demasiado grande";
				$uploadOk = 0;
			}
			
			// Allow certain file formats
			if($imageFileType != "txt") {
				echo "Solo se permiten archivos txt";
				$uploadOk = 0;
			}
			
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				echo "El archivo no ha sido subido.";
			// if everything is ok, try to upload file
			} else {
				if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
					echo "El archivo ". htmlspecialchars( basename( $_FILES["fileToUpload"]["name"])). " ha sido subido.";
				} else {
					echo "El archivo no ha sido subido.";
				}
			}
		}
	?>
</body>
</html>

