<!DOCTYPE html>
<html>
<head>
	<style>
		img {
			height: 100px;
			display: block;
		}
		td {
			border: 1px solid black;
			background-color: #000;
		}
	</style>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<?php
session_start();
$binNumArr = array();
$dosPotbinNumArr = array(8,4,2,1);
for ($i = 0; $i < 4; $i++) {
	array_push($binNumArr, rand(0,1));
}
$_SESSION["decNum"] = 0;
$binNumStr = "";
foreach($binNumArr as $indice => $valor) {
	$binNumStr = $binNumStr.$binNumArr[$indice];
	$_SESSION["decNum"] += $binNumArr[$indice]*$dosPotbinNumArr[$indice];
}

echo '
<h1>Adivina el numero en decimal</h1>
<h2>Numero en BINARIO: '.$binNumStr.'</h2>
<form action="ejercicio21.php" method="post">
<table>
	<tr>';
		foreach($binNumArr as $indice => $valor) {
			echo '
				<td><img ';
						if ($binNumArr[$indice] == 0) {
							echo 'style="visibility: hidden"';
						}
						echo 'src="'.$dosPotbinNumArr[$indice].'.jpg">
				</img></td>
			';
		}
echo '
	</tr>
</table>
<br>
Numero decimal <input type="number" name="numUsuario"> <button name="enviar">Enviar</button>
</form>
';
?>
</body>
</html>
