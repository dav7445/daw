<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<?php
session_start();
$_SESSION["numValido"] = true;
$_SESSION["valores"] = array();

if (isset($_POST["calcular"])) {
	
	for ($b = 0; $b < count($_POST)-1; $b++) {
		array_push($_SESSION["valores"],$_POST[$b]);
	}
	
	$k = 0;
	while ($k < count($_POST)-1 && $_SESSION["numValido"]) {
		if ($_POST[$k] > 100 || $_POST[$k] < 1) {
			$_SESSION["numValido"] = false;
		}
		$k++;
	}
	
	if ($_SESSION["numValido"]) {
		for ($l = 0; $l < count($_POST)-1; $l++) {
			echo $_POST[$l].' en binario: '.decbin($_POST[$l]).'<br>';
		}
	}
	
}

if (!$_SESSION["numValido"]) {
	echo '<p style="color:red">Los numeros tienen que estar entre 1 y 100</p>';
}
if (!isset($_POST["calcular"]) || !$_SESSION["numValido"]) {
	echo '
		<form action="#" method="post">
		';
		$cont = 0;
		for ($i = 0; $i < 3; $i++) {
			for ($j = 0; $j < 2; $j++) {
				echo 'E.'.$i.'.'.$j.' <input type="number"';
				
				if (!$_SESSION["numValido"]) {
					echo 'value="'.$_SESSION["valores"][$cont].'" ';
					
					if ($_SESSION["valores"][$cont] > 100 || $_SESSION["valores"][$cont] < 1) {
						echo 'style="border:1px solid red" ';
					}
					
				}
				
				echo 'name="'.$cont.'"> ';
				$cont++;
			}
			echo '<br>';
		}
		echo '
		<br>
		<button name="calcular">Calcular</button>
		</form>
	';
}
?>
</body>
</html>
