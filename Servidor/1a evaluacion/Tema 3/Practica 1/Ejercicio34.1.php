<html>
<head>
<style>
	* {
		text-align: center;
		font-family: sans-serif;
	}
	h1, h2 {
		color: #2882ba
	}
	table {
		margin: auto;
		border-collapse: collapse;
		border: 2px solid #2882ba;
	}
	thead {
		background-color: #2882ba;
		color: white;
	}
	thead th {
		border: none;
		min-width: 90px;
		font-weight: 800;
		padding-top: 0;
	}
	tr {
		height: 50px;
		user-select: none;
		transition: .15s;
	}
	table td {
		border: 1px solid #5b94b796;
		font-weight: 600;
	}
	td:hover {
		background-color: #2882ba10;
		transition: .15s;
	}
	tr:nth-child(odd) {
		background: #2882ba40
	}
	td:nth-child(7) {
		color: #ff6a6a;
	}
	.anterior {
		color: #00000052 !important
	}
</style>
</head>
<body>
<?php
	if (isset($_POST["generar"])) {
		
		$fecha = $_POST['ano']."/".$_POST['mes']."/15";
		$unixTimestamp = strtotime($fecha);
		$mesEmpiezaEn = jddayofweek(cal_to_jd(CAL_GREGORIAN, date("m",$unixTimestamp),date("d",$unixTimestamp), date("Y",$unixTimestamp)), 0);
		$diasEnElMes = cal_days_in_month(CAL_GREGORIAN, $_POST['mes'], $_POST['ano']);
		$diasDeLaSemana = [
			"Lunes","Martes","Miercoles",
			"Jueves","Viernes","Sabado","Domingo"
		];
		$mesesDelAno = [
			"Enero","Febrero","Marzo","Abril",
			"Mayo","Junio","Julio","Agosto",
			"Septiembre","Octubre","Noviembre","Diciembre"
		];
		
		
		// Si empieza en Domingo cambiar a 7
		if ($mesEmpiezaEn == 0) {
			$mesEmpiezaEn = 7;
		}
		
		$diasVacios = $mesEmpiezaEn - 1;
		
		echo '
			<h1>Calendario</h1>
			<h2>'.$mesesDelAno[$_POST['mes']-1].' '.$_POST['ano'].'</h2>
		';
		
		echo "
		<table>
			<thead>
				<tr>
		";
		for ($i = 0; $i < count($diasDeLaSemana); $i++) {
			echo "<th>$diasDeLaSemana[$i]</th>";
		}
		echo "
				</tr>
			</thead>
			<tbody>
		";
		
		$contarDias = 1;
		
		for ($j = 0; $j < $diasVacios; $j++) {
			if ($contarDias === 1) {
				echo "
					<tr>
				";
			}
			echo '<td></td>';
			$contarDias++;
		}
		
		for ($dia = 1; $dia <= $diasEnElMes; $dia++) {
			if ($contarDias === 1) {
				echo "
					<tr>
				";
			}
			echo "<td>$dia</td>";
			$contarDias++;
			if ($contarDias > 7) {
				$contarDias = 1;
				echo "
					</tr>
				";
			}
		}
		
		echo "
			</tbody>
		</table>
		";	
		
	} else {
		echo '
		<form action="#" method="post">
			<p>Mes <input type="number" name="mes"></p>
			<p>Año <input type="number" name="ano"></p>
			<p><input type="submit" name="generar" value="Generar"></p>
		</form>
		';
	}
?>
</body>
</html>