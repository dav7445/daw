<?php
	if (isset($_POST["submit"])) {
		if ($_POST["edad"] == 14) {
			echo "Menos de 14";
		} elseif ($_POST["edad"] == 15) {
			echo "Entre 15 y 20 años: todavía eres muy joven";
		} elseif ($_POST["edad"] == 21) {
			echo "De 21 a 40 años: eres una persona joven";
		} elseif ($_POST["edad"] == 41) {
			echo "Entre 41 y 60 años: eres una persona madura";
		} elseif ($_POST["edad"] == 61) {
			echo "Más de 60 años: Ya eres una persona mayor";
		}
	} else {
		echo '
			<h1>Dime tu edad:</h1>
			<form method="post" action="#">
				<input type="radio" id="men14" name="edad" value="14">
				<label for="men14">Menos de 14 años</label><br>
				
				<input type="radio" id="1520" name="edad" value="15">
				<label for="1520">de 15 a 20 años</label><br>
				
				<input type="radio" id="2140" name="edad" value="21">
				<label for="2140">de 21 a 40 años</label><br>
				
				<input type="radio" id="4160" name="edad" value="41">
				<label for="4160">de 41 a 60 años</label><br>
				
				<input type="radio" id="mas60" name="edad" value="61">
				<label for="mas60">más de 60 años</label><br>
				
				<input type="submit" name="submit" value="resultado">
			</form>
		';
	}
?>
