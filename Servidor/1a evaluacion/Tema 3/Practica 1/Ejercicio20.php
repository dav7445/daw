<?php
	$a = $_POST["a"];
	$b = $_POST["b"];
	$c = $_POST["c"];
	$calc = $b**2-4*$a*$c;
	if ($calc < 0) {
		echo "No hay soluciones";
	} elseif ($calc == 0) {
		echo "Solucion: ". -1*$b/2*$a;
	} else {
		echo "Solucion 1: ". (-1*$b+sqrt($b**2-$a*$c))/(2*$a);
		echo "<br>";
		echo "Solucion 2: ". (-1*$b-sqrt($b**2-$a*$c))/(2*$a);
	}
	// 1 0 0 = 0
	// 1 1 0 = 0 -1
	// 1 0 1 = No hay solucion
?>