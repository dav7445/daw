<?php
	$numeroAleatorio1 = rand(0, 10);
	echo $numeroAleatorio1."<br>";
	$numeroAleatorio2 = rand(0, 10);
	echo $numeroAleatorio2."<br>";
	$numMayor = max($numeroAleatorio1, $numeroAleatorio2);
	if ($numMayor % 2 === 0) {
		echo "El numero mayor $numMayor es par";
	} else {
		echo "El numero mayor $numMayor es impar";
	}
?>
