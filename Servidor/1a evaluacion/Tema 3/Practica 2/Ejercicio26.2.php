<?php
	$datos = array(
		array(
			"familia" => "Simpson",
			"padre" => "Homer",
			"madre" => "Marge",
			"hijos" => array(
				"Bart",
				"Lisa",
				"Maggie"
			)
		),
		array(
			"familia" => "Griffin",
			"padre" => "Peter",
			"madre" => "Lois",
			"hijos" => array(
				"Chris",
				"Meg",
				"Stewie"
			)
		)
	);
	echo "<ul>";
	foreach ($datos as $indiceDatos => $familia) {
		foreach ($familia as $indiceFamilia => $miembro) {
			if ($indiceFamilia == "familia") {
				echo "<b>$miembro</b>";
			} else if ($indiceFamilia == "hijos") {
				foreach ($miembro as $indiceMiembro => $hijo) {
					echo "<li>".$indiceFamilia.": ".$hijo."</li>";
				}
			} else {
				echo "<li>".$indiceFamilia.": ".$miembro."</li>";
			}
		}
	}
	echo "</ul>";
?>