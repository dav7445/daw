<?php
	$arr = array(
		"5" => 1,
		"12" => 2,
		"13" => 56,
		"x" => 42
	);
	echo "<table>";
	foreach ($arr as $indice => $n) {
		echo "<tr><td>".$indice."</td>";
		echo "<td>".$n."</td></tr>";
	};
	echo "</table>";
	
	echo "Cantidad de valores: ".count($arr);
	
	unset($arr["5"]);
	echo "<table>";
	foreach ($arr as $indice => $n) {
		echo "<tr><td>".$indice."</td>";
		echo "<td>".$n."</td></tr>";
	};
	echo "</table>";
	
	unset($arr);
?>