<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<?php
	session_start();
	if(isset($_POST)) {
		$_SESSION["nombre"] = $_POST["nombre"];
		$_SESSION["contra"] = $_POST["contrasena"];
	}
	echo '
		Los datos introducidos son los siguientes:
		<br><br>
		Usuario: '.$_SESSION["nombre"].'
		<br><br>
		Contraseña: '.$_SESSION["contra"].'
		<br><br>
		<form action="login.php" method="post">
			<button type="submit" name="submit">Continuar</button>
		</form>
		<br>
		<form action="index.php" method="post">
			<button type="submit" name="submit">Reintentar</button>
		</form>
	';
?>
</body>
</html>
