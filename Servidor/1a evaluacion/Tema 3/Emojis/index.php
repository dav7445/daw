<!DOCTYPE html>
<html>
<head>
	<style>
		img {
			height: 100px;
			width: auto;
		}
		tr table img {
			height: 20px;
			width: auto;
		}
	</style>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<?php
session_start();
$posicion = array(1,2,3,4,5,6,7);
if(!isset($_POST["barajar"])) {
	$_SESSION["contador"] = array(0,0,0,0,0,0,0);
}
if(isset($_POST["barajar"])) {
	shuffle($posicion);
	/*for ($i = 0; $i < count($posicion)-1 ; $i++) {
		// Guardo el valor de la posicion actual
		// En la posicion inicial = 1
		$valorActual = $posicion[$i];
		// Genero posicion nueva
		$posNueva = rand(0,count($posicion)-1);
		// Guardo el valor de la posicion que voy a sustituir
		$valorACambiar = $posicion[$posNueva];
		// Sustituir los valores
		$posicion[$i] = $valorACambiar;
		$posicion[$posNueva] = $valorActual;
	}*/
	$_SESSION["contador"][$posicion[6]-1]++;
}
echo '
<form action="#" method="post">
<table>
	<tr>
		<td>
			<img src="'.$posicion[0].'.png"></img>
		</td>
		<td>
			<img src="'.$posicion[1].'.png"></img>
		</td>
		<td>
			<img src="'.$posicion[2].'.png"></img>
		</td>
	</tr>
	<tr>
		<td>
			<img src="'.$posicion[3].'.png"></img>
		</td>
		<td>
			<img src="'.$posicion[4].'.png"></img>
		</td>
		<td>
			<img src="'.$posicion[5].'.png"></img>
		</td>
	</tr>
	<tr>
		<td><button type="submit" name="barajar">Barajar</button></td>
	</tr>
	<tr>
		<td>
			<p>Cara sin pintar:</p>
			<img src="'.$posicion[6].'.png"></img>
		</td>
	</tr>
	<tr>
		<table>
			<tr>
				<td>
					<img src="1.png"></img>
				</td>
				<td>
					<img src="2.png"></img>
				</td>
				<td>
					<img src="3.png"></img>
				</td>
				<td>
					<img src="4.png"></img>
				</td>
				<td>
					<img src="5.png"></img>
				</td>
				<td>
					<img src="6.png"></img>
				</td>
				<td>
					<img src="7.png"></img>
				</td>
			</tr>
			<tr>
				<td>'.$_SESSION["contador"][0].'</td>
				<td>'.$_SESSION["contador"][1].'</td>
				<td>'.$_SESSION["contador"][2].'</td>
				<td>'.$_SESSION["contador"][3].'</td>
				<td>'.$_SESSION["contador"][4].'</td>
				<td>'.$_SESSION["contador"][5].'</td>
				<td>'.$_SESSION["contador"][6].'</td>
			</tr>
		</table>
	</tr>
</table>
</form>
';
?>
</body>
</html>
