<html>
<head>
<style>
		:root {
			--bg1: #43658B;
			--bg2: #4E89AE;
			--bttnbg: #ED6663;
			--bttnbghv: #DE5F5D;
			--bttnbgact: #D15A58;
			--bttncolor: #FFF;
			--inputbg: #FFF;
			--inputcolor: #1A1A1A
		}
		body {
			background-color: var(--bg1)
		}
		table {
			display: flex;
			align-items: center;
			width: auto
		}
		tbody {
			padding: 8px;
			background-color: var(--bg2);
			border-radius: 8px
		}
		* {
			font-family: Arial;
			color: #FFF
		}
		td {
			font-size: 18px
		}
		form {
			margin-bottom: 0
		}
		input {
			border-radius: 3px;
			color: white;
			border: 0;
			outline: 0;
			height: 37.5px;
			font-size: 18px
		}
		input:not(.button) {
			background-color: var(--inputbg);
			color: var(--inputcolor);
			padding-left: 9px;
			width: 80px
		}
		.button {
			background-color: var(--bttnbg);
			color: var(--bttncolor);
			transition: .15s;
			width: 100%;
			text-align: center
		}
		.button:hover {
			background: var(--bttnbghv);
			transition: .15s
		}
		.button:active {
			background: var(--bttnbgact);
			transition: .15s
		}
</style>
</head>
<body>


<?php
	echo '
		<form action="Matriz2.php" method="post">
			<table>
			<tbody>
	';
	for ($i = 0; $i < $_POST["cantidad"]; $i++) {
		echo "<tr>";
		for ($j = 0; $j < $_POST["cantidad"]; $j++) {
			echo '
				<td>
					<input type="number" name="elem'.$i.$j.'">
				</td>
			';
		}
		echo "</tr>";
	}
	
	echo '	
			<tr>
				<td colspan="'.$_POST["cantidad"].'">
					<input class="button" type="submit" name="crearBoton" value="Crear">
				</td>
			</tr>
			</tbody>
			</table>
			
		</form>
	';
?>

</body>
</html>