<html>
<body>
<?php

	if(isset($_POST['escogerCantidad'])) {
		
		echo '
			<form action="#" method="post">
				<table>
				<tbody>
		';
		for ($i = 0; $i < $_POST["cantidad"]; $i++) {
			echo "<tr>";
			for ($j = 0; $j < $_POST["cantidad"]; $j++) {
				echo '
					<td>
						<input type="number" name="elem'.$i.$j.'">
					</td>
				';
			}
			echo "</tr>";
		}
		
		echo '	
				<tr>
					<td colspan="'.$_POST["cantidad"].'">
						<input class="button" type="submit" name="crearMatriz" value="Crear">
					</td>
				</tr>
				</tbody>
				</table>
				
			</form>
		';
		
	} elseif (isset($_POST['crearMatriz'])) {
		
		for ($i = 0; $i < sqrt(count($_POST)-1); $i++) {
			for ($j = 0; $j < sqrt(count($_POST)-1); $j++) {
				echo "El elemento $i$j es ".$_POST["elem$i$j"]."<br>";
			}
		}
		
		$sumaPrin = 0;
		echo "Los elementos de la diagonal principal son ";
		for ($i = 0; $i< sqrt(count($_POST)-1); $i++) {
			echo $_POST["elem$i$i"]." ";
			$sumaPrin += $_POST["elem$i$i"];
		}
		echo " y suman ".$sumaPrin;
		echo "<br>";
		
		$sumaSecun = 0;
		echo "Los elementos de la diagonal secundaria son ";
		$j = sqrt(count($_POST)-1);
		for ($i = 0; $i < sqrt(count($_POST)-1); $i++) {
			$j--;
			echo $_POST["elem$i$j"]." ";
			$sumaSecun += $_POST["elem$i$j"];
		}
		echo " y suman ".$sumaSecun;
		echo "<br>";
		echo "Principal + Secundaria = ".($sumaPrin+$sumaSecun);
		
	} else {
		
		echo '
		<form action="#" method="post">
			<p>Filas y columnas de la matriz
				<input type="number" name="cantidad">
			</p>
			<p>
				<input class="button" type="submit" name="escogerCantidad" value="Crear">
			</p>
		</form>
		';
		
	}

?>
</body>
</html>

