<html>
<head>
<style>
		:root {
			--bg1: #43658B;
			--bg2: #4E89AE;
			--bttnbg: #ED6663;
			--bttnbghv: #DE5F5D;
			--bttnbgact: #D15A58;
			--bttncolor: #FFF;
			--inputbg: #FFF;
			--inputcolor: #1A1A1A
		}
		body {
			background-color: var(--bg1)
		}
		* {
			font-family: Arial;
			color: #FFF
		}
</style>
</head>
<body>
<?php
	for ($i = 0; $i < sqrt(count($_POST)-1); $i++) {
		for ($j = 0; $j < sqrt(count($_POST)-1); $j++) {
			echo "El elemento $i$j es ".$_POST["elem$i$j"]."<br>";
		}
	}
	
	$sumaPrin = 0;
	echo "Los elementos de la diagonal principal son ";
	for ($i = 0; $i< sqrt(count($_POST)-1); $i++) {
		echo $_POST["elem$i$i"]." ";
		$sumaPrin += $_POST["elem$i$i"];
	}
	echo " y suman ".$sumaPrin;
	echo "<br>";
	
	$sumaSecun = 0;
	echo "Los elementos de la diagonal secundaria son ";
	$j = sqrt(count($_POST)-1);
	for ($i = 0; $i < sqrt(count($_POST)-1); $i++) {
		$j--;
		echo $_POST["elem$i$j"]." ";
		$sumaSecun += $_POST["elem$i$j"];
	}
	echo " y suman ".$sumaSecun;
	echo "<br>";
	echo "Principal + Secundaria = ".($sumaPrin+$sumaSecun);
?>
</body>
</html>

