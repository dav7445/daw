<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap-5.1.3-dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="bootstrap-5.1.3-dist/js/bootstrap.bundle.min.js"></script>
	<title>Vehículos</title>
	<style>
		p {
			font-size: 20px;
		}
	</style>
</head>

<body>
	<?php
	session_start();
	if ($_SESSION['r'] != "administrador" && $_SESSION['r'] != "consultor" && $_SESSION['r'] != "empleado") {
		session_destroy();
		header("Location:inicio.php");
	}
	if (isset($_POST['boton1'])) {
		header("Location:nuevovehiculo.php");
	} else if (isset($_POST['boton2'])) {
		header("Location:nuevoconcesionario.php");
	} else if (isset($_POST['boton3'])) {
		header("Location:verusuarios.php");
	} else if (isset($_POST['boton4'])) {
		header("Location:verbuzon.php");
	} else if (isset($_POST['boton5'])) {
		header("Location:consultas.php");
	} else if (isset($_POST['boton6'])) {
		header("Location:nuevomensaje.php");
	} else if (isset($_POST['boton7'])) {
		header("Location:visitas.php");
	} else if (isset($_POST['boton8'])) {
		header("Location:compras.php");
	} else if (isset($_POST['boton9'])) {
		header("Location:concesionarios.php");
	} else if (isset($_POST['boton10'])) {
		header("Location:nuevamarca.php");
	} else if (isset($_POST['boton11'])) {
		header("Location:vermarcas.php");
	} else if (isset($_POST['boton12'])) {
		header("Location:ventas.php");
	} else if (isset($_POST['boton13'])) {
		header("Location:nuevoempleado.php");
	} else if (isset($_POST['boton14'])) {
		header("Location:coches.php");
	} else if (isset($_POST['boton15'])) {
		session_destroy();
		header("Location:conexion.php");
	}
	?>
	<div class="container-fluid">
		<div class="row">
			<nav class="navbar navbar-expand-md bg-primary">
				<button class="navbar-dark navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="collapsibleNavbar">
					<ul class="navbar-nav me-auto">
						<li class="nav-item active">
							<a href="#" class="nav-link link-light">
								<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-house-fill" viewBox="0 0 16 16">
									<path fill-rule="evenodd" d="m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
									<path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z" />
								</svg>
								PRINCIPAL
							</a>
						</li>

						<li class="nav-item dropdown">
							<a class="nav-link link-light dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">
							<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 16 16">
								<path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
								<path fill-rule="evenodd" d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z"/>
								<path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>
							</svg> USUARIOS
							</a>
							<ul class="dropdown-menu">
							<?php
								if ($_SESSION['r'] == "administrador") {
								?>
									<li>
										<form action="#" method="POST" class="dropdown-item">
											<button type="submit" name="boton13" class="btn btn-primary my-3 w-100">
												<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-person-plus-fill" viewBox="0 0 16 16">
													<path d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
													<path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z" />
												</svg> NUEVO EMPLEADO
											</button>
										</form>
									</li>
								<?php
								}
								if ($_SESSION['r'] == "consultor") {
								?>
									<li>
										<form action="#" method="POST" class="dropdown-item">
											<button type="submit" name="boton6" class="btn btn-primary my-3 w-100">
												<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
													<path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z" />
												</svg> NUEVA SUGERENCIA O RECLAMACIÓN
											</button>
										</form>
									</li>
								<?php
								}
								?>
								<li>
									<form action="#" method="POST" class="dropdown-item">
										<button type="submit" name="boton3" class="btn btn-info my-3 w-100">
											<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-person-lines-fill" viewBox="0 0 16 16">
												<path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5 6s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zM11 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1h-4zm2 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2zm0 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2z"/>
											</svg> CONSULTAR USUARIOS
										</button>
									</form>
								</li>
								<?php
								if ($_SESSION['r'] == "administrador" || $_SESSION['r'] == "consultor") {
								?>
									<li>
										<form action="#" method="POST" class="dropdown-item">
											<button type="submit" name="boton4" class="btn btn-success my-3 w-100">
												<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-envelope-open-fill" viewBox="0 0 16 16">
													<path d="M8.941.435a2 2 0 0 0-1.882 0l-6 3.2A2 2 0 0 0 0 5.4v.314l6.709 3.932L8 8.928l1.291.718L16 5.714V5.4a2 2 0 0 0-1.059-1.765l-6-3.2ZM16 6.873l-5.693 3.337L16 13.372v-6.5Zm-.059 7.611L8 10.072.059 14.484A2 2 0 0 0 2 16h12a2 2 0 0 0 1.941-1.516ZM0 13.373l5.693-3.163L0 6.873v6.5Z" />
												</svg> CONSULTAR BUZÓN
											</button>
										</form>
									</li>
								<?php
								}
								?>
							</ul>
						</li>

						<li class="nav-item dropdown">
							<a class="nav-link link-light dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">
								<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-truck" viewBox="0 0 16 16">
									<path d="M0 3.5A1.5 1.5 0 0 1 1.5 2h9A1.5 1.5 0 0 1 12 3.5V5h1.02a1.5 1.5 0 0 1 1.17.563l1.481 1.85a1.5 1.5 0 0 1 .329.938V10.5a1.5 1.5 0 0 1-1.5 1.5H14a2 2 0 1 1-4 0H5a2 2 0 1 1-3.998-.085A1.5 1.5 0 0 1 0 10.5v-7zm1.294 7.456A1.999 1.999 0 0 1 4.732 11h5.536a2.01 2.01 0 0 1 .732-.732V3.5a.5.5 0 0 0-.5-.5h-9a.5.5 0 0 0-.5.5v7a.5.5 0 0 0 .294.456zM12 10a2 2 0 0 1 1.732 1h.768a.5.5 0 0 0 .5-.5V8.35a.5.5 0 0 0-.11-.312l-1.48-1.85A.5.5 0 0 0 13.02 6H12v4zm-9 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm9 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
								</svg>
								VEHÍCULOS
							</a>
							<ul class="dropdown-menu">
								<?php
								if ($_SESSION['r'] == "administrador" || $_SESSION['r'] == "empleado") {
								?>
									<li>
										<form action="#" method="POST" class="dropdown-item">
											<button type="submit" name="boton1" class="btn btn-primary my-3 w-100">
												<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
													<path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z" />
												</svg> NUEVO VEHÍCULO
											</button>
										</form>
									</li>
								<?php
								}
								?>
								<li>
									<form action="#" method="POST" class="dropdown-item">
										<button type="submit" name="boton5" class="btn btn-info my-3 w-100">
											<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-list-ul" viewBox="0 0 16 16">
												<path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm-3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
											</svg> CONSULTAR VEHÍCULOS
										</button>
									</form>
								</li>
								<li>
									<form action="#" method="POST" class="dropdown-item">
										<button type="submit" name="boton14" class="btn btn-info my-3 w-100">
											<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-cart3" viewBox="0 0 16 16">
												<path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .49.598l-1 5a.5.5 0 0 1-.465.401l-9.397.472L4.415 11H13a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l.84 4.479 9.144-.459L13.89 4H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
											</svg> COMPRAR VEHÍCULOS
										</button>
									</form>
								</li>
								<li>
									<form action="#" method="POST" class="dropdown-item">
										<button type="submit" name="boton7" class="btn btn-success my-3 w-100">
											<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-calendar-check" viewBox="0 0 16 16">
												<path d="M10.854 7.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 9.793l2.646-2.647a.5.5 0 0 1 .708 0z" />
												<path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
											</svg> CITAS SOLICITADAS
										</button>
									</form>
								</li>
								<li>
									<form action="#" method="POST" class="dropdown-item">
										<button type="submit" name="boton8" class="btn btn-success my-3 w-100">
											<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-cart-check" viewBox="0 0 16 16">
												<path d="M11.354 6.354a.5.5 0 0 0-.708-.708L8 8.293 6.854 7.146a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z" />
												<path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z" />
											</svg> COMPRAS REALIZADAS
										</button>
									</form>
								</li>
								<?php
								if ($_SESSION['r'] == "administrador") {
								?>
									<li>
										<form action="#" method="POST" class="dropdown-item">
											<button type="submit" name="boton12" class="btn btn-success my-3 w-100">
												<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-currency-euro" viewBox="0 0 16 16">
													<path d="M4 9.42h1.063C5.4 12.323 7.317 14 10.34 14c.622 0 1.167-.068 1.659-.185v-1.3c-.484.119-1.045.17-1.659.17-2.1 0-3.455-1.198-3.775-3.264h4.017v-.928H6.497v-.936c0-.11 0-.219.008-.329h4.078v-.927H6.618c.388-1.898 1.719-2.985 3.723-2.985.614 0 1.175.05 1.659.177V2.194A6.617 6.617 0 0 0 10.341 2c-2.928 0-4.82 1.569-5.244 4.3H4v.928h1.01v1.265H4v.928z" />
												</svg> VENTAS REALIZADAS
											</button>
										</form>
									</li>
								<?php
								}
								?>
							</ul>
						</li>
						<?php
						if ($_SESSION['r'] == "administrador" || $_SESSION['r'] == "empleado") {
						?>
							<li class="nav-item dropdown">
								<a class="nav-link link-light dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">
									<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-bank" viewBox="0 0 16 16">
										<path d="M8 .95 14.61 4h.89a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5H15v7a.5.5 0 0 1 .485.379l.5 2A.5.5 0 0 1 15.5 17H.5a.5.5 0 0 1-.485-.621l.5-2A.5.5 0 0 1 1 14V7H.5a.5.5 0 0 1-.5-.5v-2A.5.5 0 0 1 .5 4h.89L8 .95zM3.776 4h8.447L8 2.05 3.776 4zM2 7v7h1V7H2zm2 0v7h2.5V7H4zm3.5 0v7h1V7h-1zm2 0v7H12V7H9.5zM13 7v7h1V7h-1zm2-1V5H1v1h14zm-.39 9H1.39l-.25 1h13.72l-.25-1z" />
									</svg>
									CONCESIONARIOS
								</a>
								<ul class="dropdown-menu">
									<li>
										<form action="#" method="POST" class="dropdown-item">
											<button type="submit" name="boton2" class="btn btn-primary my-3 w-100">
												<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
													<path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z" />
												</svg> NUEVO CONCESIONARIO
											</button>
										</form>
									</li>
									<li>
										<form action="#" method="POST" class="dropdown-item">
											<button type="submit" name="boton9" class="btn btn-info my-3 w-100">
												<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-list-ul" viewBox="0 0 16 16">
													<path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm-3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
												</svg> CONSULTAR CONCESIONARIOS
											</button>
										</form>
									</li>
								</ul>
							</li>
						<?php
						}
						if ($_SESSION['r'] == "administrador" || $_SESSION['r'] == "empleado") {
						?>
							<li class="nav-item dropdown">
								<a class="nav-link link-light dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">
									<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-tags-fill" viewBox="0 0 16 16">
										<path d="M2 2a1 1 0 0 1 1-1h4.586a1 1 0 0 1 .707.293l7 7a1 1 0 0 1 0 1.414l-4.586 4.586a1 1 0 0 1-1.414 0l-7-7A1 1 0 0 1 2 6.586V2zm3.5 4a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z" />
										<path d="M1.293 7.793A1 1 0 0 1 1 7.086V2a1 1 0 0 0-1 1v4.586a1 1 0 0 0 .293.707l7 7a1 1 0 0 0 1.414 0l.043-.043-7.457-7.457z" />
									</svg>
									MARCAS
								</a>
								<ul class="dropdown-menu">
									<li>
										<form action="#" method="POST" class="dropdown-item">
											<button type="submit" name="boton10" class="btn btn-primary my-3 w-100">
												<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
													<path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z" />
												</svg> NUEVA MARCA
											</button>
										</form>
									</li>
									<li>
										<form action="#" method="POST" class="dropdown-item">
											<button type="submit" name="boton11" class="btn btn-info my-3 w-100">
												<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-list-ul" viewBox="0 0 16 16">
													<path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm-3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
												</svg> CONSULTAR MARCAS
											</button>
										</form>
									</li>
								</ul>
							</li>
						<?php
						}
						?>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="nav-item">
							<form action="#" method="POST" class="form-inline my-0 my-lg-0">
								<button class="btn btn-danger" type="submit" name="boton15">
									<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-box-arrow-left" viewBox="0 0 16 16">
										<path fill-rule="evenodd" d="M6 12.5a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-8a.5.5 0 0 0-.5.5v2a.5.5 0 0 1-1 0v-2A1.5 1.5 0 0 1 6.5 2h8A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-8A1.5 1.5 0 0 1 5 12.5v-2a.5.5 0 0 1 1 0v2z" />
										<path fill-rule="evenodd" d="M.146 8.354a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L1.707 7.5H10.5a.5.5 0 0 1 0 1H1.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3z" />
									</svg> CERRAR SESIÓN
								</button>
							</form>
						</li>
					</ul>
				</div>
			</nav>
	<?php
	echo "<h2 class='text-center'>Bienvenido/a al área de compras de AsturAuto, " . $_SESSION['usuario'] . "</h2>";
	echo "<br/><br/>";
	$conexion = mysqli_connect("localhost", $_SESSION['r'], $_SESSION['r'], "vehiculos");
	$coche = mysqli_query($conexion, "SELECT marcas.NOMBREMAR,coches.CODCOCHE,coches.MODELO,coches.POTENCIA,coches.NUMEPUERTAS,coches.NUMEPLAZAS,coches.COLOR,coches.COMBUSTIBLE,coches.MOTOR,coches.PRECIO,concesionarios.CIFCONCESI,concesionarios.NOMBRECON,concesionarios.CIUDADCON,distribucion.CANTIDAD FROM coches,marcas,concesionarios,distribucion WHERE marcas.CODMARCA=coches.CODMARCA AND distribucion.CIFCONCESI=concesionarios.CIFCONCESI AND distribucion.CODCOCHE=coches.CODCOCHE AND coches.ESTADO='Activo' AND concesionarios.ESTADOCON='Activo' ORDER BY NOMBREMAR,MODELO,NOMBRECON,CIUDADCON");
	$dni = $_SESSION['dni'];
	if (!isset($_SESSION['carrito'])) {
		$_SESSION['carrito'] = array();
	}

	if (isset($_POST['agregar'])) {
		$key = array_search($_POST['agregar'], array_column($_SESSION['carrito'], 0));
		if ($key !== false && $_SESSION['carrito'][$key][3] < $_POST['cantidad']) {
			$_SESSION['carrito'][$key][3]++;
		} else if ($key === false){
			array_push($_SESSION['carrito'], [$_POST['agregar'], $_POST['cifconcesi'], $_POST['precio'], 1]);
		}
	}  
	
	if (isset($_POST['quitar'])) {
		$key = array_search($_POST['quitar'], array_column($_SESSION['carrito'], 0));
		if ($key !== false) {
			unset($_SESSION['carrito'][$key]);
			$_SESSION['carrito'] = array_values($_SESSION['carrito']);
		}
	} 
	
	if (isset($_POST['comprar'])) {
		$fecha = date('Y-m-d H:i:s');
		for ($i = 0; $i < count($_SESSION['carrito']); $i++) {
			$select = mysqli_query($conexion, "SELECT marcas.NOMBREMAR,coches.CODCOCHE,coches.MODELO,coches.POTENCIA,coches.NUMEPUERTAS,coches.NUMEPLAZAS,coches.COLOR,coches.COMBUSTIBLE,coches.MOTOR,coches.PRECIO,concesionarios.CIFCONCESI,concesionarios.NOMBRECON,concesionarios.CIUDADCON,distribucion.CANTIDAD FROM coches,marcas,concesionarios,distribucion WHERE marcas.CODMARCA=coches.CODMARCA AND distribucion.CIFCONCESI=concesionarios.CIFCONCESI AND distribucion.CODCOCHE=coches.CODCOCHE AND coches.ESTADO='Activo' AND concesionarios.ESTADOCON='Activo' AND coches.CODCOCHE='". $_SESSION['carrito'][$i][0] ."' ORDER BY NOMBREMAR,MODELO,NOMBRECON,CIUDADCON");
			$auto=mysqli_fetch_array($select);
			$consul = mysqli_query($conexion, "SELECT distribucion.CIFCONCESI, distribucion.CODCOCHE, distribucion.CANTIDAD FROM distribucion,concesionarios WHERE distribucion.CODCOCHE ='" . $_SESSION['carrito'][$i][0] . "' AND distribucion.CIFCONCESI=concesionarios.CIFCONCESI");
			while ($vehiculo = mysqli_fetch_array($consul)) {
				if ($vehiculo['CANTIDAD'] == 0) {
					echo '<div class="container">';
						echo "<div class='text-center text-danger' style='background-color:lightpink;'>";
							echo "<h4>El vehículo ".$auto['NOMBREMAR']." ".$auto['MODELO']." no está disponible para comprar en este momento en el concesionario ".$auto['NOMBRECON']." - ".$auto['NOMBREMAR']." - ".$auto['CIUDADCON']."</h4>";
						echo "</div>";
					echo '</div>';
				} else {
					$x = $vehiculo['CANTIDAD'] - ($_SESSION['carrito'][$i][3]);
					$compra = mysqli_query($conexion, "INSERT INTO compras (ID, DNIUSU, FECHA, CIFCONCESI, CODCOCHE, CANTIDAD, PRECIO) VALUES (NULL, '{$dni}', '{$fecha}', '{$_SESSION['carrito'][$i][1]}', {$_SESSION['carrito'][$i][0]}, '{$_SESSION['carrito'][$i][3]}', {$_SESSION['carrito'][$i][2]})");
					$actualizacion = mysqli_query($conexion, "UPDATE distribucion SET CANTIDAD='" . $x . "' WHERE CODCOCHE = '" . $_SESSION['carrito'][$i][0] . "' AND distribucion.CIFCONCESI = '" . $_SESSION['carrito'][$i][1] . "'");
					if ($compra) {
						echo '<div class="container">';
							echo "<div class='text-center text-success' style='background-color:lightgreen;'>";
								echo "<h4>La compra realizada del vehículo ".$auto['NOMBREMAR']." ".$auto['MODELO']." se registró con éxito en la tabla Compras de la base de datos Vehículos</h4>";
								echo "<h4>La compra realizada del vehículo ".$auto['NOMBREMAR']." ".$auto['MODELO']." se envió al concesionario ".$auto['NOMBRECON']." - ".$auto['NOMBREMAR']." - ".$auto['CIUDADCON']."</h4>";
								echo "<h4>Un asesor comercial del concesionario ".$auto['NOMBRECON']." - ".$auto['NOMBREMAR']." - ".$auto['CIUDADCON']." se pondrá en contacto con usted para formalizar la compra del vehículo</h4>";
								echo "<h4>Gracias por su compra</h4>";
							echo "</div>";
						echo '</div>';
					} else {
						echo '<div class="container">';
							echo "<div class='text-center text-danger' style='background-color:lightpink;'>";
								echo "<h4>Error en el registro de la compra del vehículo ".$auto['NOMBREMAR']." ".$auto['MODELO']." en la tabla Compras de la base de datos Vehículos</h4>";
							echo "</div>";
						echo '</div>';
					}
		
					if ($actualizacion) {
						echo '<div class="container">';
							echo "<div class='text-center text-success' style='background-color:lightgreen;'>";
								echo "<h4>Cantidad de unidades del vehículo ".$auto['NOMBREMAR']." ".$auto['MODELO']." actualizada con éxito en la tabla Distribución de la base de datos Vehículos</h4>";
							echo "</div>";
						echo '</div>';
					} else {
						echo '<div class="container">';
							echo "<div class='text-center text-danger' style='background-color:lightpink;'>";
								echo "<h4>Error en actualización de la cantidad de unidades del vehículo ".$auto['NOMBREMAR']." ".$auto['MODELO']." con código en la tabla Distribución de la base de datos Vehículos</h4>";
							echo "</div>";
						echo '</div>';
					}
				}
			}
			
		}

		foreach ($_SESSION['carrito'] as $i => $value) { 
			unset($_SESSION['carrito'][$i]); 
		}
		
		if ($_SESSION['r'] == "administrador") {
			echo "<p class='text-center'><a href='administrador.php'>SIGUIENTE</a></p>";
		} else if ($_SESSION['r'] == "empleado") {
			echo "<p class='text-center'><a href='empleado.php'>SIGUIENTE</a></p>";
		} else if ($_SESSION['r'] == "consultor") {
			echo "<p class='text-center'><a href='consultor.php'>SIGUIENTE</a></p>";
		}
	}
	
	if ($_SESSION['r'] == "administrador") {
		echo "<p class='text-center'><a href='administrador.php'>ATRÁS</a></p>";
	} else if ($_SESSION['r'] == "empleado") {
		echo "<p class='text-center'><a href='empleado.php'>ATRÁS</a></p>";
	} else if ($_SESSION['r'] == "consultor") {
		echo "<p class='text-center'><a href='consultor.php'>ATRÁS</a></p>";
	}

	echo "<div class='container'>";
	echo "<div class='border border-primary rounded bg-light text-center col-12'>";
	echo "<h3 class='text-primary'>Carrito de la compra</h3>";
	
	$precio = 0;
	$total = 0;
	for ($i = 0; $i < count($_SESSION['carrito']); $i++) {
		if ($_SESSION['carrito'][$i][3] > 0) {
			$consulta = mysqli_query($conexion, 'SELECT MODELO FROM coches WHERE CODCOCHE="' . $_SESSION['carrito'][$i][0] . '"');
			$nombre = mysqli_fetch_array($consulta);
			echo "<h4>".$_SESSION['carrito'][$i][3] . " - " . $nombre['MODELO'] . "</h4>";
			$precio = $_SESSION['carrito'][$i][3] * $_SESSION['carrito'][$i][2];
			echo "<h3>" . $precio . "€</h3>";
			echo "<form action='#' method='POST'>";
			echo "<button class='btn btn-warning' style='width: 150px; height: 50px;' type='submit' name='quitar' value='" . $_SESSION['carrito'][$i][0] . "'>
						<svg xmlns='http://www.w3.org/2000/svg' width='30' height='30' fill='currentColor' class='bi bi-cart-dash' viewBox='0 0 16 16'>
							<path d='M6.5 7a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1h-4z'/>
							<path d='M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z'/>
						</svg><strong> Quitar</strong>
					</button>";
			echo "</form>";
			echo "<hr class='border border-primary'>";
		}
		$total = $total + $precio;
	}

	echo "<h3 class='text-center'>Total: " . $total . "€</h3>";

	echo "<br/><br/>";

	echo "<form action='#' method='POST'>";
	echo "<button class='text-center btn btn-success' style='width: 200px; height: 70px;' type='submit' name='comprar'>
			<svg xmlns='http://www.w3.org/2000/svg' width='35' height='35' fill='currentColor' class='bi bi-cart-check-fill' viewBox='0 0 16 16'>
				<path d='M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm-1.646-7.646-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L8 8.293l2.646-2.647a.5.5 0 0 1 .708.708z'/>
			</svg><strong> COMPRAR</strong>
		  </button>";
	echo "</form>";
	echo "<br/>";
	echo "</div>";
	echo "<br/>";
	echo "</div>";
	
	echo "<div class='table-responsive'>";
	echo "<table class='table border border-primary'>";
	echo "<thead class='table-primary text-center'>";
	echo "<tr>";
	echo "<th>Marca</th>";
	echo "<th>Modelo</th>";
	echo "<th>Potencia(CV)</th>";
	echo "<th>Color</th>";
	echo "<th>Motor</th>";
	echo "<th>Concesionario</th>";
	echo "<th>Localidad</th>";
	echo "<th>Cantidad</th>";
	echo "<th>Precio(€)</th>";
	echo "<th>Ver vehículo</th>";
	echo "<th>Agregar</th>";
	echo "</tr>";
	echo "</thead>";
	echo "<tbody class='table-light border-primary text-center'>";
	while ($_SESSION['coches'] = mysqli_fetch_array($coche)) {
		echo "<tr>";
			$_SESSION['coches']['CIFCONCESI'];
			echo "<td>" . $_SESSION['coches']['NOMBREMAR'] . "</td>";
			echo "<td>" . $_SESSION['coches']['MODELO'] . "</td>";
			echo "<td>" . $_SESSION['coches']['POTENCIA'] . "</td>";
			echo "<td>" . $_SESSION['coches']['COLOR'] . "</td>";
			echo "<td>" . $_SESSION['coches']['MOTOR'] . "</td>";
			echo "<td>" . $_SESSION['coches']['NOMBRECON'] . "</td>";
			echo "<td>" . $_SESSION['coches']['CIUDADCON'] . "</td>";
			echo "<td>" . $_SESSION['coches']['CANTIDAD'] . "</td>";
			echo "<td>" . $_SESSION['coches']['PRECIO'] . "€</td>";
			echo "<td><form action='citas.php' method='POST'><input type='hidden' name='marca' value='" . $_SESSION['coches']['NOMBREMAR'] . "'><input type='hidden' name='modelo' value='" . $_SESSION['coches']['MODELO'] . "'><input type='hidden' name='conce' value='" . $_SESSION['coches']['CIFCONCESI'] ."'><input type='hidden' name='ciudad' value='" . $_SESSION['coches']['CIUDADCON'] . "'><button class='btn btn-outline-primary' type='submit' name='cita' value='" . $_SESSION['coches']['CODCOCHE'] . "'>Solicitar cita</button></form></td>";
			echo "<td><form action='#' method='POST'><input type='hidden' name='cifconcesi' value='" . $_SESSION['coches']['CIFCONCESI'] . "'><input type='hidden' name='precio' value='" . $_SESSION['coches']['PRECIO'] . "'><input type='hidden' name='cantidad' value='". $_SESSION['coches']['CANTIDAD'] ."'><button class='btn btn-outline-success' type='submit' name='agregar' value='" . $_SESSION['coches']['CODCOCHE'] . "'>Agregar</button></form></td>";
		echo "</tr>";
	}
	echo "</tbody>";
	echo "</table>";
	echo "</div>";
	
	mysqli_close($conexion);
	include("templates/footer.php");
	?>
	</div>
	</div>
</body>
</html>