let i = 1;
// i = i ? i > 0 ? i += 2 : i : 0;
// i = i ? (i > 0 ? (i += 2) : i) : 0;
i = i ?
	i > 0 ?
		i += 2
	: i
: 0;
console.log(i);
