// Array methods

let arr = ["I", "study", "JavaScript"];

// splice
arr.splice(1, 1); // from index 1 remove 1 element
console.log( arr ); // ["I", "JavaScript"]

// forEach
arr.forEach(function(item, index, array) {
	console.log(`${item} is at index ${index} in [${array}]`);
});


// indexOf
console.log( arr.indexOf("I") ); // 0

// find
let users = [
	{id: 1, name: "John"},
	{id: 2, name: "Pete"},
	{id: 3, name: "Mary"}
];
console.log(users);
let user = users.find(item => item.id == 1);
console.log(user.name); // John

// filter
users = [
	{id: 1, name: "John"},
	{id: 2, name: "Pete"},
	{id: 3, name: "Mary"}
];
let someUsers = users.filter(item => item.id < 3); // [{id: 1, name: "John"}, {id: 2, name: "Pete"},]
console.log(someUsers.length); // 2

// map
const array1 = [1, 4, 9, 16];
const map1 = array1.map(function(elem) {
	return elem * 2;
});
console.log(map1); // [2, 8, 18, 32]

// sort (strings)
arr = [ 1, 2, 15 ];
arr.sort();
console.log( arr ); // [1, 15, 2]

// sort (numbers)
arr = [ 1, 2, 15 ];
arr.sort( (a, b) => a - b );
console.log(arr); // [1, 2, 15]

