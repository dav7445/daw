<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Wordle</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

	<style>
		.container {
			max-width: 600px;
		}

		.cuadrado {
			height: 50px;
			width: 50px;
			border: 1px solid rgb(0, 0, 0, .5);
		}

		.blanco {
			background-color: white;
		}

		.amarillo {
			background-color: yellow;
		}

		.verde {
			background-color: green;
		}
	</style>
</head>

<body>

	<div class="container mt-3">
		<div class="row">
			<?php
			session_start();

			const blanco = '<div class="cuadrado blanco"></div>';
			const amarillo = '<div class="cuadrado amarillo"></div>';
			const verde = '<div class="cuadrado verde"></div>';

			if (!isset($_SESSION["matrizResultado"])) {
				$_SESSION["matrizResultado"] = array(
					array(blanco, blanco, blanco, blanco, blanco),
					array(blanco, blanco, blanco, blanco, blanco),
					array(blanco, blanco, blanco, blanco, blanco),
					array(blanco, blanco, blanco, blanco, blanco),
					array(blanco, blanco, blanco, blanco, blanco),
					array(blanco, blanco, blanco, blanco, blanco)
				);
				$_SESSION["contador"] = -1;
			}

			if (isset($_POST["reset"])) {
				session_destroy();
				header("Location: #");
			} else if (isset($_POST["intentar"])) {
				$intento = $_POST["intento"];
				$adivinar = "aabcd";
				$_SESSION["contador"] += 1;
				$_SESSION["matrizResultado"] = nuevoIntento($adivinar, $intento, $_SESSION["contador"], $_SESSION["matrizResultado"]);
			}
			
			/**
			 * nuevoIntento
			 *
			 * @param  mixed $palabraAdivinar
			 * @param  mixed $palabraIntento
			 * @param  mixed $numeroIntento
			 * @param  mixed $matriz
			 * @return void
			 */
			function nuevoIntento($palabraAdivinar, $palabraIntento, $numeroIntento, $matriz)
			{
				$posicionNoAmarilla = array();

				for ($posicionIntento = 0; $posicionIntento < 5; $posicionIntento++) {
					if ($palabraIntento[$posicionIntento] === $palabraAdivinar[$posicionIntento]) {
						$matriz[$numeroIntento][$posicionIntento] = verde;
					}
					$posicionNoAmarilla[$posicionIntento] = blanco;
				}

				if (!todasAcertadas($matriz[$numeroIntento])) {
					for ($posicionIntento = 0; $posicionIntento < 5; $posicionIntento++) {
						$noAmarilla = true;
						$posContador = 0;
						while ($posContador < 5 && $noAmarilla && $matriz[$numeroIntento][$posicionIntento] !== verde) {
							if (
								$palabraIntento[$posicionIntento] === $palabraAdivinar[$posContador]
								&& $posicionIntento !== $posContador
								&& $matriz[$numeroIntento][$posContador] !== verde
								&& $posicionNoAmarilla[$posContador] !== amarillo
							) {
								$posicionNoAmarilla[$posContador] = amarillo;
								$matriz[$numeroIntento][$posContador] = amarillo;
								$noAmarilla = false;
							}
							$posContador++;
						}
					}
				}
				return $matriz;
			}
			
			/**
			 * todasAcertadas
			 *
			 * @param  mixed $array
			 * @return void
			 */
			function todasAcertadas($array)
			{
				foreach ($array as $valor) {
					if ($valor !== verde) {
						return false;
					}
				}
				return true;
			}

			
			/**
			 * dibujarcuadrados
			 *
			 * @param  mixed $matriz
			 * @return void
			 */
			function dibujarcuadrados($matriz)
			{
			?>
				<div class="d-flex flex-column align-items-center">
					<?php
					for ($a = 0; $a < 6; $a++) {
					?>
						<div class="d-flex">
							<?php
							for ($b = 0; $b < 5; $b++) {
								echo $matriz[$a][$b];
							}
							?>
						</div>
					<?php
					}
					?>
				</div>
			<?php
			}

			dibujarcuadrados($_SESSION["matrizResultado"]);

			?>

			<div class="col-12 mt-3">
				<form action="#" method="post">
					<div class="row">
						<div class="col-12 col-md-8">
							<label for="intento" class="visually-hidden">Palabra</label>
							<input type="text" class="form-control" id="intento" name="intento" placeholder="Palabra">
						</div>
						<div class="col-12 col-md-4">
							<button type="submit" name="intentar" class="btn btn-primary mb-3">Probar</button>
							<button type="submit" name="reset" class="btn btn-outline-secondary mb-3">Resetear</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>

</html>