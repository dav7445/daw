import bcrypt
from argon2 import PasswordHasher
import time

def bcrypt_hash(password):
    return bcrypt.hashpw(password, bcrypt.gensalt())

def check_bcrypt_hash(password, hashed):
    return bcrypt.checkpw(password, hashed)

def argon2_hash(password):
    return PasswordHasher().hash(password)

def check_argon2_hash(password, hashed):
    return PasswordHasher().verify(hashed, password)

password = b"password"

print("bcrypt hash:", bcrypt_hash(password))
print("bcrypt check:", check_bcrypt_hash(password, bcrypt_hash(password)))

print("argon2 hash:", argon2_hash(password))
print("argon2 check:", check_argon2_hash(password, argon2_hash(password)))
