const arrayMinDescendientes = [
	[1, 2400],
	[2, 2700],
	[3, 4000],
	[4, 4500],
];

function minimoFamiliar(descendientes) {
	/*
	let minimo = 0;
	for (i = 0; i < descendientes; i++) {
		if (i >= arrayMinDescendientes.length) {
			minimo += arrayMinDescendientes[3][1];
		} else if (arrayMinDescendientes[i][0] <= descendientes) {
			minimo += arrayMinDescendientes[i][1];
		}
	}
	*/
	/*
	let minimo = 0;
	for (i = 0; i < arrayMinDescendientes.length; i++) {
		if (i < arrayMinDescendientes.length - 1) {
			minimo += arrayMinDescendientes[i][1];
		} else {
			minimo += ((descendientes - i) * arrayMinDescendientes[i][1]);
		}
	}
	*/

	let minimo = arrayMinDescendientes.reduce(function(acumulador, item, index, array) {
		if (index < array.length - 1) {
			return acumulador + item[1];
		} else {
			return acumulador + ((descendientes - index) * item[1]);
		}
	}, 0);

	return minimo;
}

let hijos = 5;
console.log(`Total para ${hijos} hijos: ${minimoFamiliar(hijos)}€`);
