var arrayVentas = [
	[
		"Semana 1",
		["06/09/2021", "12/09/2021"],
		[
			[1, 0],
			[2, 2500],
			[3, 4500],
			[4, 5000],
			[5, 3000],
			[6, 2500],
			[7, 8000],
		],
	],
	[
		"Semana 2",
		["13/09/2021", "19/09/2021"],
		[
			[1, 0],
			[2, 3500],
			[3, 5800],
			[4, 2000],
			[5, 2000],
			[6, 1500],
			[7, 3000],
		],
	],
	[
		"Semana 3",
		["20/09/2021", "26/09/2021"],
		[
			[1, 0],
			[2, 3500],
			[3, 5800],
			[4, 2000],
			[5, 2000],
			[6, 1500],
			[7, 3000],
		],
	],
	[
		"Semana 4",
		["27/09/2021", "03/10/2021"],
		[
			[1, 0],
			[2, 3500],
			[3, 4800],
			[4, 2000],
			[5, 2500],
			[6, 1500],
			[7, 1000],
		],
	],
	[
		"Semana 5",
		["04/10/2021", "10/10/2021"],
		[
			[1, 0],
			[2, 3500],
			[3, 5800],
			[4, 2000],
			[5, 2000],
			[6, 1500],
			[7, 3000],
		],
	],
];

let arraySalida = [
	["Info 1 - Días de mayor y menor venta"],
	["Info 2 - Domingo supera media"],
	["Info 3 - Ventas totales y medias diarias"],
	["Info 4 - Ranking de ventas"],
	["Info 5 - Diferencia entre la media y la mejor"],
];

arraySalida[2].push([]);

for (i = 1; i <= 7; i++) {
	arraySalida[2][1].push([i, [0, 0]]);
}

arrayVentas.forEach((item, index) => {
	item.forEach((item2, index2) => {
		if (index2 == 0) {
			// Semana X
			// Info 1
			arraySalida[0].push([item2]);

			// Info 2
			arraySalida[1].push([item2]);
		} else if (index2 == 1) {
			// Fecha
			// Info 1
			arraySalida[0][index + 1].push([item2]);

			// Info 2
			arraySalida[1][index + 1].push([item2]);
		} else if (index2 == 2) {
			// Ventas
			// Info 1 | Mayor y menor
			let max = ["",0];
			let min = ["",Number.MAX_SAFE_INTEGER];
			let suma = 0;
			item2.forEach((item3, index3) => {
				suma += item3[1];
				if (item3[1] > max[1]) {
					max = item3;
				}
				if (item3[1] < min[1] && index3 != 0) {
					min = item3;
				}
			});
			arraySalida[0][index + 1].push([max[0], min[0]]);

			// Info 2 | Domingo sumera media?
			if (item[2][6][1] > suma / 6) {
				arraySalida[1][index + 1].push(true);
			} else {
				arraySalida[1][index + 1].push(false);
			}

			// Info 3 | Ventas totales y ventas medias
			item2.forEach((item3, index3) => {
				arraySalida[2][1][index3][1][0] += item3[1];
				arraySalida[2][1][index3][1][1] = arraySalida[2][1][index3][1][0];
			});
		}
	});
});

// Info 3 | ventas medias
arraySalida[2][1].forEach((item) => {
	item[1][1] /= arrayVentas.length;
});

// Info 4 - Ranking de ventas
let tempArr = JSON.parse(JSON.stringify(arraySalida[2][1])); // Truco para copiar sin referencia
tempArr.sort((a, b) => b[1][0] - a[1][0]);
tempArr.forEach((item, index) => {
	item.splice(0, 0, index + 1);
});
arraySalida[3].push(tempArr);
tempArr = []; // Limpiar el array para usarlo luego

// Info 5 - Ranking de ventas
arraySalida[2][1].forEach((item, index) => {
	tempArr.push([index + 1, 0]);
	tempArr[index][1] = item[1][1] - arraySalida[3][1][0][2][1]; // arraySalida[3][1][0][2][1] = la media mas alta
});
arraySalida[4].push(tempArr);

console.log(arraySalida);
