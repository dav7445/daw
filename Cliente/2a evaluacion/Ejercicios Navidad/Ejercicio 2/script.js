let tarjetas = [
	[4, 5, 3, 9, 6, 7, 7, 9, 0, 8, 0, 1, 6, 8, 0, 8],
	[5, 5, 3, 5, 7, 6, 6, 7, 6, 8, 7, 5, 1, 4, 3, 9],
	[3, 7, 1, 6, 1, 2, 0, 1, 9, 9, 8, 5, 2, 3, 6],
	[6, 0, 1, 1, 1, 4, 4, 3, 4, 0, 6, 8, 2, 9, 0, 5],
	[4, 9, 1, 3, 5, 4, 0, 4, 6, 3, 0, 7, 2, 5, 2, 3],
	[5, 7, 9, 5, 5, 9, 3, 3, 9, 2, 1, 3, 4, 6, 4, 3],
	[3, 7, 5, 7, 9, 6, 0, 8, 4, 4, 5, 9, 9, 1, 4],
	[5, 3, 8, 2, 0, 1, 9, 7, 7, 2, 8, 8, 3, 8, 5, 4],
	[4, 5, 3, 9, 6, 8, 9, 8, 8, 7, 7, 0, 5, 7, 9, 8],
	[3, 3, 8, 2, 0, 1, 9, 7, 7, 2, 8, 8, 3, 8, 5, 4],
	[3, 4, 4, 8, 0, 1, 9, 6, 8, 3, 0, 5, 4, 1, 4],
	[5, 4, 6, 6, 1, 0, 0, 8, 6, 1, 6, 2, 0, 2, 3, 9],
	[4, 5, 3, 2, 7, 7, 8, 7, 7, 1, 0, 9, 1, 7, 9, 5],
	[6, 0, 1, 1, 1, 2, 7, 9, 6, 1, 7, 7, 7, 9, 3, 5],
	[6, 0, 1, 1, 3, 7, 7, 0, 2, 0, 9, 6, 2, 6, 5, 6, 2, 0, 3],
	[4, 5, 3, 9, 4, 0, 4, 9, 6, 7, 8, 6, 9, 6, 6, 6],
	[4, 9, 2, 9, 8, 7, 7, 1, 6, 9, 2, 1, 7, 0, 9, 3],
];
/*let tarjetas = [
	[4, 1, 6, 8, 8, 1, 8, 8, 4, 4, 4, 4, 7, 1, 2, 3],
	[4, 1, 6, 8, 8, 1, 8, 8, 4, 4, 4, 4, 7, 1, 2, 4], // false
	[4, 8, 0, 0, 3, 5, 9, 3, 5, 8, 5, 9, 8, 4, 9, 2],
	[4, 8, 0, 0, 3, 5, 5, 4, 8, 9, 4, 5, 2, 6, 9, 0],
	[4, 8, 0, 0, 3, 2, 7, 2, 0, 2, 1, 2, 0, 7, 6, 3],
	[4, 8, 0, 0, 3, 1, 4, 7, 3, 1, 7, 9, 3, 9, 4, 8],
	[5, 3, 2, 6, 9, 1, 5, 3, 7, 9, 7, 6, 5, 5, 1, 9],
	[5, 3, 2, 6, 9, 1, 6, 2, 8, 4, 3, 5, 3, 8, 3, 9],
];*/

let entidades = [
	[3, "Amex (American Express)"],
	[4, "Visa"],
	[5, "Mastercard"],
	[6, "Otra"],
];

let arraySalida = [];

tarjetas.forEach((item) => {
	arraySalida.push([obtenerEntidad(item), item.join(""), tarjetaValida(item)]);
});

console.log(arraySalida);
console.log(datosPorEntidad(arraySalida));

function tarjetaValida(tarjeta) {
	// En vez de trabajar del final al principio, le doy la vuelta al array
	tarjeta.reverse();
	let sum = 0;
	tarjeta.forEach((digito, index) => {
		// Si el indice es impar lo multiplico x2
		if (index % 2 != 0) {
			digito *= 2;
			// Si el resultado de multiplicar es mayor que 9, resto 9
			if (digito > 9) {
				digito -= 9;
			}
		}
		// Suma cada digito
		sum += digito;
	});
	if (sum % 10 == 0) {
		return true;
	}
	return false;
}

function obtenerEntidad(tarjeta) {
	let entidad;
	entidades.forEach((item) => {
		if (item[0] == tarjeta[0]) {
			entidad = item[1];
		}
	});
	return entidad;
}

function datosPorEntidad(arraySalida) {
	let entidadesSalida = [];
	entidades.forEach((item) => {
		entidadesSalida.push([item[1], 0, 0, 0]);
	});
	arraySalida.forEach((item) => {
		entidadesSalida.forEach((item2, index2, array2) => {
			if (item[0] == item2[0]) {
				// Contar cada vez que aparece cada entidad
				array2[index2][1] += 1;
				// Contar la cantidad de false
				if (!item[2]) {
					array2[index2][2] += 1;
				}
			}
			// Calcular el % de false solo si hay algun false (evita dividir entre 0)
			if (array2[index2][1] != 0) {
				array2[index2][3] = array2[index2][2] / array2[index2][1];
			}
		});
	});
	return entidadesSalida;
}
