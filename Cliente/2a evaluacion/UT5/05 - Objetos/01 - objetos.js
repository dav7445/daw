// OBJETOS
// sintaxis literal

let objeto = {};

// definicion y propiedades
let cochePaula = {
	marca: "Opel",
	modelo: "Corsa",
	matricula: "U-000-NOSE",
	color: "blanco perlado",
	kilometros: 154256,
	fechaMatricula: 2007,
};

console.log(cochePaula.matricula);

cochePaula.combustible = "Gasolina";
cochePaula.caballos = undefined;

console.log(cochePaula.motorizacion); //undefined
console.log(cochePaula.combustible);
console.log(cochePaula.caballos);

// ¿Existe la propiedad?

console.log("caballos" in cochePaula);
console.log("motorización" in cochePaula);

// Añadiendo  propiedades con corchetes...

cochePaula["motorizacion"] = "TI 2000CC";

// Propiedades "dinámicas variables" ...

let nuevaPropiedad = "número de puertas";

cochePaula[nuevaPropiedad] = 5;

// Obtener todas las propiedades de un objeto

for (propiedad in cochePaula) {
	console.log(`propiedad ${propiedad} es: ${cochePaula[propiedad]}`);
}

// Hacer objetos a partir de una función...

function nuevoCoche(nombrePropietario, matricula, marca, modelo) {
	return {
		nombrePropietario,
		matricula,
		marca,
		modelo,
	};
}

let cocheDani = nuevoCoche("Dani", "O-2343-BB", "Seat", "Ibiza");
