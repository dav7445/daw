let cochePaula = {
	marca: "Opel",
	modelo: "Corsa",
	matricula: "U-000-NOSE",
	color: "blanco perlado",
	kilometros: 154256,
	fechaMatricula: 2007,
};

// Un método para "mostrar el coche"

cochePaula.mostrarCoche = function () {
	for (propiedad in cochePaula) {
		console.log(cochePaula[propiedad]);
	}
};

cochePaula.mostrarCoche = function () {
	for (propiedad in this) {
		console.log(this[propiedad]);
	}
};

cochePaula.actualizaKilometros = function (nuevoKilometraje) {
	this.kilometros = nuevoKilometraje;
};

cochePaula.mostrarCoche();

let cocheSara = {};

Object.assign(cocheSara, cochePaula);

cocheSara.color = "rojo";

cocheSara.mostrarCoche();

cocheSara.actualizaKilometros(90000);

let a = Object.keys(cocheSara);
