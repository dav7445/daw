let cochePaula = {
	marca: "Opel",
	modelo: "Corsa",
	matricula: "U-000-NOSE",
	color: "blanco perlado",
	kilometros: 154256,
	fechaMatricula: 2007,
};

// Copiar objeto

// no podemos asignarlo como variable

let cocheNuria = cochePaula;

cocheNuria.color = "plata";

console.log("Color coche de Nuria: ", cocheNuria.color);
console.log("Color coche de Paula: ", cochePaula.color);

console.log(cocheNuria === cochePaula);

// copiamos las propiedades

cocheNuria = {};

for (propiedad in cochePaula) {
	cocheNuria[propiedad] = cochePaula[propiedad];
}

// Utilizando el método assign para  copiar propiedades.

Object.assign(cocheNuria, cochePaula);

// Creamos un objeto motor

let motorOpelCorsa2000 = {
	combustible: "Gasolina",
	centimetrosCubicos: 2000,
	cilindros: 4,
	turbo: false,
};

// El método object.assign no copia los datos de los objetos incluidos

cochePaula.motor = motorOpelCorsa2000;

let cocheVictor = {};
Object.assign(cocheVictor, cochePaula);
