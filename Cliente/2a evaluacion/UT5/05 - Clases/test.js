class PersonalSanitario {
	// funcion constructora
	constructor(nombre, tipo) {
		this.nombre = nombre;
		this.tipo = tipo;
	}

	// metodo de la clase
	mostrar() {
		console.log("Nombre: " + this.mostrar);
	}

	set tipo(valor) {
		switch (valor) {
			case "enfermera":
			case "medico":
			case "auxiliar":
				this._tipo = tipo;
				break;
			default:
				this._tipo = undefined;
		}
	}

	get tipo() {
		return this._tipo;
	}

	set nombre(valor) {
		this._nombre = valor;
	}

	get nombre() {
		return this._nombre;
	}
}

const medico = new PersonalSanitario("Maria", "medico");

class Medico extends PersonalSanitario {

	static especialidades = ["Cardiologia", "Hematologia", "Nefrologia", "General"]

	constructor (nombre, especialidad) {
		super(nombre, "medico");
		this.especialidad = especialidad;
	}
	
	set especialidad(valor) {
		this._especialidad = valor;
	}

	get especialidad() {
		return this._especialidad;
	}
}

let nuria = new Medico("Nuria", "Hematologia");
