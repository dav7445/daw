function Autor(numero, nombre) {
	this.numero = numero;
	this.nombre = nombre;
	this.mostrar = function () {
		let atributo;
		for (let elemento in this) {
			console.log(`${elemento}: ${this[elemento]}`);
			atributo = Object.getOwnPropertyDescriptor(autorPrueba, elemento);
			for (let caracter in atributo) {
				console.log(`${caracter}: ${atributo[caracter]}`)
			}
		}
	}
}
let autorPrueba = new Autor(1, "prueba");

let prueba = Object.getOwnPropertyDescriptor(autorPrueba, "numero");

Object.defineProperty(autorPrueba, "nacionalidad", {value: "Sudafricano"});

console.log(autorPrueba);

