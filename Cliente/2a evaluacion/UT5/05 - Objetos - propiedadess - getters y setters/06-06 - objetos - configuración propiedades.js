// Configuración de las propiedades de objetos //
//                                             //

function Inmueble(id, tipo, direccion) {
	this.id = id;
	this.tipo = tipo;
	this.direccion = direccion;

	this.mostrar = function () {
		for (let car in this) {
			console.log(`${car} :=> ${this[car]}`);
		}
	};
}

let inmuebleGijon = new Inmueble(1, "vivienda", "El Coto");

/*

Las propiedades de los objetos tienen 4 atributos:

1. El valor, por ejemplo la propiedad 'tipo' de 'inmuebleGijon' tiene el valor "vivienda";

El resto son atributos especiales, que podemos cambiar:

2. writable. Si es true, el valor de la propiedad se puede cambiar.
3. enumerable. Si es true, la propiedad aparece en los bucles, for...in
4. configurable. Si es true, la propiedad se puede borrar y los atributos especiales se pueden modificar.

En principio, se establecen por defecto a a true.

propertyDescriptor es un objeto que muestra la información de los 4 atributos.

*/

let atributo = Object.getOwnPropertyDescriptor(inmuebleGijon, "tipo");

console.log(atributo);

/* SALIDA 
{value: 'vivienda', writable: true, enumerable: true, configurable: true}
configurable: true
enumerable: true
value: 'vivienda'
writable: true
[[Prototype]]: Object
*/

// Podemos crear una propiedad definiéndola previamente
//

// Añadimos la propiedad 'superficie' del inmueble.

Object.defineProperty(inmuebleGijon, "superficie", {
	value: 200,
	enumerable: true,
});

// Mostramos el inmueble y comprobamos que "superficie" es una nueva propiedad del objeto

inmuebleGijon.mostrar();

// Consultamos los atributos de esta nueva propiedad

atributo = Object.getOwnPropertyDescriptor(inmuebleGijon, "superficie");

console.log(atributo);

/*

Al crear la propiedad con defineProperty, sino damos valor a los atributos se crean con false

configurable: false
enumerable: true
value: 200
writable: false

En este caso, no podríamos cambiar los atributos de la propiedad, ni su valor.

*/

inmuebleGijon.superficie = 230; // no cambia, ya que esta propiedad no es "writable"

/*
Si no queremos que nuestros métodos se muestren en inmuebleGijon.mostrar(), por ejemplo,
establecemos enumerable a false.
*/

Object.defineProperty(inmuebleGijon, "mostrar", {
	enumerable: false,
});

inmuebleGijon.mostrar(); // Ahora el bucle for...in de mostrar() ya no itera sobre la propiedad "mostrar" y
// por lo tanto, ya no se muestra.
