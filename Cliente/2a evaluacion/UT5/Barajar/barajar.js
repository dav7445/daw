/*
Para el desarrollo de un juego, nos han pedido un método que permita barajar el contenido de un array cualquiera.
Por ejemplo, dado un array:
baraja = [1,2,3,4,5,6,7,8];
Poder realizar la siguiente operación:
barajada = baraja.barajar();
Nos devuelva, por ejemplo la baraja siguiente:
barajada = [1, 8, 4, 3, 5, 6, 2, 7];
Para ello utilizaremos el prototipo de los objetos estándar Array.
Array.prototype.barajar = function() {}
*/

baraja = [1, 2, 3, 4, 5, 6, 7, 8];

Array.prototype.barajar = function () {
	let copy = structuredClone(this) // Clonar el array (Node 17)
	copy.forEach((elemento, indice, array) => {
		let randomPos = Math.floor(Math.random() * array.length);
		let elementoNuevaPos = array[randomPos];
		array[randomPos] = elemento;
		elemento = elementoNuevaPos;
	});
	return copy;
};

barajada = baraja.barajar();

console.log(`Baraja: ${baraja}\nBarajada: ${barajada}`);
