class Blog {
	static TIPOS_ELEMENTO = [{ tipo: "texto" }, { tipo: "imagen" }, { tipo: "video" }];
	static TAGS = ["deporte", "sociedad", "salud", "economia", "nacional", "internacional"];

	constructor(nombre) {
		this.nombre = nombre;
		this._entradas = [];
		this._tags = Blog.TAGS;
		this._tiposElemento = Blog.TIPOS_ELEMENTO;
	}

	get nombre() {
		return this._nombre;
	}
	set nombre(valor) {
		this._nombre = valor;
	}

	static nuevoTAG(nombre) {
		this.TAGS.push(nombre);
		return nombre;
	}

	nuevaEntrada(fecha, titulo, autor, tags) {
		let entrada = new Entrada(titulo, fecha, autor, tags);
		this._entradas.push(entrada);
		return entrada;
	}

	seleccionarEntradasPorTag(tag) {
		return this._entradas.filter((elemento) => elemento._tags.includes(tag));
	}
}

class Entrada {
	constructor(titulo, fecha, autor, tags) {
		this.titulo = titulo;
		this.fecha = fecha;
		this.autor = autor;
		this._tags = [];
		this._elementos = [];
		this._elementoRaiz = null;
		this.etiquetar(tags);
	}

	get titulo() {
		return this._titulo;
	}
	set titulo(valor) {
		this._titulo = valor;
	}

	get fecha() {
		return this._fecha;
	}
	set fecha(valor) {
		this._fecha = valor;
	}

	get autor() {
		return this._autor;
	}
	set autor(valor) {
		this._autor = valor;
	}

	get elementoRaiz() {
		return this._elementoRaiz;
	}

	etiquetar(tag) {
		if (Array.isArray(tag)) {
			tag.forEach((element) => {
				this._tags.push(element);
			});
		} else {
			this._tags.push(tag);
		}
	}

	crearElementoRaiz(tipo, contenido) {
		let elemento = new Elemento(tipo, contenido);
		this._elementos.push(elemento);
		this._elementoRaiz = elemento;
		return elemento;
	}
}

class Elemento {
	constructor(tipo, contenido) {
		this._tipo = tipo;
		this._id = Math.floor(Math.random() * 10000000);
		this.contenido = contenido;
		this._subelementos = [];
	}

	get contenido() {
		return this._contenido;
	}
	set contenido(valor) {
		this._contenido = valor;
	}

	nuevoSubElemento(tipo, contenido) {
		let subelemento = new Subelemento(tipo, contenido);
		this._subelementos.push(subelemento);
		return subelemento;
	}
}

class Subelemento extends Elemento {
	constructor(tipo, contenido) {
		super(tipo, contenido);
	}
}

// 1. Añadir 3 nuevos tags a la propiedad estática TAGS
Blog.nuevoTAG("Tag1");
Blog.nuevoTAG("Tag2");
Blog.nuevoTAG("Tag3");

// 2. Crear un Blog
let blog1 = new Blog("Blog1");

// 3. Añadir una entrada al Blog
let entrada1 = blog1.nuevaEntrada(new Date(), "Entrada1", "Autor1", ["Tag1", "Tag2"]);
let entrada2 = blog1.nuevaEntrada(new Date(), "Entrada2", "Autor2", "Tag3");

// 4. Añadir un elemento raíz a la entrada recién creada.
let elementoraiz1 = entrada1.crearElementoRaiz("Tipo1", "Contenido1");

// 5. Añadir dos subelementos al elemento raíz.
let subelemento1 = elementoraiz1.nuevoSubElemento("SubTipo1", "SubContenido1");
let subelemento2 = elementoraiz1.nuevoSubElemento("SubTipo2", "SubContenido2");

// 6. Imprimir por consola el blog creado. Console.log
console.log(blog1);

entrada2.etiquetar("Tag1");

console.log(blog1.seleccionarEntradasPorTag("Tag1"));

console.log(entrada1.elementoRaiz);

console.log();

module.exports = Blog;
