const Blog = require("./blog.js");

function log(mensaje, correcto) {
	if (!correcto) {
		console.error(mensaje);
	} else {
		console.log(mensaje);
	}
}

const tagACrear = "motor";
const nombreBlog = "El tiempo en mi casa";
const entradaBlog = [
	{
		fecha: new Date(),
		titulo: "probando",
		autor: "yomismo",
		tags: ["hogar", "salud", "dinero", "corazón"],
	},
	{
		fecha: new Date(),
		titulo: "segunda prueba",
		autor: "yomismo",
		tags: ["fotografía", "cocina"],
	},
	{
		fecha: new Date(),
		titulo: "tercera prueba",
		autor: "yomismo",
		tags: ["formula1", "motoGP", "Roland Garros", "motor"],
	},
];

const retornoConsultaTags = ["probando", "tercera prueba"];

const datosElementoRaiz = {
	tipo: "texto",
	contenido: "Este artículo es lo mismo de siempre",
};

const datosElementoImagen = {
	tipo: "imagen",
	contenido: "Esta es la imagen del artículo",
};

const etiquetasEntrada = ["comida", "bebida", "música", "motor"];

let miBlog;
let entrada = [];
let elementoRaiz;
let elementoImagen;

log("0 - tipos elemento array objetos", typeof Blog.TIPOS_ELEMENTO[0] === "object");

Blog.nuevoTAG(tagACrear);
log("1 - añadir 1 tag: " + tagACrear + " a la propiedad Estática TAGS", Blog.TAGS.includes(tagACrear));

miBlog = new Blog(nombreBlog);
log("2.1 - devuelve un blog creado - nombre: " + nombreBlog, nombreBlog === miBlog.nombre);
log("2.2 - array de entradas ", Array.isArray(miBlog._entradas));
log("2.3 - array de entradas vacío", miBlog._entradas.length === 0);

// 3 - crear entradas en el blog, ()

for (let i = 0; i < entradaBlog.length; i++) {
	entrada.push(miBlog.nuevaEntrada(entradaBlog[i].fecha, entradaBlog[i].titulo, entradaBlog[i].autor, entradaBlog[i].tags));
}

for (let i = 0; i < entradaBlog.length; i++) {
	log("3 - crear entradas en el blog: " + i + " : " + entrada[i].titulo, entrada[i].titulo === entradaBlog[i].titulo);
}

// 4 - etiquetar entradas

for (let i = 0; i < etiquetasEntrada.length; i++) {
	entrada[0].etiquetar(etiquetasEntrada[i]);
}

for (let i = 0; i < etiquetasEntrada.length; i++) {
	log("4 - etiquetar entradas: " + i + ": " + etiquetasEntrada[i], entrada[0]._tags.includes(etiquetasEntrada[i]));
}

// 5 - crear elemnto raiz

elementoRaiz = entrada[0].crearElementoRaiz(datosElementoRaiz.texto, datosElementoRaiz.contenido);

log("5 - crear elemnto raiz", elementoRaiz.contenido === datosElementoRaiz.contenido);

//  6 - añadir subElemento

elementoImagen = elementoRaiz.nuevoSubElemento(datosElementoImagen.tipo, datosElementoImagen.contenido);

log("6 - añadir subElemento", elementoImagen.contenido === datosElementoImagen.contenido);

// 7 - seleccionarEntradasPorTag

let entradasTag = miBlog.seleccionarEntradasPorTag("motor");

log("7 - seleccionarEntradasPorTag: entradas tageadas: " + entradasTag.length, entradasTag.length === retornoConsultaTags.length);

for (let i = 0; i < entradasTag.length; i++) {
	log("7 - seleccionarEntradasPorTag: " + i + " : " + retornoConsultaTags[i], entradasTag[i].titulo === retornoConsultaTags[i]);
}

console.log(miBlog);

console.log();
