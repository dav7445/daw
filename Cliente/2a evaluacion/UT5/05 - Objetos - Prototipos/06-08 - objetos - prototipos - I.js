// El PROTOTIPO I - Herencia prototípica
/*
 Todos los objetos tienen una propiedad oculta y especial que se denomina
 [[Prototype]]

*/

let estudiante = {
	nombre: "Saturnino",
};

console.log(estudiante);

/* Salida - objeto definido - estudiante
    {nombre: 'Saturnino'}
    nombre:'Saturnino'
    [[Prototype]]:Object
*/

// También podemos verlo en objetos del lenguaje

let unArray = [1, 2];
console.log(unArray);

/* Salida - array
    0:1
    1:2
    [[Prototype]]:Object
    length:2
    [[Prototype]]:Array(0)
*/

let suma = (a, b) => a + b;

console.log(suma);

/* Salida - función
    suma
    (a,b) => a+b
    arguments (get): ƒ ()
    arguments (set): ƒ ()
    caller (get): ƒ ()
    caller (set): ƒ ()
    length: 2
    name: 'suma'
    [[FunctionLocation]]: ...
    [[Prototype]]: ƒ ()
    [[Scopes]]: Scopes[1] ...
*/

/* UTILIZACIÓN DEL PROTOTIPO

La propiedad [[Prototype]], es el mecanismo que permite la herencia de
propiedades y métodos entre objetos. 

Cuando no se encuentra una propiedad en un objeto, JavaScript la busca en su
prototipo.

Para acceder a la propiedad, se utiliza el nombre especial:
__proto__

*/

let animal = {
	esSerVivo: true,
};

Object.defineProperty(animal, "mostrar", {
	value: function () {
		for (propiedad in this) {
			console.log(`${propiedad} => ${this[propiedad]}`);
		}
	},
	enumerable: false,
});

let mamifero = {
	tieneSangreCaliente: true,
};

// Establecemos que mamifero herede las propiedades de animal

mamifero.__proto__ = animal; // 'mamifero' tiene como prototipo a 'animal'

// De esta forma podemos acceder a los métodos y propiedades del prototipo

console.log(mamifero.esSerVivo); // true

mamifero.mostrar();

// ¿Qué hace la siguiente instrucción?

mamifero.esSerVivo = false;

// Esta propiedad no existe en el objeto, ¿va a buscarla al prototipo?
//
// No va a buscarla, al no existir, la crea en el propio objeto, por lo tanto,
// la próxima vez que acceda la tomará del objeto, no del prototipo.
//
// Comprobémeslo:

console.log(mamifero.hasOwnProperty("esSerVivo")); // Devuelve 'true'

// Object.hasOwnProperty(), nos permite comprobar si una propiedad es del objeto
// o heredada.

// Creemos otro objeto

let ave = {
	tieneSangreCaliente: true,
	tienePlumas: true,
	__proto__: animal,
};

ave.mostrar();

console.log(ave.hasOwnProperty("esSerVivo")); // Devuelve 'false'

// Volvamos a "revivir" al mamífero (borramos la propiedad)

delete mamifero.esSerVivo;

console.log(mamifero.hasOwnProperty("esSerVivo")); // Devuelve 'false'

// Por lo tanto, la herencia de propiedades del prototipo tiene sentido en los procesos
// de lectura. A no ser que se utilicen métodos setter. En ese caso la herencia es completa, a través de los
// setter y getter.

/* HERENCIA A TRAVÉS DE LA CADENA DE PROTOTIPOS */

// Es posible seguir enlazando o encadenando herencias a través del prototipo

let perro = {
	dieta: "carnívora",
	longevidad: 10,
	sonido: "ladrido",
	__proto__: mamifero,
};

// 'perro' hereda las propiedades de 'mamifero' que, a su vez, las hereda de 'animal'.
