// El PROTOTIPO II - Herencia prototípica
/*

La utilización de la propiedad __proto__ está en desuso, antes de ver el modo más
actual de ver los Objetos a partir de las Clases de JavaScript, revisamos el uso de los
prototipos mediante las funciones constructoras.

En el código anterior, PROTOTIPO I, vimos __proto__ y prototipos a través de los objetos.

*/

// 'Construyamos' animales
//
// Por abreviar, no utilizo setters y getters.

// La función constructora, devuelve un objeto,
// no es un contenedor de las propiedades del mismo
//

function Vertebrado(familia, genero, especie, nombreComun) {
	this.familia = familia;
	this.genero = genero;
	this.especie = especie;
	this.nombreComun = nombreComun;
}

// La constante 'mamifero' es un objeto 'instanciado a través de la función
// constructora 'Vertebrado', es una 'instancia'

let mamifero = new Vertebrado("mamifero"); // Objeto creado a través de constructor

// Creamos otro objeto de similares características utilizando la "declaración literal"

let otroMamifero = {
	familia: "mamifero",
	genero: undefined,
	especie: undefined,
	nombreComun: undefined,
};

// En cualquier caso, existe la propiedad __proto___ (que no debemos utilizar)

// ¿Quién es el prototipo de mamifero?
// Podemos verlo con la propiedad __proto__, o utilizando el método estándar
//
// Object.getPrototypeOf()

console.log(Object.getPrototypeOf(mamifero));
console.log(mamifero.__proto__); // sintaxis a evitar

// Salida {constructor:ƒ } Vertebrado(familia, genero, especie, nombreComun).
//
// Devuelve un objeto que incluye
// la función (constructor) que es la que utilizamos para crear o instanciar el objeto
//

// Sin embargo el prototipo de otroMamifero, objeto que hemos creado mediante literal

console.log(Object.getPrototypeOf(otroMamifero));

// Salida {constructor: ƒ, __defineGetter__: ƒ, __defineSetter__: ƒ, hasOwnProperty: ƒ, __lookupGetter__: ƒ, …}
//
// Devuelve un objeto con una
// función (constructor) que es la base de cualquier objeto.
//
// Comprobémoslo:
//
// Creamos un objeto con el constructor de objetos:

nuevoObjeto = new Object();

prototipoOtroMamifero = Object.getPrototypeOf(otroMamifero); // igual que otroMamifero.__proto__
prototipoNuevoObjeto = Object.getPrototypeOf(nuevoObjeto); // igual que nuevoObjeto.__proto__

console.log(prototipoNuevoObjeto === prototipoOtroMamifero); // Devuelve true.

// Como en JavaScript todo son objetos, al final hay un "prototipo" que es la base de todos ellos
//

// Extendiendo el prototipo
//
// Podemos añadir nuevos métodos a nuestro constructor (prototípico)

Vertebrado.prototype.sonido = "";
Vertebrado.prototype.emitirSonido = function () {
	console.log(this.sonido);
};

Vertebrado.prototype.mostrar = function () {
	console.log(this.familia, this.genero, this.especie, this.nombreComun, this.sonido);
};

mamifero.emitirSonido(); // Salida vacía

mamifero.sonido = "depende";

mamifero.mostrar();

// Podemos modificar el prototipo del objeto creado anteriormente

Object.setPrototypeOf(otroMamifero, Vertebrado.prototype);

otroMamifero.mostrar();

// Ahora los dos objetos tienen el mismo prototipo.

console.log(Object.getPrototypeOf(otroMamifero));
console.log(Object.getPrototypeOf(mamifero));
