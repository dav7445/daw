/* Modelo de Biblioteca .
Una Biblioteca contiene una colección de ejemplares (libros):
1. Cada ejemplar tiene una signatura en la biblioteca que permite su identificación
2. Cada ejemplar tiene una ubicación (por ejemplo, zona, librería, estantería)
3. Cada ejemplar es de una Publicación.
4. Cada Publicación tiene un código ISBN, un título y un conjunto de autores (1 ó más).
5. Los autores de identifican por un número y un nombre.
6. La Biblioteca tiene un conjunto de Lectores, que identificamos por DNI y nombre.
7. Se registran los préstamos de ejemplares de Los lectores.
8. Se registran las devoluciones de los préstamos.

Objetos
=======
Biblioteca
Publicaciones
Autores
Ejemplares
Lectores
Préstamos
El objeto Biblioteca mantiene las siguientes colecciones (arrays):
publicaciones => Todas las publicaciones que maneja la biblioteca.
ejemplares => Los ejemplares referenciados catalogados por su signatura
lectores => Los lectores que toman a préstamo los ejemplares de la biblioteca
prestamos => Cada uno de los préstamos realizados por cada lector.

Métodos:
Actualización
=============
nuevaPublicacion(isbn, titulo)
nuevoEjemplar(signatura, publicacion, ubicacion)
nuevoLector(dni, nombre)
nuevoPrestamo(ejemplar, lector)
procesaPrestamo(ejemplar, lector) => busca si el ejemplar está disponible y si lo está ejecuta nuevoPrestamo
devuelvePrestamo(signatura) => procesa la devolución del ejemplar.

Consultas y listados
====================
ejemplaresTitulo(titulo) => devuelve array con todos los ejemplares correspondiente al título de la publicación
ejemplaresDisponiblesTitulo(titulo) => igual que el anterior, pero devuelve sólo los disponibles.
seleccionarEjemplar(signatura) => devuelve el ejemplar correspondiente a la signatura
listarEjemplares(coleccion o array) => lista todos los ejemplares del array por consola, por defecto, todos los ejemplares de la biblioteca.
listarLectores => lista por consola todos los lectores de la bibilioteca.
listarPrestamos(ejemplar) => lista por consola todos los préstamos de un ejemplar
El objeto Ejemplar, tiene el método Ficha() que muestra por consola la ficha del mismo.
El objeto Publicación, mantiene un array con los autores de cada publicación
*/
// Ejemplo de uso:

/*
const biblioGijon = new Biblioteca("Gijón");
let publicacion = biblioGijon.nuevaPublicacion("0131103628", "The C Programming Language");
publicacion.nuevoAutor(1, "Briwan W. Kernighan");
biblioGijon.nuevoEjemplar("C/12", publicacion, "b12-n");
publicacion = biblioGijon.nuevaPublicacion("97818479411007", "Scrum: The Art of Doing Twice the Work in Half the Time", ["Jeff Shuterland"]);
biblioGijon.listarEjemplares();
biblioGijon.listarEjemplares(biblioGijon.ejemplaresDisponiblesTitulo("The C Programming Language"));
let ejemplar = biblioGijon.seleccionarEjemplar("C/12");
ejemplar.ficha();
const maria = biblioGijon.nuevoLector("aaaaaaaaa", "María Garrido");
biblioGijon.listarLectores();
biblioGijon.procesaPrestamo("The C Programming Language", maria);
biblioGijon.listarPrestamos(ejemplar);
biblioGijon.devuelvePrestamo("C/14");
*/

function Biblioteca(nombre) {
	this.nombre = nombre;
	this.publicaciones = [];
	this.ejemplares = [];
	this.lectores = [];
	this.prestamos = [];

	this.nuevaPublicacion = function (isbn, titulo) {
		let publicacion = new Publicacion(isbn, titulo);
		this.publicaciones.push(publicacion);
		return publicacion;
	};

	this.nuevoEjemplar = function (signatura, publicacion, ubicacion) {
		let ejemplar = new Ejemplar(signatura, publicacion, ubicacion);
		this.ejemplares.push(ejemplar);
		return ejemplar;
	};

	this.nuevoLector = function (dni, nombre) {
		let lector = new Lector(dni, nombre);
		this.lectores.push(lector);
		return lector;
	};

	this.ejemplaresTitulo = function (titulo) {
		return this.ejemplares.filter((ejemplar) => ejemplar.publicacion.titulo === titulo);
	};

	this.ejemplaresDisponiblesTitulo = function (titulo) {
		return this.ejemplares.filter((ejemplar) => ejemplar.publicacion.titulo === titulo && ejemplar.disponible);
	};

	this.seleccionarEjemplar = function (signatura) {
		return this.ejemplares.find((ejemplar) => ejemplar.signatura === signatura);
	};

	this.nuevoPrestamo = function (ejemplar, lector) {
		if (ejemplar.disponible) {
			let nuevoPrestamo = new Prestamo(ejemplar, lector);
			this.prestamos.push(nuevoPrestamo);
			return nuevoPrestamo;
		} else {
			console.error("Publicación no disponible");
			return null;
		}
	};

	this.procesaPrestamo = (titulo, lector) => {
		let ejemplar = this.ejemplares.find((ejemplar) => ejemplar.publicacion.titulo === titulo && ejemplar.disponible);

		if (ejemplar === undefined) {
			console.error("Publicación no disponible");
			return null;
		} else {
			nuevoPrestamo = this.nuevoPrestamo(ejemplar, lector);
			return this.nuevoPrestamo;
		}
	};

	this.devuelvePrestamo = (signatura) => {
		let prestamo = this.prestamos.find((prestamo) => prestamo.ejemplar.signatura === signatura && !prestamo.ejemplar.disponible);

		if (prestamo === undefined) {
			console.error("No hay ningun prestamo con esa signatura");
		} else {
			prestamo.fechaFinal = new Date();
			prestamo.ejemplar.disponible = true;
		}
	};

	this.listarEjemplares = function () {
		console.log("Ejemplares: ");
		this.ejemplares.forEach((ejemplar) => console.log(ejemplar));
	};

	this.listarLectores = function () {
		console.log("Lectores: ");
		this.lectores.forEach((lector) => console.log(lector));
	};
}

function Ejemplar(signatura, publicacion, ubicacion) {
	this.signatura = signatura;
	this.publicacion = publicacion;
	this.ubicacion = ubicacion;
	this.disponible = true;

	this.ficha = function () {
		console.log("Signatura: " + this.signatura);
		console.log("Titulo: " + this.publicacion.titulo);
		console.log("Ubicacion: " + this.ubicacion);
	};
}

function Lector(dni, nombre) {
	this.dni = dni;
	this.nombre = nombre;
}

function Autor(numero, nombre) {
	this.numero = numero;
	this.nombre = nombre;
}

function Publicacion(isbn, titulo) {
	this.isbn = isbn;
	this.titulo = titulo;
	this.autores = [];

	this.nuevoAutor = function (numero, nombre) {
		let autor = new Autor(numero, nombre);
		this.autores.push(autor);
		return autor;
	};
}

function Prestamo(ejemplar, lector) {
	this.ejemplar = ejemplar;
	this.lector = lector;
	this.fechaInicio = new Date();
	this.fechaFinal = "";
	this.ejemplar.disponible = false;
}

const biblioGijon = new Biblioteca("Gijón");

let cProgramming = biblioGijon.nuevaPublicacion("0131103628", "The C Programming Language");

biblioGijon.nuevoEjemplar("C/12", cProgramming, "b12-n");

cProgramming.nuevoAutor(1, "Briwan W. Kernighan");
cProgramming.nuevoAutor(2, "Dennis M. Ritchie");

const maria = biblioGijon.nuevoLector("aaaaaaaaa", "María Garrido");

console.log(biblioGijon.ejemplaresTitulo("The C Programming Language"));

console.log(biblioGijon.ejemplaresDisponiblesTitulo("The C Programming Language"));

console.log(biblioGijon.seleccionarEjemplar("C/12"));

biblioGijon.procesaPrestamo("The C Programming Language", maria);

biblioGijon.devuelvePrestamo("C/12");

console.log();
