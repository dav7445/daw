// ARROW FUNCTIONS - FUNCIONES FLECHA

function sumaDeclaracion(a, b) {
	return a + b;
}

let sumaExpresion = function (a, b) {
	return a + b;
};

let sumaFlecha = (a, b) => a + b;

console.log(sumaDeclaracion(4, 5));
console.log(sumaExpresion(4, 5));
console.log(sumaFlecha(4, 5));

let suma10 = (n) => n + 10;

console.log(suma10(20));

let circulo5 = () => 2 * Math.PI * 5;

console.log(circulo5());
