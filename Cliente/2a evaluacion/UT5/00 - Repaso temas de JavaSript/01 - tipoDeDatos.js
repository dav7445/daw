// TIPOS DE DATOS

// PRIMITIVOS
String;
Boolean;
Number;
BigInt;
Symbol;

// Especiales

null;
undefined;

//NO PRIMITIVO

Object;

// Tipos de objeto
Array; // Es un objeto
Function; // Es un objeto

a = 3.45;
b = 20n;
c = "hola";
d = false;
e = null;
f = undefined;
g = {};
h = Symbol();
i = [];

function j(a) {
	return a * 2;
}

k = j;

console.log(typeof a);
console.log(typeof b);
console.log(typeof c);
console.log(typeof d);
console.log(typeof e);
console.log(typeof f);
console.log(typeof g);
console.log(typeof h);
console.log(typeof i);
console.log(typeof j);
console.log(typeof k);
