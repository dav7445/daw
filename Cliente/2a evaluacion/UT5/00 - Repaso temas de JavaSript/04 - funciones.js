// ALCANCE DE LAS VARIABLES CON LAS FUNCIONES

// PARÁMETROS POR DEFECTO EN LAS FUNCIONES

function holaMundoDefecto(mensaje = "hola") {
	//if (mensaje === undefined) {
	//  mensaje = "hola";
	// }
	// mensaje = mensaje || "hola"; // undefined => "hola"
	// mensaje = mensaje ?? "hola"; // undefined o null => "hola"
	console.log(mensaje);
}

holaMundoDefecto();
holaMundoDefecto("hola");

// FUNCIONES ANIDADAS

function holaYAdios(nombre, apellido) {
	function nombreCompleto() {
		return nombre + " " + apellido;
	}
	console.log("Hola amigo, " + nombreCompleto());
	console.log("Adiós colega" + nombreCompleto());
}

holaYAdios("Pedro", "Piqueras");
// nombreCompleto(); //ReferenceError: nombreCompleto is not defined

// EXPRESIONES DE FUNCION

let miFuncion = function () {
	console.log("hola");
};

miFuncion();

let nuevaFuncion = miFuncion;

nuevaFuncion();

console.log(suma(2, 3)); // hoisting

function suma(a, b) {
	return a + b;
}

console.log(multiplica(3, 4)); // ReferenceError: Cannot access 'multiplica' before initialization

let multiplica = function (a, b) {
	return a * b;
};
