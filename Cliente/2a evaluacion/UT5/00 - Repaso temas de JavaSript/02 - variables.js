// var - let - const
//

console.log(x); //ReferenceError: x is not defined

var x = 6; //hoisting...sube la declaración al inicio del script

console.log(x);

// console.log(y); //Cannot access 'y' before initialization

let y = 10;

var x = "hola";

console.log(x);

// let y = "hola";

// ALCANCE Y VISIBILIDAD

var c = 0;
{
	var d = 1;
	console.log(c, d);
}
console.log(c, d);

let w = 0;
{
	let z = 1;
	console.log(w, z);
}
// console.log(w,z); //ReferenceError: z is not defined

let i;
for (let i = 0; i <= 5; i++) {
	console.log(i);
}
console.log(i);

const color = "rojo";

const arrayDani = [];

arrayDani[0] = 10;

{
	const color = "verde";
	console.log(color);
}

console.log(color);

var a = 0;
var b = 2;

function miSuma(x, y) {
	var a = x + y;
	return a * b;
}

console.log(miSuma(2, 3));
console.log(a);
