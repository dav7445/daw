"use strict";

// a = 0; //ReferenceError: a is not defined
// console.log(a);

{
	console.log(resta(2, 3));
	function resta(a, b) {
		return a - b;
	}
}

console.log(resta(34, 35)); // ReferenceError: resta is not defined
