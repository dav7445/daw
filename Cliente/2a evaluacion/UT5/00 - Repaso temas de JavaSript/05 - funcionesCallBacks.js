// CALLBACKS

function suma(a, b) {
	return a + b;
}

function resta(a, b) {
	return a - b;
}

let multiplica = function (a, b) {
	return a * b;
};

function resultado(operacion, v1, v2) {
	return operacion(v1, v2);
}

console.log(resultado(suma, 3, 4));

console.log(
	resultado(
		function (a, b) {
			return a / b;
		},
		27,
		3
	)
);

console.log(resultado((a, b) => a / b, 27, 3));

function multiplicador(x) {
	return function (y) {
		return x * y;
	};
}

console.log(multiplicador(4));

let multiplicaPor4 = multiplicador(4);
let multiplicaPor17 = multiplicador(17);

console.log(multiplicaPor4(8));
console.log(multiplicaPor17(32));
