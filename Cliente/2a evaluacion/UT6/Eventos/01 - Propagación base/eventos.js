function gestionarEvento(evento, objeto) {
    console.log("Tipo de evento: ", evento.type);
    console.log("Target - elemento que lanza evento: ",evento.target);
    console.log("CurrenTarget - elemento que escucha evento: ",evento.currentTarget);
    evento.stopPropagation();
    evento.preventDefault();
}

let hijos = document.getElementsByClassName("hijo");
for (let hijo of hijos) {
    hijo.onclick = gestionarEvento;
    hijo.onmouseover = changeElementColor.bind(null, hijo);
    hijo.onmouseout = changeElementColor.bind(null, hijo);
}

let nietos = document.getElementsByClassName("nieto");
for (let nieto of nietos) {
    nieto.addEventListener("click", gestionarEvento);
    nieto.addEventListener("mousemove", changeElementColor.bind(null, nieto));
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function changeElementColor(element) {
    element.style.backgroundColor = getRandomColor();
}
