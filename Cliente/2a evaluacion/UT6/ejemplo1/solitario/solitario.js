class Carta {
	constructor (valor, palo, color, visible = false) {
		this.valor = valor;
		this.palo = palo;
		this.color = color;
		this.visible = visible;
	}

	// metodo voltear (cambia carta visible-invisible)
}
class Baraja {
	static palos = [
		{nombre: 'corazones', color: 'rojo'},
		{nombre: 'diamantes', color: 'rojo'},
		{nombre: 'treboles', color: 'negro'},
		{nombre: 'picas', color: 'negro'},
	];
	constructor (nombre, numeroCartas) {
		this.nombre = nombre;
		this.numeroCartas = numeroCartas;
		this.cartas = [];
		this.generarCartas = function () {
			let numeroPalos = Baraja.palos.length;
			let cartasPalo = this.numeroCartas / numeroPalos;
			for (let i = 0; i < numeroPalos; i++) {
				for (let j = 0; j < cartasPalo; j++) {
					this.cartas.push(new Carta(j, Baraja.palos[i].nombre, Baraja.palos[i].color))
				}
			}
		}
		this.generarCartas();
	}
}
class Mazo {
	constructor(nombre, baraja) {
		this.nombre = nombre;
		this.grupos = {
			visible: [],
			oculto: [],
		}
	}

	_moverCartasEntreMazos(numero, desde, hasta) {
		this._moverCartasEntreMazos(numero, this, this, desde, hasta)
	}

	nuevoSubmazo(nombre) {
		let mazo = this.submazos[nombre] = new Submazo(nombre);
		return mazo;
	}

	repartirCartas(destinos, distribucion) {
		let totalCartasARepartir = distribucion.reduce((a, b) => a + b);
		for (let i = 0; i < distribucion.length; i++) {
			for (let j = 0; j < destinos.length; j++) {
				if (i < distribucion[j]) {
					this._moverCartasEntreMazos(1, this, destinos[j], "oculta", "oculta");
				}
			}
		}
	}
	// metodo barajar
}
class Submazo extends Mazo {

}
class Juego {
	miBaraja = new Baraja
}