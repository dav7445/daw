class Blog {
	static TAGS = ["deporte", "sociedad", "salud", "economia", "nacional", "internacional"];
	static TIPOS_ELEMENTO = [
		{
			tipo: "texto",
			layoutHTML: {
				elemento: "div",
				atributos: {
					class: "texto",
				},
				estilos: {},
			},
		},
		{
			tipo: "imagen",
			layoutHTML: {
				elemento: "img",
				atributos: {
					class: "imagen",
				},
				estilos: {},
			},
			gestorContenido: (elemento, contenido) => {
				elemento.elementoHTML.setAttribute("src", contenido);
				elemento.elementoHTML.style.maxWidth = "75%";
			},
		},
		{
			tipo: "video",
			layoutHTML: {
				elemento: "div",
				atributos: {
					class: "video",
					controls: true,
				},
				estilos: {},
			},
		},
	];
	static LAYOUT_BLOG = {
		elemento: "section",
		atributos: {
			name: "Blog",
			id: "raiz",
		},
		estilos: {
			backgroundColor: "green",
		},
	};
	static LAYOUT_ENTRADAS = {
		elemento: "article",
		atributos: {
			class: "entrada",
		},
		estilos: {
			backgroundColor: "gray",
		},
	};
	constructor(nombre, layout = Blog.LAYOUT_BLOG, raizHTML = document.body, tags = Blog.TAGS, tiposElemento = Blog.TIPOS_ELEMENTO) {
		this.nombre = nombre;
		this._entradas = [];
		this._tags = [...tags];
		this.tiposElemento = [...tiposElemento];
		this.layout = layout;
		this._raizHTML = raizHTML;
		this.autores = [];
	}

	get nombre() {
		return this._nombre;
	}
	set nombre(valor) {
		this._nombre = valor;
	}

	get layout() {
		return this._layout;
	}
	set layout(valor) {
		this._layout = new Layout(valor, this);
	}

	static nuevoTAG(tag) {
		this.TAGS.push(tag);
		return tag;
	}

	seleccionarEntradasPorTag(tag) {
		return this._entradas.filter((elemento) => elemento._tags.includes(tag));
	}

	desplegarLayout = () => Layout.desplegarLayout(this._raizHTML, this.layout.elementoHTML, "beforeend");

	nuevaEntrada(fecha, titulo, autor, tags = Entrada.tags, layout = this.LAYOUT_ENTRADAS) {
		let entrada = new Entrada(this, fecha, titulo, autor, tags, layout);
		this._entradas.push(entrada);
		return entrada;
	}
}

class Entrada {
	constructor(padre, fecha, titulo, autor, tags, layout) {
		this.fecha = fecha;
		this.titulo = titulo;
		this.autor = autor;
		this._padre = padre;
		this._blog = padre;
		this._tags = [...tags];
		this.layout = layout;
		this._elementos = [];
		this._elementoRaiz = null;
	}

	get padre() {
		return this._padre;
	}

	get fecha() {
		return this._fecha;
	}
	set fecha(valor) {
		this._fecha = valor;
	}

	get titulo() {
		return this._titulo;
	}
	set titulo(valor) {
		this._titulo = valor;
	}

	get autor() {
		return this._autor;
	}
	set autor(valor) {
		this._autor = valor;
	}

	get estado() {
		return this._estado;
	}
	set estado(valor) {
		this._estado = valor;
	}

	get raiz() {
		return this._raiz;
	}
	set raiz(valor) {
		this._raiz = valor;
	}

	get layout() {
		return this._layout;
	}
	set layout(valor) {
		this._layout = new Layout(valor, this);
	}

	get elementoRaiz() {
		return this._layout;
	}
	set elementoRaiz(valor) {
		this._elementoRaiz = new Layout(valor, this);
	}

	get blog() {
		return this._blog;
	}

	etiquetar(tag) {
		this._tags.push(tag);
	}

	crearElementoRaiz(tipo, contenido, layout) {
		if (this._elementoRaiz === null) {
			let elemento = new Elemento(this, tipo, contenido, layout);
			this._elementoRaiz = elemento;
			this._elementos.push(elemento);
		}
		return this._elementoRaiz;
	}

	desplegarLayout = () => Layout.desplegarLayout(this.padre.layout.elementoHTML, this.layout.elementoHTML, "beforeend");
}

class Elemento {
	constructor(padre, tipo, contenido, layout) {
		this._tipo = tipo;
		this._id = Math.floor(Math.random() * 10000000);
		this._padre = padre;
		this.blog = padre.blog;
		this.layout = layout;
		this.contenido = contenido;
		this._subelementos = [];
	}

	get padre() {
		return this._padre;
	}

	get tipo() {
		return this._tipo;
	}

	get contenido() {
		return this._contenido;
	}
	set contenido(valor) {
		this._contenido = valor;
		this.layout.gestionarContenido(valor);
	}

	get layout() {
		return this._layout;
	}
	set layout(valor) {
		if (valor === null || valor === undefined) {
			let valorLayout = this.blog.tiposElemento.find((elem) => elem.tipo === this._tipo);
			if (valorLayout != undefined) {
				valor = valorLayout.layoutHTML;
			}
		}
		this._layout = new Layout(valor, this);
	}

	desplegarLayout = () => Layout.desplegarLayout(this.padre.layout.elementoHTML, this.layout.elementoHTML, "beforeend");

	nuevoSubElemento(tipo, contenido, layout) {
		let subElemento = new Subelemento(this, tipo, contenido, layout);
		subElemento.blog = this.blog;
		this._subelementos.push(subElemento);
		return subElemento;
	}
}

class Subelemento extends Elemento {
	constructor(padre, tipo, contenido, layout) {
		super(padre, tipo, contenido, layout);
	}
}

class Layout {
	constructor(layout, objeto, callBack) {
		this.layout = layout;
		this.objeto = objeto;
		this.gestorContenido = layout.gestorContenido;
		this.callBack = callBack;
		this.elementoHTML = Layout.generarElementoHTML(this.layout, this.objeto);
	}

	get layout() {
		return this._layout;
	}
	set layout(valor) {
		this._layout = valor;
	}

	get callBack() {
		return this._callBack;
	}
	set callBack(funcion) {
		this._callBack = funcion;
	}

	set elementoHTML(valor) {
		this._elementoHTML = valor;
	}
	get elementoHTML() {
		return this._elementoHTML;
	}

	runCallBack(parametros) {
		if (this.callBack != undefined) {
			this.callBack(this, parametros);
		}
	}

	static generarElementoHTML = (layout, objeto) => {
		if (layout != undefined) {
			let elementoHTML = document.createElement(layout.elemento);
			for (let atributo in layout.atributos) {
				elementoHTML.setAttribute(atributo, layout.atributos[atributo]);
			}
			for (let estilo in layout.estilos) {
				elementoHTML.style[estilo] = layout.estilos[estilo];
			}
			if (layout.eventos != undefined) {
				for (let evento in layout.eventos) {
					elementoHTML.addEventListener(layout.eventos[evento].evento, (event) => layout.eventos[evento].funcion(event, objeto));
				}
			}
			return elementoHTML;
		}
		return null;
	};

	static desplegarLayout = (elementoRaiz, elemento, posicion) => {
		elementoRaiz.insertAdjacentElement(posicion, elemento);
	};

	runCallBack(parametros) {
		this.callBack(this, parametros);
	}

	gestionarContenido(valor) {
		if (this.gestorContenido != undefined) {
			this.gestorContenido(this, valor);
		} else {
			this.elementoHTML.innerHTML = valor;
		}
	}
}

export { Blog };
