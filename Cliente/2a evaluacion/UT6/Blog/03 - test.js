import { Blog } from "./blog.js";

function log(mensaje, correcto) {
	if (!correcto) {
		console.error(mensaje);
	} else {
		console.log(mensaje);
	}
}

export function probar() {
	const tagACrear = "motor";
	const nombreBlog = "El tiempo en mi casa";
	const entradaBlog = [
		{
			fecha: new Date(),
			titulo: "probando",
			autor: "yomismo",
			tags: ["hogar", "salud", "dinero", "corazón"],
		},
		{
			fecha: new Date(),
			titulo: "segunda prueba",
			autor: "yomismo",
			tags: ["fotografía", "cocina"],
		},
		{
			fecha: new Date(),
			titulo: "tercera prueba",
			autor: "yomismo",
			tags: ["formula1", "motoGP", "Roland Garros", "motor"],
		},
	];

	const retornoConsultaTags = ["probando", "tercera prueba"];

	const datosElementoRaiz = {
		tipo: "texto",
		contenido: "Este artículo es lo mismo de siempre",
	};

	const datosElementoImagen = {
		tipo: "imagen",
		contenido: "Esta es la imagen del artículoe",
	};

	const etiquetasEntrada = ["comida", "bebida", "música", "motor"];

	let miBlog;
	let entrada = [];
	let elementoRaiz;
	let elementoImagen;

	const layoutHTMLBlog = {
		elemento: "section",
		atributos: {
			name: "Blog",
			id: "raiz",
		},
		estilos: {
			backgroundColor: "green",
			display: "flex",
			margin: "1rem",
			flexDirection: "column",
			width: "100%",
			height: "100%",
		},
	};

	const layoutHTMLEntrada = {
		elemento: "article",
		atributos: {
			class: "entrada",
		},
		estilos: {
			backgroundColor: "gray",
		},
		// eventos: [],
	};

	const layoutHTMLElementoimagen = {
		elemento: "img",
		atributos: {
			class: "imagen",
		},
	};

	const layoutHTMLElementoRaiz = {
		elemento: "div",
		atributos: {
			class: "texto",
		},
	};

	log("0 - tipos elemento array objetos", typeof Blog.TIPOS_ELEMENTO[0] === "object");

	Blog.nuevoTAG(tagACrear);
	log("1 - añadir 1 tag: " + tagACrear + " a la propiedad Estática TAGS", Blog.TAGS.includes(tagACrear));

	miBlog = new Blog(nombreBlog);
	log("2.1 - devuelve un blog creado - nombr: " + nombreBlog, nombreBlog === miBlog.nombre);
	log("2.2 - array de entradas ", Array.isArray(miBlog._entradas));
	log("2.3 - array de entradas vacío", miBlog._entradas.length === 0);
	log("2.4 - layout <estandar> generado", miBlog.layout.elementoHTML instanceof HTMLElement);

	// 3 - crear entradas en el blog, ()

	for (let i = 0; i < entradaBlog.length; i++) {
		entrada.push(miBlog.nuevaEntrada(entradaBlog[i].fecha, entradaBlog[i].titulo, entradaBlog[i].autor, entradaBlog[i].tags));
	}

	log("3 - Creando entrdas en el blog", true);

	for (let i = 0; i < entradaBlog.length; i++) {
		log("3.1 - entrada el blog: " + i + " : " + entrada[i].titulo, entrada[i].titulo === entradaBlog[i].titulo);
		log("3.2 - layout de la entrada en blog: " + i + " : " + entrada[i].titulo, entrada[i].layout.elementoHTML instanceof HTMLElement);
	}

	// 4 - etiquetar entradas

	for (let i = 0; i < etiquetasEntrada.length; i++) {
		entrada[0].etiquetar(etiquetasEntrada[i]);
	}

	for (let i = 0; i < etiquetasEntrada.length; i++) {
		log("4 - etiquetar entradas: " + i + ": " + etiquetasEntrada[i], entrada[0]._tags.includes(etiquetasEntrada[i]));
	}

	// 5 - crear elemento raiz

	elementoRaiz = entrada[0].crearElementoRaiz(datosElementoRaiz.texto, datosElementoRaiz.contenido);

	log("5 - crear elemnto raiz", elementoRaiz.contenido === datosElementoRaiz.contenido);

	//  6 - añadir subElemento

	elementoImagen = elementoRaiz.nuevoSubElemento(datosElementoImagen.tipo, datosElementoImagen.contenido);

	log("6 - añadir subElemento", elementoImagen.contenido === datosElementoImagen.contenido);

	// 7 - seleccionarEntradasPorTag

	let entradasTag = miBlog.seleccionarEntradasPorTag("motor");

	log("7 - seleccionarEntradasPorTag: entradas tageadas: " + entradasTag.length, entradasTag.length === retornoConsultaTags.length);

	for (let i = 0; i < entradasTag.length; i++) {
		log("7 - seleccionarEntradasPorTag: " + i + " : " + retornoConsultaTags[i], entradasTag[i].titulo === retornoConsultaTags[i]);
	}

	// 8 - crear elemento HTML para el Blog

	miBlog.layout = layoutHTMLBlog;

	miBlog.desplegarLayout();

	log("8 - crear elemento Layout para del Blog", miBlog.layout.elementoHTML instanceof HTMLElement);

	// 9 - crear elemento HTML para las entradas

	let primeraEntrada = entrada[0];

	primeraEntrada.layout = layoutHTMLEntrada;

	primeraEntrada.desplegarLayout();

	log("9 - crear elemento Layout para las entradas", primeraEntrada.layout.elementoHTML instanceof HTMLElement);

	// 10 - crear elemento HTML para el elemento raiz

	elementoRaiz.layout = layoutHTMLElementoRaiz;

	elementoRaiz.layout.elementoHTML.innerHTML = "Caray";

	elementoRaiz.desplegarLayout();

	log("10 - crear elemento HTML para el elemento raiz", elementoRaiz.layout.elementoHTML instanceof HTMLElement);

	// 11 - crear un subelemento HTML - para el elemento raiz

	elementoImagen.layout = layoutHTMLElementoimagen;
	elementoImagen.desplegarLayout();

	log("11 - crear elemento HTML para el elemento raiz", elementoImagen.layout.elementoHTML instanceof HTMLElement);

	elementoImagen.layout.callBack = (elemento, parametro) => {
		elemento.elementoHTML.setAttribute("src", parametro.src);
		elemento.elementoHTML.setAttribute("width", parametro.width);
	};

	elementoImagen.layout.runCallBack({
		src: ".\\images\\1_de_corazones.png",
		width: "25%",
	});
}
