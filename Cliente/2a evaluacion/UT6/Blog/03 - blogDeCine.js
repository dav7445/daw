import { Blog } from "./blog.js";

export function blogDeCine() {
	// constructor(nombre, layout = Blog.LAYOUT_BLOG, raizHTML = document.body) {

	const layoutHTMLBlog = {
		elemento: "section",
		atributos: {
			name: "Blog",
			id: "blogCine",
		},
		estilos: {
			backgroundColor: "#FFF",
			margin: "0px",
			padding: "50px 60px",
			display: "flex",
			width: "100%",
			alignConten: "space-around",
		},
	};

	const layoutEntrada = {
		elemento: "article",
		atributos: {
			id: "raiz",
		},
		estilos: {
			border: "2px solid #9DD1F1",
			display: "flex",
			height: "auto",
			marginTop: "4px",
			padding: "30px auto",
			textAlign: "center",
			width: "350px",
		},
	};

	const tiposElemento = [
		{
			tipo: "raiz",
			layoutHTML: {
				elemento: "div",
				atributos: {
					class: "raiz",
				},
				estilos: {
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
				},
			},
		},
		{
			tipo: "titulo",
			layoutHTML: {
				elemento: "h1",
				atributos: {
					class: "titulo",
				},
				estilos: {
					fontSize: "2rem",
					textAlign: "center",
				},
				eventos: [
					{
						evento: "click",
						funcion: (e, obj) => {
							let estado = obj.layout.elementoHTML.style.fontSize;
							obj.layout.elementoHTML.style.fontSize = estado === "5rem" ? "2rem" : "5rem";
						},
					},
				],
			},
		},
		{
			tipo: "parrafo",
			layoutHTML: {
				elemento: "p",
				atributos: {
					class: "parrafo",
				},
				estilos: {
					fontSize: "1rem",
					textAlign: "justify",
					padding: "12px",
				},
			},
		},
		{
			tipo: "texto",
			layoutHTML: {
				elemento: "div",
				atributos: {
					class: "texto",
				},
				estilos: {},
			},
		},
		{
			tipo: "imagen",
			layoutHTML: {
				elemento: "img",
				atributos: {
					class: "imagen",
				},
				estilos: {
					padding: "1em",
				},
				gestorContenido: (elemento, contenido) => {
					elemento.elementoHTML.setAttribute("src", contenido);
					elemento.elementoHTML.style.maxWidth = "75%";
				},
			},
		},
		{
			tipo: "video",
			layoutHTML: {
				elemento: "div",
				atributos: {
					class: "video",
					controls: true,
				},
				estilos: {},
			},
		},
	];

	const miBlogDeCine = new Blog("Mi Blog de Cine", layoutHTMLBlog, document.body, ["cortometraje", "largometraje", "drama", "terror", "romántica", "aventuras"], tiposElemento);
	miBlogDeCine.desplegarLayout();

	// CON LA VIDA ------

	const conLaVida = miBlogDeCine.nuevaEntrada(new Date(2022, 2, 16), "Con la vida hicieron fuego", "", ["Drama", "Luarca", "Cudillero", "Figueras"], layoutEntrada);
	conLaVida.desplegarLayout();

	let elementoRaiz = conLaVida.crearElementoRaiz("raiz", "");

	elementoRaiz.desplegarLayout();

	let titulo = elementoRaiz.nuevoSubElemento("titulo", "Con la vida hicieron fuego");

	titulo.desplegarLayout();

	let contenidoElemento = elementoRaiz.nuevoSubElemento(
		"parrafo",
		"Argumento: Quico, un marino que se fue a América tras la guerra, vuelve a Ferrera después de 15 años. Allí se reencontrará con sus amigos y con algunos fantasmas del pasado. La melancolía le invade, pero cuando se decide a rehacer su vida algo se interpondrá entre él y su futuro."
	);

	contenidoElemento.desplegarLayout();

	let imagenPelicula = elementoRaiz.nuevoSubElemento("imagen", ".\\images\\Con_la_vida_hicieron_fuego_the_wandering_s-496x715.jpg");

	imagenPelicula.desplegarLayout();

	let videoPelicula = elementoRaiz.nuevoSubElemento(
		"video",
		'<iframe width="350" height="200" src="https://www.youtube.com/embed/jMkAOWNF6wY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
	);

	videoPelicula.desplegarLayout();

	// HERIDA LUMINOSA

	const heridaLuminosa = miBlogDeCine.nuevaEntrada(new Date(2022, 2, 17), "La herida luminosa", "", ["Drama", "Lastres"], layoutEntrada);

	heridaLuminosa.desplegarLayout();

	let elementoRaizHerida = heridaLuminosa.crearElementoRaiz("raiz", "");

	elementoRaizHerida.desplegarLayout();

	let titulo2 = elementoRaizHerida.nuevoSubElemento("titulo", "La herida luminosa");

	titulo2.desplegarLayout();

	let argumentoHerida = elementoRaizHerida.nuevoSubElemento(
		"parrafo",
		"Argumento: El doctor Molinos y su mujer Isabel atraviesan una crisis conyugal. Viven en una capital de provincias, en la opresiva España de los 50, en compañía de dos criadas y su única hija ha ingresado en un monasterio. El inesperado amor que siente el doctor por Julia hace que vuelva a sentirse vivo. La negativa de Isabel a aceptar la separación le lleva a recurrir a otros métodos."
	);

	argumentoHerida.desplegarLayout();

	let imagenHerida = elementoRaizHerida.nuevoSubElemento("imagen", ".\\images\\La-herida-luminosa-1997.jpg");

	imagenHerida.desplegarLayout();

	// ASIGNATURA APROBADA
	const asignaturaAprobado = miBlogDeCine.nuevaEntrada(new Date(2022, 2, 17), "Asignatura aprobada", "", ["Drama", "Thriller", "Gijón"], layoutEntrada);

	asignaturaAprobado.desplegarLayout();

	let elementoRaizAsignatura = asignaturaAprobado.crearElementoRaiz("raiz", "");

	elementoRaizAsignatura.desplegarLayout();

	let titulo3 = elementoRaizAsignatura.nuevoSubElemento("titulo", "Asignatura aprobada");

	titulo3.desplegarLayout();

	let argumentoAsignatura = elementoRaizAsignatura.nuevoSubElemento(
		"parrafo",
		"Argumento: El doctoArgumento: José Manuel Alcántara es un autor teatral de unos cincuenta años. A causa del fracaso de su última obra o quizá debido al abandono de su mujer, decide irse de Madrid e instalarse en una ciudad de provincias a orillas del mar. Allí vive tranquilamente, dedicado a sus colaboraciones en radio y prensa.r Molinos y su mujer Isabel atraviesan una crisis conyugal. Viven en una capital de provincias, en la opresiva España de los 50, en compañía de dos criadas y su única hija ha ingresado en un monasterio. El inesperado amor que siente el doctor por Julia hace que vuelva a sentirse vivo. La negativa de Isabel a aceptar la separación le lleva a recurrir a otros métodos."
	);

	argumentoAsignatura.desplegarLayout();

	let imagenAsignatura = elementoRaizAsignatura.nuevoSubElemento("imagen", ".\\images\\asignatura-aprobada-the-wandering-s.jpeg");

	imagenAsignatura.desplegarLayout();

	let videoAsignatura = elementoRaizAsignatura.nuevoSubElemento(
		"video",
		'<iframe width="350" height="200" src="https://www.youtube.com/embed/1tvu7mZJE9M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
	);

	videoAsignatura.desplegarLayout();
}
