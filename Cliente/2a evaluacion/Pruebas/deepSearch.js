let array1 = [1, ["a", "b", [["x", "y", "z"]]]];

explorar(array1);

function explorar(array) {
	for (i = 0; i < array.length; i++) {
		if (Array.isArray(array[i])) {
			explorar(array[i]);
		} else {
			console.log(array[i]);
		}
	}
}
