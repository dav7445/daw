// OBJETOS
// sintaxis literal
let objeto = {};

// definicion y propiedades
let cochePaula = {
	marca: "Opel",
	modelo: "Corsa",
	matricula: "U-000-NOSE",
	color: "blanco perlado",
	kilometros: 154256,
	fechaMatricula: 2007,
};

// Añadiendo  propiedades...
cochePaula.combustible = "Gasolina";
cochePaula.caballos = undefined;
cochePaula["motorizacion"] = "TI 2000CC";

// ¿Existe la propiedad?
console.log("caballos: " + ("caballos" in cochePaula));
console.log("motorizacion: " + ("motorizacion" in cochePaula));

// Propiedades "dinámicas variables" ...
let nuevaPropiedad = "número de puertas";
cochePaula[nuevaPropiedad] = 5;

// Mostrar todas las propiedades con un for in
for (propiedad in cochePaula) {
	console.log(`Propiedad ${propiedad}: ${cochePaula[propiedad]}`);
}

/* Deep clone by value (node 17+) */
let cocheNuria = structuredClone(cochePaula);

/* Constructor */
function Coche(matricula, marca, modelo) {
	this.matricula = matricula;
	this.marca = marca;
	this.modelo = modelo;
	this.mostrarCoche = function () {
		for (let atributo in this) {
			console.log(`${atributo} => ${this[atributo]}`);
		}
	};
}
const cochePaula = new Coche("00000", "Opel", "Meriva");
cochePaula.mostrarCoche();
