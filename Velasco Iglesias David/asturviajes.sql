-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2022 at 05:19 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asturviajes`
--
CREATE DATABASE IF NOT EXISTS `asturviajes` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `asturviajes`;

DROP TABLE IF EXISTS `hoteles_img`;
DROP TABLE IF EXISTS `premium`;
DROP TABLE IF EXISTS `hoteles_reservas`;
DROP TABLE IF EXISTS `hoteles_habitaciones`;
DROP TABLE IF EXISTS `hoteles`;
DROP TABLE IF EXISTS `usuarios`;

-- --------------------------------------------------------

--
-- Table structure for table `hoteles`
--

CREATE TABLE IF NOT EXISTS `hoteles` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `ciudad` varchar(50) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `localizacion` enum('playa','ciudad','montaña','campo') NOT NULL,
  `estrellas` int(1) NOT NULL,
  `premium` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hoteles`
--

INSERT INTO `hoteles` (`id`, `nombre`, `descripcion`, `direccion`, `ciudad`, `pais`, `localizacion`, `estrellas`, `premium`) VALUES
(1, 'Monument Hotel', 'El Monument Hotel está ubicado en un emblemático palacete premodernista en el corazón de Barcelona.', 'Calle Passeig de Gràcia 75', 'Barcelona', 'España', 'ciudad', 5, 1),
(2, 'Eurostars Ibiza', 'Eurostars Ibiza Sol, playa y relax: disfruta de unas vacaciones al estilo ibicenco', 'Calle de Ramon Muntaner 25-33', 'Ibiza', 'España', 'playa', 4, 0),
(3, 'Nuevo Madrid', 'Ideal para quien visita por negocios o para quien busca una zona más residencial y tranquila para alojarse en Madrid.', 'Calle Bausa, 27', 'Madrid', 'España', 'ciudad', 4, 0),
(5, 'Lopesan Costa Meloneras Resort & Spa', 'Hotel de lujo en la playa con spa, cerca de Faro de Maspalomas', 'Mar Mediterraneo 1', 'Meloneras', 'España', 'playa', 5, 0),
(12, 'Renaissance Barcelona Hotel', 'Hotel de lujo con restaurante, cerca de Casa Batlló', 'Pau Claris 122', 'Barcelona', 'España', 'ciudad', 5, 0),
(13, 'NH Collection Sevilla', 'Hotel de 4 estrellas con 1 restaurantes, fácil acceso al centro de convenciones y cerca de Plaza de España', 'Avenida de Diego Martinez Barrio, 8', 'Sevilla', 'España', 'ciudad', 4, 0),
(15, 'Hotel Arnal', 'Hotel de 3 estrellas en Puértolas con restaurante y bar o lounge', 'Pineta,S/N', 'Puertolas', 'España', 'campo', 3, 0),
(16, 'Hotel Cal Ruiz', 'Una estancia perfecta y confortable', 'Avinguda Del Consell General 10', 'Pas de la Casa', 'Andorra', 'montaña', 3, 0),
(18, 'Hotel Cervol', 'Hotel de 3 estrellas en Andorra la Vella', 'Avinguda Santa Coloma, 46', 'Andorra la Vella', 'Andorra', 'montaña', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hoteles_habitaciones`
--

CREATE TABLE IF NOT EXISTS `hoteles_habitaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(6) NOT NULL,
  `disponibles` int(6) NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `capacidad` int(3) NOT NULL,
  `descuento` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hoteles_habitaciones`
--

INSERT INTO `hoteles_habitaciones` (`id`, `hotel_id`, `disponibles`, `precio`, `tipo`, `capacidad`, `descuento`) VALUES
(1, 1, 0, '400.00', 'deluxe', 2, 0),
(2, 1, 3, '900.00', 'familiar', 4, 0),
(3, 1, 4, '300.00', 'junior', 2, 0),
(4, 1, 2, '500.00', 'deluxe doble', 4, 0),
(5, 1, 2, '600.00', 'junior doble', 4, 0),
(9, 5, 1, '250.00', 'deluxe', 2, 10),
(18, 5, 5, '270.00', 'suite', 2, 0),
(19, 2, 5, '600.00', 'suite', 2, 0),
(20, 2, 5, '1300.00', 'suite deluxe', 4, 15),
(21, 3, 10, '45.00', 'individual', 1, 0),
(22, 3, 10, '50.00', 'doble', 2, 0),
(23, 3, 5, '55.00', 'premium doble', 2, 0),
(24, 12, 1, '800.00', 'suite familiar', 4, 0),
(25, 12, 5, '350.00', 'urban', 2, 0),
(26, 13, 10, '125.00', 'superior', 2, 0),
(27, 13, 3, '160.00', 'premium', 2, 0),
(29, 15, 2, '60.00', 'individual', 1, 0),
(30, 15, 1, '80.00', 'doble', 2, 0),
(31, 16, 2, '160.00', 'doble', 2, 0),
(32, 16, 5, '185.00', 'triple', 3, 0),
(34, 16, 5, '200.00', 'cuadruple', 4, 0),
(35, 18, 5, '25.00', 'económica doble', 2, 0),
(36, 18, 5, '30.00', 'doble', 2, 0),
(37, 18, 5, '40.00', 'doble superior', 2, 0),
(38, 18, 5, '45.00', 'triple', 3, 0),
(39, 18, 2, '75.00', 'cuadruple superior', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hoteles_img`
--

CREATE TABLE IF NOT EXISTS `hoteles_img` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(6) NOT NULL,
  `img` varchar(100) NOT NULL,
  `habitacion` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hotel_id` (`hotel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hoteles_img`
--

INSERT INTO `hoteles_img` (`id`, `hotel_id`, `img`, `habitacion`) VALUES
(2, 1, 'assets/img/hotel/1/3.jpg', ''),
(3, 1, 'assets/img/hotel/1/4.webp', 'deluxe'),
(4, 1, 'assets/img/hotel/1/5.webp', 'familiar'),
(5, 1, 'assets/img/hotel/1/6.webp', 'junior'),
(13, 1, 'assets/img/hotel/1/1.jpg', ''),
(23, 5, 'assets/img/hotel/5/bf188864.jpg', 'deluxe'),
(24, 5, 'assets/img/hotel/5/74e8a8cf.jpg', 'suite'),
(27, 5, 'assets/img/hotel/5/d8f5ce80.webp', ''),
(28, 5, 'assets/img/hotel/5/44eb89c2.webp', ''),
(29, 2, 'assets/img/hotel/2/4f532801.webp', ''),
(30, 2, 'assets/img/hotel/2/6a3500d2.webp', ''),
(31, 2, 'assets/img/hotel/2/23b8176f.webp', 'suite'),
(32, 2, 'assets/img/hotel/2/783482f4.webp', 'suite deluxe'),
(33, 3, 'assets/img/hotel/3/P5HF9SIbnTih20odvJtx_original.jpg', 'individual'),
(34, 3, 'assets/img/hotel/3/GNDsW4MEhos8gmW7qfXf_original.jpg', 'doble'),
(35, 3, 'assets/img/hotel/3/thRpw1MwvsV5PAkKbcfG_original.jpg', 'premium doble'),
(36, 3, 'assets/img/hotel/3/99b00acb.webp', ''),
(37, 1, 'assets/img/hotel/1/7e6b27d5.webp', 'deluxe doble'),
(38, 12, 'assets/img/hotel/12/483e84e3.webp', ''),
(39, 12, 'assets/img/hotel/12/de98d7fc.webp', 'suite familiar'),
(40, 12, 'assets/img/hotel/12/95532cf4.webp', 'urban'),
(41, 13, 'assets/img/hotel/13/35cfdaec.webp', ''),
(42, 13, 'assets/img/hotel/13/e792b8be.webp', 'superior'),
(43, 13, 'assets/img/hotel/13/3d33906b.webp', 'premium'),
(44, 15, 'assets/img/hotel/15/b82417a4.webp', ''),
(45, 15, 'assets/img/hotel/15/ac19965a.webp', 'doble'),
(46, 15, 'assets/img/hotel/15/6c9c1396.webp', 'doble'),
(47, 16, 'assets/img/hotel/16/8e490bb1.webp', ''),
(48, 16, 'assets/img/hotel/16/cb3b2022.webp', ''),
(49, 16, 'assets/img/hotel/16/5b8ed5ba.webp', 'doble'),
(50, 16, 'assets/img/hotel/16/91b7ab2f.webp', 'triple'),
(51, 16, 'assets/img/hotel/16/abeb8a98.webp', 'cuadruple'),
(52, 18, 'assets/img/hotel/18/7d2cf677.webp', ''),
(53, 18, 'assets/img/hotel/18/62fbd2d5.webp', 'económica doble'),
(54, 18, 'assets/img/hotel/18/a8706588.webp', 'doble'),
(55, 18, 'assets/img/hotel/18/28ca316d.webp', 'doble superior'),
(56, 18, 'assets/img/hotel/18/7bd84fc5.webp', 'triple'),
(57, 18, 'assets/img/hotel/18/9b2b087b.webp', 'cuadruple superior');

-- --------------------------------------------------------

--
-- Table structure for table `hoteles_reservas`
--

CREATE TABLE IF NOT EXISTS `hoteles_reservas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `hotel_id` int(6) NOT NULL,
  `habitacion_id` int(6) NOT NULL,
  `entrada` date NOT NULL,
  `salida` date NOT NULL,
  `adultos` int(2) NOT NULL,
  `ninos` int(2) NOT NULL,
  `noches` int(2) NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  `cancelada` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `hotel_id` (`hotel_id`),
  KEY `habitacion_id` (`habitacion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hoteles_reservas`
--

INSERT INTO `hoteles_reservas` (`id`, `email`, `hotel_id`, `habitacion_id`, `entrada`, `salida`, `adultos`, `ninos`, `noches`, `precio`, `cancelada`) VALUES
(88, 'carlos@ejemplo.com', 5, 9, '2022-07-01', '2022-07-15', 1, 0, 14, '3150.00', 1),
(89, 'carlos@ejemplo.com', 1, 3, '2022-07-16', '2022-07-29', 1, 0, 13, '3900.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `premium`
--

CREATE TABLE IF NOT EXISTS `premium` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `fecha_compra` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `precio` decimal(6,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `premium`
--

INSERT INTO `premium` (`id`, `email`, `fecha_compra`, `fecha_fin`, `precio`) VALUES
(33, 'carlos@ejemplo.com', '2022-06-10 12:53:11', '2023-06-10 12:53:11', '19.00');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `nombre` varchar(20) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rol` int(1) NOT NULL,
  `picture` varchar(150) NOT NULL,
  `telefono` int(9) DEFAULT NULL,
  `localidad` varchar(50) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`nombre`, `apellidos`, `email`, `password`, `rol`, `picture`, `telefono`, `localidad`) VALUES
('Administrador', 'Admin Admin', 'admin@ejemplo.com', '$2y$10$UNmaakckEkE6S.96s2VgROV3jQSw/nnigWSt9XQkes9lMeEZpL202', 2, '', NULL, ''),
('Carlos', 'Estrada García', 'carlos@ejemplo.com', '$2y$10$/K6zDdCl/FyPhQD/CwWp0eFphC/mj7w5tNWFR/0QncP45iJed8w0e', 1, 'assets/img/usuario/carlos@ejemplo.com/f06818c6392bdefa2a7b47bf2e3be470.jpg', 654321987, 'Pravia'),
('David', 'Velasco Iglesias', 'david@ejemplo.com', '$2y$10$IKHqDL2.PdMtLJ5wQftYieBY0uIBARzQBFrDnhwKSCcIOOgyMcoR6', 0, '', NULL, '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `hoteles_habitaciones`
--
ALTER TABLE `hoteles_habitaciones`
  ADD CONSTRAINT `hoteles_habitaciones_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hoteles` (`id`);

--
-- Constraints for table `hoteles_img`
--
ALTER TABLE `hoteles_img`
  ADD CONSTRAINT `hoteles_img_ibfk_1` FOREIGN KEY (`hotel_id`) REFERENCES `hoteles` (`id`);

--
-- Constraints for table `hoteles_reservas`
--
ALTER TABLE `hoteles_reservas`
  ADD CONSTRAINT `hoteles_reservas_ibfk_1` FOREIGN KEY (`email`) REFERENCES `usuarios` (`email`),
  ADD CONSTRAINT `hoteles_reservas_ibfk_2` FOREIGN KEY (`hotel_id`) REFERENCES `hoteles` (`id`),
  ADD CONSTRAINT `hoteles_reservas_ibfk_3` FOREIGN KEY (`habitacion_id`) REFERENCES `hoteles_habitaciones` (`id`);

--
-- Constraints for table `premium`
--
ALTER TABLE `premium`
  ADD CONSTRAINT `premium_ibfk_1` FOREIGN KEY (`email`) REFERENCES `usuarios` (`email`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
