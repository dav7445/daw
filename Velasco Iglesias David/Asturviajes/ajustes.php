<?php include("templates/head.php"); ?>

<?php
session_start();
include("templates/signin_access.php");
include("templates/conectar_bd_consultor.php");
?>

<body>
	<div class="position-relative min-vh-100 d-flex flex-column justify-content-between">

		<?php include("templates/header.php"); ?>

		<div class="mb-5 mb-md-0">
			<div class="container mt-4">
				<div class="row">
					<div class="col-12 col-md-3">
						<?php include("templates/ajustes_sidebar.php"); ?>
					</div>

					<div class="d-block d-md-none">
						<hr>
					</div>

					<div class="col-12 col-md-9">
						<div class="row">
							<?php
							if (isset($_GET['tab']) && $_GET['tab'] == 'perfil') {
								$email = $_SESSION["email"];
								include("templates/ajustes_perfil.php");
							} else if (isset($_GET['tab']) && $_GET['tab'] == 'compras') {
								include("templates/ajustes_compras.php");
							} else if (isset($_GET['tab']) && $_GET['tab'] == 'admin_hoteles') {
								include("templates/admin/admin_hoteles.php");
							} else if (isset($_GET['tab']) && $_GET['tab'] == 'admin_usuarios') {
								include("templates/admin/admin_usuarios.php");
							} else if (isset($_GET['tab']) && $_GET['tab'] == 'admin_basedatos') {
								include("templates/admin/admin_basedatos.php");
							} else {
								header("Location: ajustes.php?tab=perfil");
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include("templates/footer.php"); ?>
	</div>
</body>