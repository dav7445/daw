<?php include("templates/head.php"); ?>

<?php
/* if user is already logged in redirect to index */
session_start();
include("templates/signin_forbidden.php");
?>

<?php
$loginErr = $loginSucc = "";
$nombre = $apellidos = $email = "";

if (isset($_POST["submit"])) {
	include("templates/admin/conectar_bd_admin.php");
	$nombre = mysqli_real_escape_string($enlace, $_POST["name"]);
	$apellidos = mysqli_real_escape_string($enlace, $_POST["lastname"]);
	$email = mysqli_real_escape_string($enlace, $_POST["email"]);

	if (empty($_POST["name"]) || empty($_POST["lastname"]) || empty($_POST["email"]) || empty($_POST["password"]) || empty($_POST["password2"])) {
		$loginErr = "Todos los campos son obligatorios";

	} else if (strlen($_POST["name"]) > 20) {
		$loginErr = "El nombre no puede tener más de 20 caracteres";

	} else if (strlen($_POST["lastname"]) > 50) {
		$loginErr = "Los apellidos no pueden tener más de 50 caracteres";

	} else if (strlen($_POST["email"]) > 50) {
		$loginErr = "El email no pueden tener más de 50 caracteres";

	} else if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
		$loginErr = "El email no es válido";

	} else if (strlen($_POST["password"]) < 8) {
		$loginErr = "La contraseña debe tener al menos 8 caracteres";

	} else if ($_POST["password"] != $_POST["password2"]) {
		$loginErr = "Las contraseñas no coinciden";

	} else {
		$password = mysqli_real_escape_string($enlace, $_POST["password"]);

		$password = password_hash($password, PASSWORD_DEFAULT);

		$sql = "INSERT INTO usuarios (nombre, apellidos, email, password, rol) VALUES ('$nombre', '$apellidos', '$email', '$password', 0)";
		$query = mysqli_query($enlace, $sql);

		if ($query) {
			$loginSucc = "Usuario registrado correctamente";
			header("Refresh:3; url=signin.php");
		} else {
			$loginErr = "Error al registrar el usuario, puede que el email ya esté registrado";
		}

		mysqli_close($enlace);
	}
}
?>

<body>
	<div class="position-relative min-vh-100 d-flex flex-column justify-content-between">

		<?php include("templates/header_simple.php"); ?>

		<div class="container col-xl-10 col-xxl-8 px-md-4 py-5">
			<div class="row align-items-center g-lg-5 py-5">
				<div class="col-lg-7 text-center text-lg-start">
					<h1 class="display-4 fw-bold lh-1 mb-3">AsturViajes</h1>
					<p class="col-lg-10 fs-4">El placer de viajar</p>
				</div>
				<div class="col-md-10 mx-auto col-lg-5">
					<form class="p-4 p-md-5 mb-2 border rounded-3 bg-light needs-validation" method="post" novalidate>
						<div class="row gy-3">
							<div class="col-md-6">
								<label for="name" class="form-label">Nombre <span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="name" name="name" placeholder="Nombre" value="<?= $nombre ?>" required pattern="[A-ZÁÉÍÓÚ][a-záéíóú]+$" />
								<div class="invalid-feedback">Nombre no valido. Una palabra, solo letras, la primera mayuscula.</div>
							</div>

							<div class="col-md-6">
								<label for="lastname" class="form-label">Apellidos <span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Apellido Apellido" value="<?= $apellidos ?>" required pattern="[A-ZÁÉÍÓÚ][a-záéíóú]+ [A-ZÁÉÍÓÚ][a-záéíóú]+$" />
								<div class="invalid-feedback">Apellidos no validos. Dos palabras, solo letras, primera mayuscula.</div>
							</div>

							<div class="col-12">
								<label for="email" class="form-label">Email <span class="text-danger">*</span></label>
								<input type="email" class="form-control" id="email" name="email" placeholder="ejemplo@ejemplo.com" value="<?= $email ?>" required />
								<div class="invalid-feedback">Email no valido.</div>
							</div>

							<div class="col-12">
								<label for="password" class="form-label">Contraseña <span class="text-danger">*</span></label>
								<input type="password" class="form-control" id="password" name="password" placeholder="Contraseña" min="8" required pattern=".{8,}" />
								<small class="text-muted">Al menos 8 caracteres</small>
								<div class="invalid-feedback">Contraseña no valida. Al menos 8 caracteres.</div>
							</div>

							<div class="col-12">
								<label for="password2" class="form-label">Confirmar contraseña <span class="text-danger">*</span></label>
								<input type="password" class="form-control" id="password2" name="password2" placeholder="Confirmar contraseña" min="8" required pattern=".{8,}" />
								<div class="invalid-feedback">Contraseña no valida. Al menos 8 caracteres.</div>
							</div>

							<div class="col-12">
								<?php
								if ($loginErr != "") {
								?>
									<div class='alert alert-danger' role='alert'><?= $loginErr ?></div>
								<?php
								} else if ($loginSucc != "") {
								?>
									<div class='alert alert-success' role='alert'><?= $loginSucc ?></div>
								<?php
								}
								?>
								<button class="w-100 btn btn-lg btn-primary" type="submit" name="submit">Registrarse</button>
							</div>
						</div>
					</form>
					<p class="text-center">¿Ya tienes una cuenta? <a href="signin.php" class="text-decoration-none">Inicia sesión</a></p>
				</div>
			</div>
		</div>

		<?php include("templates/footer.php"); ?>
	</div>
	<script src="assets/js/form-validation.js"></script>
</body>

</html>