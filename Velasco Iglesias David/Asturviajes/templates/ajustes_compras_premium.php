<?php
$warning = "";

$today = date("Y-m-d H:i");
$today_completo = date("Y-m-d H:i:s");

if (isset($_POST["cancelar_premium"])) {
	$id_premium = $_POST["cancelar_premium"];
	/* update premium table: fecha_fin and update usuarios table setting rol to 0 */
	include("templates/admin/conectar_bd_admin.php");
	$sql = "UPDATE premium SET fecha_fin = '$today_completo' WHERE id = '$id_premium'";
	$query = mysqli_query($enlace, $sql);

	if ($query) {
		$sql = "UPDATE usuarios SET rol = 0 WHERE email = '$email'";
		$query = mysqli_query($enlace, $sql);
		if ($query) {
			$warning = '<div class="alert alert-success" role="alert">Premium cancelado correctamente</div>';
		} else {
			$warning = '<div class="alert alert-danger" role="alert">Ha ocurrido un error al cancelar el premium</div>';
		}

	} else {
		$warning = '<div class="alert alert-danger" role="alert">Ha ocurrido un error al cancelar el premium</div>';
	}
}

?>

<div class="col-12">
	<h1 class="display-4">Compras Premium</h1>
	<hr>
	<?= $warning ?>
</div>

<?php

$sql = "SELECT id, fecha_compra, fecha_fin, precio FROM premium WHERE email = '$email'";
$query = mysqli_query($enlace, $sql);

if (mysqli_num_rows($query) == 0) {
?>
	<div class="col-12">
		<div class='alert alert-info'>Nunca ha adquirido el premium, puede comprarlo <a class="alert-link" href="plans.php">aquí</a>.</div>
	</div>
	<?php
} else {
	while ($row = mysqli_fetch_assoc($query)) {
		$fecha_compra = $row["fecha_compra"];
		$fecha_fin = $row["fecha_fin"];
		$fecha_compra = date("Y-m-d H:i", strtotime($fecha_compra));
		$fecha_fin = date("Y-m-d H:i", strtotime($fecha_fin));

		$precio = $row["precio"];
		$id_compra = $row["id"];
	?>
		<div class="col-12">
			<?php
			if ($today < $fecha_fin) {
			?>
				<div class="card border-dark mb-3">
					<h5 class="card-header d-flex justify-content-between">
						<div class="d-flex align-items-center">Compra #<?= $id_compra; ?></div>
						<div class="d-flex">
							<form action="#" class="mb-0" method="post">
								<button type="button" class="btn btn-sm btn-outline-danger" data-bs-toggle="modal" data-bs-target="#confirmar_premium<?= $id_compra; ?>">Cancelar Premium</button>
								<div class="modal fade" id="confirmar_premium<?= $id_compra; ?>" tabindex="-1" aria-labelledby="modallabel_premium<?= $id_compra; ?>" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="modallabel_premium<?= $id_compra; ?>">Confirma que quieres cancelar la membresia #<?= $id_compra; ?></h5>
												<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Volver</button>
												<button type="submit" name="cancelar_premium" value="<?= $id_compra; ?>" class="btn btn-outline-danger">Cancelar Premium</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</h5>
				<?php
			} else {
				?>
					<div class="card text-bg-light mb-3">
						<h5 class="card-header d-flex">
							<div class="d-flex">Compra #<?= $id_compra; ?> (Expirado el <?= $fecha_fin; ?>)</div>
						</h5>
					<?php
				}
					?>
					<div class="card-body">
						<div class="row">

							<div class="col-md-4 d-flex align-items-end justify-content-md-end">
								<h1 class="display-5"><?= $precio; ?><small>€</small></h1>
							</div>

							<div class="col-md-8 order-md-first">
								<div class="card-text">
									<div class="row">
										<div class="col-12"><strong>Fecha adquisición:</strong> <?= $fecha_compra; ?></div>
										<div class="col-12"><strong>Fecha expiración:</strong> <?= $fecha_fin; ?></div>
									</div>
								</div>
							</div>

						</div>
					</div>
					</div>
				</div>
		<?php
	}
}
		?>