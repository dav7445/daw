<?php
$warning = "";

if ($_SESSION["rol"] == "2" && isset($_GET["accion"]) && $_GET["accion"] == "editar") {
	$admin_editando = true;
} else {
	$admin_editando = false;
}

if (isset($_POST['actualizar_perfil'])) {
	if (empty($_POST["nombre"]) || empty($_POST["apellidos"])) {
		$warning = "<div class='alert alert-danger' role='alert'>Todos los campos marcados con * son obligatorios</div>";

	} else if ((empty($_POST["password"]) || empty($_POST["password2"])) && !$admin_editando) {
		$warning = "<div class='alert alert-danger' role='alert'>Todos los campos marcados con * son obligatorios</div>";

	} else if (strlen($_POST["password"]) < 8 && !$admin_editando) {
		$warning = "<div class='alert alert-danger' role='alert'>La contraseña debe tener al menos 8 caracteres</div>";

	} else if ($_POST["password"] != $_POST["password2"]) {
		$warning = "<div class='alert alert-danger' role='alert'>Las contraseñas no coinciden</div>";

	} else if (strlen($_POST["nombre"]) > 20) {
		$warning = "<div class='alert alert-danger' role='alert'>El nombre no puede tener más de 20 caracteres</div>";

	} else if (strlen($_POST["apellidos"]) > 50) {
		$warning = "<div class='alert alert-danger' role='alert'>Los apellidos no pueden tener más de 50 caracteres</div>";

	} else if (!empty($_POST["localidad"]) && strlen($_POST["localidad"]) > 50) {
		$warning = "<div class='alert alert-danger' role='alert'>La localidad no puede tener más de 50 caracteres</div>";

	} else if (!empty($_POST["telefono"]) && strlen($_POST["telefono"]) > 9) {
		$warning = "<div class='alert alert-danger' role='alert'>El telefono no puede tener más de 9 numeros</div>";

	} else {
		include("templates/admin/conectar_bd_admin.php");

		$nombre = mysqli_real_escape_string($enlace, $_POST["nombre"]);
		$apellidos = mysqli_real_escape_string($enlace, $_POST["apellidos"]);
		if (isset($_POST["rol"])) {
			$rol = $_POST["rol"];
		}

		if (!empty($_POST["localidad"])) {
			$localidad = mysqli_real_escape_string($enlace, $_POST["localidad"]);
		} else {
			$localidad = "";
		}
		if (!empty($_POST["telefono"])) {
			$telefono = mysqli_real_escape_string($enlace, $_POST["telefono"]);
		} else {
			$telefono = "NULL";
		}

		if (empty($_POST["password"])) {
			$sql = "UPDATE usuarios SET nombre = '$nombre', apellidos = '$apellidos', localidad = '$localidad', telefono = $telefono, rol = $rol WHERE email = '$email'";
		} else {
			$password = mysqli_real_escape_string($enlace, $_POST["password"]);
			$password = password_hash($password, PASSWORD_DEFAULT);
			if ($admin_editando) {
				$sql = "UPDATE usuarios SET nombre = '$nombre', apellidos = '$apellidos', password = '$password', localidad = '$localidad', telefono = $telefono, rol = $rol WHERE email = '$email'";
			} else {
				$sql = "UPDATE usuarios SET nombre = '$nombre', apellidos = '$apellidos', password = '$password', localidad = '$localidad', telefono = $telefono WHERE email = '$email'";
			}
		}

		$query = mysqli_query($enlace, $sql);

		if ($query) {
			$warning = "<div class='alert alert-success' role='alert'>Perfil actualizado correctamente</div>";
		} else {
			$warning = "<div class='alert alert-danger' role='alert'>Error al actualizar el perfil</div>";
		}
	}
}

if (isset($_POST['subir'])) {
	$hash = bin2hex(random_bytes(16));

	$archivo_nombre = basename($_FILES["imagen_upload"]["name"]);
	$archivo_tipo = strtolower(pathinfo($archivo_nombre, PATHINFO_EXTENSION));

	$archivo_nombre = $hash . "." . $archivo_tipo;

	$archivo_carpeta = "assets/img/usuario/" . $email . "/";
	if (!file_exists($archivo_carpeta)) {
		mkdir($archivo_carpeta, 0777, true);
	}
	$archivo_ruta = $archivo_carpeta . $archivo_nombre;

	$uploadOk = true;

	// Check if image file is a actual image or fake image
	if (isset($_POST["submit"])) {
		$check = getimagesize($_FILES["imagen_upload"]["tmp_name"]);
		if ($check) {
			$warning .= "<div class='alert alert-danger' role='alert'>El archivo no es una imagen.</div>";
			$uploadOk = false;
		}
	}

	// Check if file already exists
	if (file_exists($archivo_ruta)) {
		$warning .= "<div class='alert alert-danger' role='alert'>El archivo ya existe.</div>";
		$uploadOk = false;
	}

	// Check file size
	if ($_FILES["imagen_upload"]["size"] > 10 * 1000 * 1000) {
		$warning .= "<div class='alert alert-danger'>Fichero demasiado grande.</div>";
		$uploadOk = false;
	}

	// Allow certain file formats
	if ($archivo_tipo != "jpg" && $archivo_tipo != "png" && $archivo_tipo != "jpeg" && $archivo_tipo != "gif" && $archivo_tipo != "webp") {
		$warning .= "<div class='alert alert-danger'>Solo se permiten archivos JPG, JPEG, PNG, GIF o WEBP.</div>";
		$uploadOk = false;
	}

	// Check if $uploadOk is set to 0 by an error
	if (!$uploadOk) {
		$warning .= "<div class='alert alert-danger'>El archivo no ha sido subido.</div>";
		// if everything is ok, try to upload file
	} else {
		if (move_uploaded_file($_FILES["imagen_upload"]["tmp_name"], $archivo_ruta)) {
			include("templates/admin/conectar_bd_admin.php");
			$sql = "UPDATE usuarios SET picture = '$archivo_ruta' WHERE email = '$email'";
			$query = mysqli_query($enlace, $sql);
			if (!$query) {
				$warning .= "<div class='alert alert-danger' role='alert'>Error al insertar en la tabla.</div>";
			} else {
				$warning .= "<div class='alert alert-success' role='alert'>Imagen subida correctamente.</div>";
			}
		} else {
			$warning .= "<div class='alert alert-danger' role='alert'>Error al subir la imagen.</div>";
		}
	}
}
if ($admin_editando) {
	$sql = "SELECT * FROM usuarios WHERE email = '$email'";
	$query = mysqli_query($enlace, $sql);
	$row = mysqli_fetch_array($query);
	$nombre = $row["nombre"];
	$apellidos = $row["apellidos"];
	$localidad = $row["localidad"];
	$telefono = $row["telefono"];
	$img = $row["picture"];
	if ($img == "") {
		$img = "assets/img/usuario/default.png";
	}
	$rol = $row["rol"];
	$titulo = "Editar usuario: " . $nombre . " " . $apellidos;
} else {
	include("templates/user_details.php");
	$nombre = $_SESSION["nombre"];
	$apellidos = $_SESSION["apellidos"];
	$localidad = $_SESSION["localidad"];
	$telefono = $_SESSION["telefono"];
	$img = $_SESSION["img"];
	$rol = $_SESSION["rol"];
	$titulo = "Perfil";
}
?>

<div class="col-12">
	<h1 class="display-4"><?= $titulo ?></h1>
	<hr>
	<?= $warning ?>
</div>

<div class="col-md-3">
	<div class="mb-3 position-relative">
		<img src="<?= $img ?>" alt="profile picture" class="img-fluid rounded-2 mb-2">

		<div class="col-12">
			<form action="#" method="post" enctype="multipart/form-data">
				<div class="mb-3">
					<label for="imagen_upload" class="form-label">Cambiar imagen</label>
					<input class="form-control" type="file" id="imagen_upload" name="imagen_upload">
				</div>
				<div class="mb-3">
					<button type="submit" class="w-100 btn btn-primary" name="subir">Subir</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="col-md-9">
	<form action="#" method="post" class="needs-validation" novalidate>
		<div class="row">

			<div class="mb-3 col-md-6">
				<label for="nombre">Nombre <span class="text-danger">*</span></label>
				<input type="text" class="form-control" id="nombre" name="nombre" value="<?= $nombre ?>" required pattern="[A-ZÁÉÍÓÚ][a-záéíóú]+$">
				<div class="invalid-feedback">Nombre no valido. Una palabra, solo letras, la primera mayuscula.</div>
			</div>

			<div class="mb-3 col-md-6">
				<label for="apellidos">Apellidos <span class="text-danger">*</span></label>
				<input type="text" class="form-control" id="apellidos" name="apellidos" value="<?= $apellidos ?>" required pattern="[A-ZÁÉÍÓÚ][a-záéíóú]+ [A-ZÁÉÍÓÚ][a-záéíóú]+$">
				<div class="invalid-feedback">Apellidos no validos. Dos palabras, solo letras, primera mayuscula.</div>
			</div>

			<div class="mb-3">
				<label for="email">Email</label>
				<input type="email" class="form-control" id="email" value="<?= $email ?>" disabled>
				<div class="invalid-feedback">Email no valido.</div>
			</div>

			<div class="mb-3">
				<label for="localidad">Localidad</label>
				<input type="text" class="form-control" id="localidad" name="localidad" value="<?= $localidad ?>" pattern="[A-ZÁÉÍÓÚ][a-záéíóú]+$">
				<div class="invalid-feedback">Localidad no valida. Una palabra, solo letras, la primera mayuscula.</div>
			</div>

			<div class="mb-3">
				<label for="telefono">Teléfono</label>
				<input type="tel" class="form-control" id="telefono" name="telefono" value="<?= $telefono ?>" pattern="[1-9][0-9]{8}">
				<div class="invalid-feedback">Teléfono no valido. 9 números, el primero del 1 al 9.</div>
			</div>

			<div class="mb-3">
				<label for="password">Contraseña <span class="text-danger"><?= ($admin_editando) ? "" : "*" ?></span></label>
				<input type="password" class="form-control" id="password" name="password" min="8" <?= ($admin_editando) ? "" : "required" ?> pattern=".{8,}">
				<div class="invalid-feedback">Contraseña no valida. Al menos 8 caracteres.</div>
			</div>


			<div class="mb-3">
				<label for="password2">Confirmar contraseña <span class="text-danger"><?= ($admin_editando) ? "" : "*" ?></span></label>
				<input type="password" class="form-control" id="password2" name="password2" min="8" <?= ($admin_editando) ? "" : "required" ?> pattern=".{8,}">
				<div class="invalid-feedback">Contraseña no valida. Al menos 8 caracteres.</div>
			</div>

			<?php
			if ($admin_editando) {
			?>
				<div class="mb-3">
					<label for="rol">Rol</label>
					<select class="form-select" id="rol" name="rol">
						<option value="0" <?= ($rol == 0) ? " selected" : ""; ?>>Estándar</option>
						<option value="1" <?= ($rol == 1) ? " selected" : ""; ?>>Premium</option>
						<option value="2" <?= ($rol == 2) ? " selected" : ""; ?>>Administrador</option>
					</select>
				</div>
			<?php
			}
			?>
			<div class="col-12">
				<button type="submit" name="actualizar_perfil" class="btn btn-primary">Actualizar</button>
			</div>
		</div>
		<script src="assets/js/form-validation.js"></script>
	</form>
</div>