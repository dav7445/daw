<?php
if (!isset($_SESSION['rol'])) {
	header('Location: forbidden.php');
} else {
	include("templates/user_details.php");
	if ($_SESSION['rol'] != 2) {
		header('Location: forbidden.php');
	}
}
