<?php include("templates/admin/admin_access.php"); ?>

<?php include ("templates/admin/functions.php "); ?>

<?php
$warning = "";

$email = $_GET['email'];

if (isset($_POST['borrar'])) {
	include("templates/admin/conectar_bd_admin.php");
	$sql = "DELETE FROM usuarios WHERE email = '$email'";
	$query = mysqli_query($enlace, $sql);
	if ($query) {
		$sql = "DELETE FROM premium WHERE email = '$email'";
		$query = mysqli_query($enlace, $sql);
		if ($query) {
			$sql = "DELETE FROM hoteles_reservas WHERE email = '$email'";
			$query = mysqli_query($enlace, $sql);
			if ($query) {
				$directorio = "assets/img/usuario/$email";
				eliminarCarpeta($directorio);
				$warning = "<div class='alert alert-success' role='alert'>Usuario borrado correctamente</div>";
			} else {
				$warning = "<div class='alert alert-danger' role='alert'>Error al borrar las reservas del usuario</div>";
			}
		} else {
			$warning = "<div class='alert alert-danger' role='alert'>Ha ocurrido un error al borrar las compras de premium del usuario.</div>";
		}
	} else {
		$warning = "<div class='alert alert-danger' role='alert'>Ha ocurrido un error al borrar el usuario</div>";
	}
}

$sql = "SELECT nombre, apellidos FROM usuarios WHERE email = '$email'";
$query = mysqli_query($enlace, $sql);

if (mysqli_num_rows($query) == 0) {
?>
	<div class="col-12">
		<?= $warning ?>
		<div class='alert alert-danger' role='alert'>El usuario no existe</div>
	</div>
<?php
} else {
	$fila = mysqli_fetch_array($query);
	$nombre = $fila['nombre'];
	$apellidos = $fila['apellidos'];

?>
	<div class="col-12">
		<?= $warning; ?>
		<h1 class="display-4">Borrar: <?= $nombre ?> <?= $apellidos ?></h1>
		<hr>
	</div>
	<div class="col-12">
		<form action="#" method="post" class="mb-0">
			<div class="row">
				<div class="mb-3">
					<div class="alert alert-danger" role="alert">
						<h4 class="alert-heading">¡Atención!</h4>
						<p>Esta acción borrará <strong>toda la información</strong> relacionada con el usuario, incluyendo las <strong>compras</strong> y las <strong>reservas</strong>.</p>
						<hr>
						<p class="mb-0">¿Está seguro de que desea borrar el usuario?</p>
					</div>

					<div class="mb-3">
						<button type="button" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#confirmar">Borrar</button>
						<div class="modal fade" id="confirmar" tabindex="-1" aria-labelledby="modallabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="modallabel">Confirma que quieres eliminarlo</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
										<button type="submit" name="borrar" value="<?= $email ?>" class="btn btn-outline-danger">Borrar</button>
									</div>
								</div>
							</div>
						</div>
						<a href="ajustes.php?tab=admin_usuarios" class="btn btn-secondary">Cancelar</a>
					</div>
				</div>
			</div>
		</form>
	</div>
<?php
}
?>