<div class="col-12">
	<h1 class="display-4">Usuarios</h1>
	<hr>
</div>

<div class="col-12 mt-2">
	<?php
	$sql = "SELECT * FROM usuarios";
	$query = mysqli_query($enlace, $sql);
	while ($fila = mysqli_fetch_array($query)) {
		if ($fila['rol'] == 0) {
			$rol = "Estándar";
		} else if ($fila['rol'] == 1) {
			$rol = "Premium";
		} else if ($fila['rol'] == 2) {
			$rol = "Administrador";
		}
	?>
		<div class="card mb-4">
			<div class="card-body">
				<h5 class="card-title"><?= $fila['nombre'] ?> <?= $fila['apellidos'] ?></h5>
				<p class="card-text">
					<small class="text-muted">
						<span class="fw-bold">Email:</span> <?= $fila['email'] ?>
						<br>
						<span class="fw-bold">Rol:</span> <?= $rol ?>
					</small>
				</p>
				<a href="ajustes.php?tab=admin_usuarios&email=<?= $fila['email'] ?>&accion=editar" class="btn btn-primary">Editar</a>
				<a href="ajustes.php?tab=admin_usuarios&email=<?= $fila['email'] ?>&accion=compras" class="btn btn-outline-secondary">Compras</a>
				<a href="ajustes.php?tab=admin_usuarios&email=<?= $fila['email'] ?>&accion=borrar" class="btn btn-outline-danger">Borrar</a>
			</div>
		</div>
	<?php
	}
	?>
</div>