<?php include("templates/admin/admin_access.php"); ?>

<?php
$warning = "";

if (isset($_POST['borrar'])) {
	$tabla = $_POST['tabla'];
	if (isset($_POST["id"])) {
		$sql = "DELETE FROM $tabla WHERE id = $_POST[id]";
	} else {
		$sql = "DELETE FROM $tabla WHERE email = '$_POST[email]'";
	}
	include("templates/admin/conectar_bd_admin.php");
	$query = mysqli_query($enlace, $sql);
	if ($query) {
		$warning = "<div class='alert alert-success' role='alert'>Se ha eliminado el registro.</div>";
	} else {
		$warning = "<div class='alert alert-danger' role='alert'>No se ha podido eliminar el registro.</div>";
	}
}

$sql = "SELECT TABLE_NAME AS tabla FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_SCHEMA='asturviajes'";
$query = mysqli_query($enlace, $sql);
while ($row = mysqli_fetch_array($query)) {
	$tablas[] = $row['tabla'];
}
?>

<div class="col-12">
	<h1 class="display-4">Base de Datos</h1>
	<hr>
</div>

<?= $warning ?>

<ul class="nav nav-tabs" id="myTab" role="tablist">
	<?php
	$i = 0;
	while ($i < count($tablas)) {
		$tabla = $tablas[$i];
		if ($i == 0) {
			$active = " active";
			$aria_selected = "true";
		} else {
			$active = "";
			$aria_selected = "false";
		}
	?>
		<li class="nav-item" role="presentation">
			<button class="nav-link text-dark<?= $active ?>" id="<?= $tabla ?>-tab" data-bs-toggle="tab" data-bs-target="#<?= $tabla ?>-tab-pane" type="button" role="tab" aria-selected="<?= $aria_selected ?>"><?= ucfirst($tabla) ?></button>
		</li>
	<?php
		$i++;
	}
	?>
</ul>
<div class="tab-content" id="myTabContent">
	<?php
	$i = 0;
	while ($i < count($tablas)) {
		$tabla = $tablas[$i];
		$sql = "DESCRIBE $tabla";
		$query = mysqli_query($enlace, $sql);
		$campos = array();
		while ($row = mysqli_fetch_array($query)) {
			$campos[] = $row['Field'];
		}
		if ($i == 0) {
			$active = " show active";
		} else {
			$active = "";
		}
	?>
		<div class="tab-pane fade<?= $active ?>" id="<?= $tabla ?>-tab-pane" role="tabpanel" aria-labelledby="<?= $tabla ?>-tab" tabindex="0">
			<div class="table-responsive">
				<table class="table table-sm table-striped table-hover">
					<thead>
						<tr>
							<th>Borrar</th>
							<?php
							$j = 0;
							while ($j < count($campos)) {
								$campo = $campos[$j];
							?>
								<th><?= $campo ?></th>
							<?php
								$j++;
							}
							?>
						</tr>
					</thead>
					<tbody class="table-group-divider">
						<?php
						$sql = "SELECT * FROM $tabla";
						$query2 = mysqli_query($enlace, $sql);
						$k = 0;
						while ($row2 = mysqli_fetch_array($query2)) {
							if (isset($row2["id"])) {
								$id = $row2["id"];
							} else {
								$id = $k;
								$email = $row2["email"];
							}
						?>
							<tr>
								<td>
									<form class="mb-0" action="#" method="post">
										<button type="button" class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#<?= $tabla ?>_<?= $id ?>"><i class="bi bi-trash-fill"></i></button>
										<div class="modal fade" id="<?= $tabla ?>_<?= $id ?>" tabindex="-1" aria-labelledby="modallabel" aria-hidden="true">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="modallabel">Confirma que quieres eliminarlo</h5>
														<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
														<button type="submit" name="borrar" class="btn btn-outline-danger">Borrar</button>
														<input type="hidden" name="tabla" value="<?= $tabla ?>">
														<?php
														if (isset($row2["id"])) {
														?>
															<input type="hidden" name="id" value="<?= $id ?>">
														<?php
														} else {
														?>
															<input type="hidden" name="email" value="<?= $email ?>">
														<?php
														}
														?>
													</div>
												</div>
											</div>
										</div>
									</form>
								</td>
								<?php
								$j = 0;
								while ($j < count($campos)) {
									$campo = $campos[$j];
								?>
									<td class="text-nowrap"><?= $row2[$campo] ?></td>
								<?php
									$j++;
								}
								?>
							</tr>
						<?php
							$k++;
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	<?php
		$i++;
	}
	?>
</div>