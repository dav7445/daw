<?php include("templates/admin/admin_access.php"); ?>

<?php include ("templates/admin/functions.php "); ?>

<?php
$warning = "";

$hotel_id = $_GET['id'];

if (isset($_POST['borrar'])) {
	include("templates/admin/conectar_bd_admin.php");
	$sql = "DELETE FROM hoteles WHERE id = $hotel_id";
	$query = mysqli_query($enlace, $sql);
	if ($query) {
		$sql = "DELETE FROM hoteles_habitaciones WHERE hotel_id = $hotel_id";
		$query = mysqli_query($enlace, $sql);
		if ($query) {
			$sql = "DELETE FROM hoteles_img WHERE hotel_id = $hotel_id";
			$query = mysqli_query($enlace, $sql);
			if ($query) {
				$sql = "DELETE FROM hoteles_reservas WHERE hotel_id = $hotel_id";
				$query = mysqli_query($enlace, $sql);
				if ($query) {
					$directorio = "assets/img/hotel/$hotel_id";
					eliminarCarpeta($directorio);
					$warning = "<div class='alert alert-success' role='alert'>Hotel borrado correctamente</div>";
				} else {
					$warning = "<div class='alert alert-danger' role='alert'>Ha ocurrido un error al borrar las reservas</div>";
				}
			} else {
				$warning = "<div class='alert alert-danger' role='alert'>Ha ocurrido un error al borrar las imagenes</div>";
			}
		} else {
			$warning = "<div class='alert alert-danger' role='alert'>Ha ocurrido un error al borrar las habitaciones</div>";
		}
	} else {
		$warning = "<div class='alert alert-danger' role='alert'>Ha ocurrido un error al borrar el hotel</div>";
	}
}


$sql = "SELECT nombre FROM hoteles WHERE id = $hotel_id";
$query = mysqli_query($enlace, $sql);

if (mysqli_num_rows($query) == 0) {
?>
	<div class="col-12">
		<?= $warning ?>
		<div class='alert alert-danger' role='alert'>El hotel no existe</div>
	</div>
<?php
} else {
	$fila = mysqli_fetch_array($query);
	$hotel_nombre = $fila['nombre'];

?>
	<div class="col-12">
		<?= $warning; ?>
		<h1 class="display-4">Borrar: <?= $hotel_nombre ?></h1>
		<hr>
	</div>
	<div class="col-12">
		<form action="#" method="post" class="mb-0">
			<div class="row">
				<div class="mb-3">
					<div class="alert alert-danger" role="alert">
						<h4 class="alert-heading">¡Atención!</h4>
						<p>Esta acción borrará <strong>toda la información</strong> relacionada con el hotel, incluyendo las <strong>habitaciones</strong>, <strong>imagenes</strong> y <strong>reservas</strong>.</p>
						<hr>
						<p class="mb-0">¿Está seguro de que desea borrar el hotel?</p>
					</div>

					<div class="mb-3">
						<button type="button" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#confirmar">Borrar</button>
						<div class="modal fade" id="confirmar" tabindex="-1" aria-labelledby="modallabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="modallabel">Confirma que quieres eliminarlo</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
										<button type="submit" name="borrar" value="<?= $hotel_id ?>" class="btn btn-outline-danger">Borrar</button>
									</div>
								</div>
							</div>
						</div>
						<a href="ajustes.php?tab=admin_hoteles" class="btn btn-secondary">Cancelar</a>
					</div>
				</div>
			</div>
		</form>
	</div>
<?php
}
?>