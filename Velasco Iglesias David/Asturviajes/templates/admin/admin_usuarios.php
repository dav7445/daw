<?php include("templates/admin/admin_access.php"); ?>

<?php
if (isset($_GET["accion"]) && $_GET["tab"] = "admin_usuarios" && $_GET["accion"] == "insertar") {
	include("templates/admin/insertar_usuario.php");

} else if (isset($_GET["accion"]) && $_GET["tab"] = "admin_usuarios" && $_GET["accion"] == "editar") {
	include("templates/admin/editar_usuario.php");

} else if (isset($_GET["accion"]) && $_GET["tab"] = "admin_usuarios" && $_GET["accion"] == "borrar") {
	include("templates/admin/borrar_usuario.php");

} else if (isset($_GET["accion"]) && $_GET["tab"] = "admin_usuarios" && $_GET["accion"] == "compras") {
	include("templates/admin/compras_usuario.php");

} else {
	include("templates/admin/dashboard_usuarios.php");
}
