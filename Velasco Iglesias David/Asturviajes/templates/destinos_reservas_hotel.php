<div class="row">
	<div class="col-12">
		<h1 class="display-4">Hoteles</h1>
		<hr>
	</div>
</div>

<?php
foreach ($_GET as $key => $value) {
	if ($value != "") {
?>
		<span class="badge text-bg-primary mb-3"><?= ucfirst($key) ?>: <?= $value ?></span>
<?php
	}
}
?>

<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-xl-4 g-3">
	<?php
	// get all of the url parameters and create a query string from them
	$sql = "SELECT hoteles.id, hoteles.nombre, hoteles.descripcion, hoteles.estrellas, hoteles.premium, hoteles_habitaciones.precio FROM hoteles, hoteles_habitaciones WHERE hoteles.id = hoteles_habitaciones.hotel_id ";
	foreach ($_GET as $key => $value) {
		if ($key != "min" && $key != "max") {
			$value = trim($value);
			$sql .= " AND $key LIKE '%$value%'";
		}
	}
	if (isset($_GET["min"]) && $_GET["min"] != "") {
		$sql .= " AND hoteles_habitaciones.precio >= $_GET[min]";
	}
	if (isset($_GET["max"]) && $_GET["max"] != "") {
		$sql .= " AND hoteles_habitaciones.precio <= $_GET[max]";
	}
	$sql .= " GROUP BY hoteles.id ORDER BY hoteles.estrellas DESC, hoteles_habitaciones.precio ASC";

	$query = mysqli_query($enlace, $sql);
	if (!$query) {
	?>
		<div class="col">
			<div class="alert alert-danger" role="alert">Error en la consulta</div>
		</div>
	<?php
	} else if (mysqli_num_rows($query) == 0) {
	?>
		<div class="col">
			<div class="alert alert-danger" role="alert">No se han encontrado resultados.</div>
		</div>
		<?php
	} else {
		while ($row = mysqli_fetch_assoc($query)) {
			$id = $row["id"];
			$name = $row["nombre"];
			$description = $row["descripcion"];
			$stars = $row["estrellas"];
			$premium = $row["premium"];
			$precio_min = $row["precio"];
		?>
			<div class="col">
				<div class="card shadow-sm h-100 position-relative<?= ($row["premium"]) ? " text-bg-primary" : "" ?>">
					<div class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="2000" style="max-height: 500px">
						<div class="carousel-inner">
							<?php
							// get the images from the database and display them in the carousel
							$sql = "SELECT * FROM hoteles_img WHERE hotel_id = $id";
							$query2 = mysqli_query($enlace, $sql);
							if (mysqli_num_rows($query2) == 0) {
							?>
								<div class="carousel-item active" style="max-height: 500px">
									<img src="assets/img/imagen_no_disponible.png" alt="no_disponible" class="d-block w-100 card-img-top" />
								</div>
								<?php
							} else {
								$i = 0;
								while ($row2 = mysqli_fetch_assoc($query2)) {
									$img = $row2["img"];
								?>
									<div class="carousel-item<?= ($i == 0) ? ' active' : ''; ?>" style="max-height: 500px">
										<img src="<?= $img ?>" alt="<?= $name ?>" class="d-block w-100 card-img-top" />
									</div>
							<?php
									$i++;
								}
							}
							?>
						</div>
					</div>
					<div class="card-body d-flex flex-column">
						<?php
						if ($row["premium"]) {
						?>
							<span class="badge mb-2 text-bg-light">Premium</span>
						<?php
						}
						?>
						<h5 class="card-title"><?= $name ?></h5>
						<p class="card-text"><?= $description ?></p>
						<div class="d-flex mt-auto justify-content-between align-items-center">
							<small>
								<?php
								for ($i = 0; $i < $stars; $i++) {
								?>
									<i class="bi bi-star-fill"></i>
								<?php
								}
								?>
							</small>
							<div class="fw-bold"><?= $precio_min ?>€</div>
						</div>
					</div>
					<a href="hotel.php?id=<?= $id ?>" class="position-absolute top-0 bottom-0 left-0 right-0 index-1"></a>
				</div>
			</div>
	<?php
		}
	}
	?>
</div>