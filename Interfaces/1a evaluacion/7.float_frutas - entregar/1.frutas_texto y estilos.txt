Frutas de verano
Seguramente son las frutas m�s consumidas en verano, por su intenso sabor y su reducido tama�o, lo que las convierte en ideales tentempi�s para picar entre horas.
	
Las cerezas son una fruta t�pica de temporada, precisamente de principios de verano, que se puede consumir s�lo durante 3 � 4 semananas al a�o.

Son las frutas m�s energ�ticas del verano, aunque si te preocupa el peso, debes saber que 100 gramos apenas representan 75 calor�as.

Excelente como diur�tico; as� como antioxidante por sus importantes dosis de vitamina C. Tambi�n tienen un alto contenido en hierro, magnesio y postasio.
        
Las uvas son ricas en az�cares, hasta un 16% aproximadamente. Son glucosas y fructuosas, de mejor asimilaci�n que la sacarosa o az�car blanco.

Sin duda son dos excelentes frutas que te ayudaran a combatir los calores veraniegos, refresc�ndote y aliment�ndote sanamente con su moderado consumo. No dudes en probarlas e incluirla en tu dieta diaria, obtendr�s maravillosos beneficios.


/* estilos css */

body{
    font-family: Verdana, Geneva, sans-serif;
    width: 800px;
}

p {
    font-size: 14px;
    line-height: 1.3rem;
}