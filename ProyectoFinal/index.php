<?php include("templates/head.php"); ?>

<?php
session_start();
include("templates/conectar_bd_consultor.php");

include("templates/user_details.php");
?>

<body>
	<div class="position-relative min-vh-100 d-flex flex-column justify-content-between">

		<?php include("templates/header.php"); ?>

		<div class="content-hero d-flex flex-column py-5 py-md-0 mb-5 mb-md-0 text-center border-bottom">
			<div class="d-flex flex-column justify-content-center flex-grow-1 mb-md-3">
				<h1 class="display-4 fw-bold">AsturViajes</h1>
				<div class="col-lg-6 mx-auto">
					<p class="lead mb-4">El placer de viajar</p>
					<div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
						<a href="destinos.php" type="button" class="btn btn-primary btn-lg px-4">Ver Destinos</a>
						<?php
						if (!isset($_SESSION['rol'])) {
						?>
							<a href="signin.php" type="button" class="btn btn-outline-primary btn-lg px-4">Iniciar Sesión</a>
						<?php
						}
						?>
					</div>
				</div>
			</div>
			<div class="overflow-hidden d-none d-md-block" style="max-height: 30vh">
				<div class="container px-5">
					<img src="assets/img/website/avion.jpg" class="img-fluid border rounded-4 shadow-lg mb-4" alt="Example image" width="700" height="500" loading="lazy" />
				</div>
			</div>
		</div>

		<div class="container py-md-5">
			<h2 class="pb-2 border-bottom">Escoge tu destino</h2>

			<div class="row row-cols-1 row-cols-md-2 row-cols-xxl-4 align-items-stretch g-4 py-5">
				<div class="col">
					<a href="destinos.php?localizacion=playa" class="text-decoration-none link-light">
						<div class="card card-cover h-100 border-0 overflow-hidden text-white bg-dark rounded-4 shadow-lg">
							<div class="img-cover">
								<img class="img-fluid card-img-top" src="assets/img/website/playa.jpg" />
							</div>
							<div class="d-flex flex-column h-100 p-5 pb-3 text-shadow-1 index-1">
								<h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Playa</h2>
							</div>
						</div>
					</a>
				</div>

				<div class="col">
					<a href="destinos.php?localizacion=ciudad" class="text-decoration-none link-light">
						<div class="card card-cover h-100 border-0 overflow-hidden text-white bg-dark rounded-4 shadow-lg">
							<div class="img-cover">
								<img class="img-fluid card-img-top" src="assets/img/website/torre_eiffel.jpg" />
							</div>
							<div class="d-flex flex-column h-100 p-5 pb-3 text-shadow-1 index-1">
								<h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Ciudad</h2>
							</div>
						</div>
					</a>
				</div>

				<div class="col">
					<a href="destinos.php?localizacion=montaña" class="text-decoration-none link-light">
						<div class="card card-cover h-100 border-0 overflow-hidden text-white bg-dark rounded-4 shadow-lg">
							<div class="img-cover">
								<img class="img-fluid card-img-top" src="assets/img/website/montaña.jpg" />
							</div>
							<div class="d-flex flex-column h-100 p-5 pb-3 text-shadow-1 index-1">
								<h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Montaña</h2>
							</div>
						</div>
					</a>
				</div>

				<div class="col">
					<a href="destinos.php?localizacion=campo" class="text-decoration-none link-light">
						<div class="card card-cover h-100 border-0 overflow-hidden text-white bg-dark rounded-4 shadow-lg">
							<div class="img-cover">
								<img class="img-fluid card-img-top" src="assets/img/website/campo.jpg" />
							</div>
							<div class="d-flex flex-column h-100 p-5 pb-3 text-shadow-1 index-1">
								<h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Campo</h2>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>

		<div class="container">
			<div id="carousel" class="carousel slide carousel-fade" data-bs-ride="carousel" style="max-height: 500px">
				<div class="carousel-inner rounded-4">
					<?php
					/* select distict hotel_id from hoteles_img */
					$sql = "SELECT DISTINCT hotel_id FROM hoteles_img";
					$query = mysqli_query($enlace, $sql);
					/* numbers from query to array */
					$hoteles_con_imagen = array();
					while ($row = mysqli_fetch_array($query)) {
						array_push($hoteles_con_imagen, $row['hotel_id']);
					}

					/* random hotel_id from array */
					$random_hotel_id = $hoteles_con_imagen[array_rand($hoteles_con_imagen)];

					/* select hoteles_img */
					$sql = "SELECT hoteles_img.img, hoteles.id, hoteles.nombre FROM hoteles_img, hoteles WHERE hoteles_img.hotel_id = hoteles.id AND hoteles_img.hotel_id = $random_hotel_id";
					$query = mysqli_query($enlace, $sql);
					$i = 0;
					while ($row = mysqli_fetch_array($query)) {
					?>
						<div class="carousel-item<?= ($i == 0) ? ' active' : ''; ?>" style="height: 500px">
							<img src="<?= $row["img"] ?>" class="d-block w-100 h-100 card-img-top">
							<div class="carousel-caption d-none d-md-block">
								<h2><a href="hotel.php?id=<?= $row["id"] ?>" class="text-decoration-none link-light"><?= $row["nombre"] ?></a></h2>
							</div>
						</div>
					<?php
						$i++;
					}
					?>
				</div>
				<button class="carousel-control-prev gradient-left rounded-4" type="button" data-bs-target="#carousel" data-bs-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Anterior</span>
				</button>
				<button class="carousel-control-next gradient-right rounded-4" type="button" data-bs-target="#carousel" data-bs-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Siguiente</span>
				</button>
			</div>
		</div>

		<?php include("templates/footer.php"); ?>
	</div>
</body>