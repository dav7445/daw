<?php include("templates/head.php"); ?>

<?php
session_start();
include("templates/conectar_bd_consultor.php");

include("templates/user_details.php");

$warning = "";
$today = date("Y-m-d");
$today_completo = date("Y-m-d H:i:s");
$next_year = date("Y-m-d H:i:s", strtotime("+1 year"));

/* create session variable for the cart */
if (!isset($_SESSION["cart"])) {
	$_SESSION["cart"] = array();
}

if (isset($_POST["quitar"])) {
	$id = $_POST["quitar"];
	for ($i = 0; $i < count($_SESSION["cart"]); $i++) {
		if ($_SESSION["cart"][$i]["id_compra"] == $id) {
			array_splice($_SESSION["cart"], $i, 1);
		}
	}
} else if (isset($_POST["comprar"])) {
	/* go through the cart array and make a reserve in hoteles_reservas */
	include("templates/admin/conectar_bd_admin.php");
	for ($i = 0; $i < count($_SESSION["cart"]); $i++) {
		$email = $_SESSION["email"];
		$id = $_SESSION["cart"][$i]["id_producto"];
		$precio = $_SESSION["cart"][$i]["precio"];

		if ($id == "premium") {
			$fecha_inicio = $today;
			$fecha_fin = $next_year;

			$sql = "INSERT INTO premium (email, fecha_compra, fecha_fin, precio) VALUES ('$email', '$today_completo', '$fecha_fin', '$precio')";
			$query = mysqli_query($enlace, $sql);

			if ($query) {
				$sql = "UPDATE usuarios SET rol = 1 WHERE email = '$email'";
				$query = mysqli_query($enlace, $sql);
			} else {
				$warning = '<div class="alert alert-danger" role="alert"><strong>!Error!</strong> No se ha podido realizar la compra.</div>';
				break;
			}
		} else {
			$entrada = $_SESSION["cart"][$i]["fecha_entrada"];
			$salida = $_SESSION["cart"][$i]["fecha_salida"];
			$habitacion = $_SESSION["cart"][$i]["habitacion"];
			$adultos = $_SESSION["cart"][$i]["adultos"];
			$ninos = $_SESSION["cart"][$i]["ninos"];
			$noches = $_SESSION["cart"][$i]["noches"];

			$sql = "INSERT INTO hoteles_reservas (id, email, hotel_id, habitacion_id, entrada, salida, adultos, ninos, noches, precio) VALUES (NULL, '$email', '$id', $habitacion, '$entrada', '$salida', '$adultos', '$ninos', '$noches', '$precio')";
			$query = mysqli_query($enlace, $sql);
		}

		if ($query) {
			array_splice($_SESSION["cart"], $i, 1);
			$i--;
		} else {
			$warning = '<div class="alert alert-danger" role="alert"><strong>!Error!</strong> No se ha podido realizar la compra.</div>';
			break;
		}
	}

	if ($warning == "") {
		$warning = '<div class="alert alert-success" role="alert"><strong>¡Éxito!</strong> Se han realizado las reservas correctamente.</div>';
	}
}

?>

<body>
	<div class="position-relative min-vh-100 d-flex flex-column justify-content-between">

		<?php include("templates/header.php"); ?>

		<form action="#" method="post" class="" novalidate>
			<div class="container py-3">
				<div class="row">
					<?php
					if (!isset($_SESSION["email"])) {
					?>
						<div class="col-12">
							<div class='alert alert-danger' role='alert'>
								Para comprar debes <a class="alert-link" href='signin.php'>iniciar sesión</a>
							</div>

						</div>
					<?php
					}
					?>
					<div class="col-12">
						<h1 class="display-4">Carrito</h1>
						<hr>
						<?php
						/* if the cart array is empty show this message */
						if (empty($_SESSION["cart"])) {
						?>
							<?= $warning; ?>
							<div class='alert alert-warning' role='alert'>
								El carrito está vacío.
							</div>
						<?php
						} else {
						?>
							<ul class="list-group mb-3">
								<?php
								$total = 0;
								foreach ($_SESSION["cart"] as $item) {
									$total += $item["precio"];
								?>
									<li class="list-group-item d-flex justify-content-between lh-sm">

										<div>
											<?php
											/* if the item has noches then multiply the price by the number of noches */
											if (isset($item["noches"])) {
												/* select from hoteles_habitaciones tipo where id = $item["habitacion"] */
												$sql = "SELECT tipo FROM hoteles_habitaciones WHERE id = $item[habitacion]";
												$query = mysqli_query($enlace, $sql);
												$row = mysqli_fetch_array($query);
												$tipo = $row["tipo"];
											?>
												<h6 class="mb-0"><a href="hotel.php?id=<?= $item["id_producto"]; ?>"><?= $item["nombre"]; ?></a></h6>
												<small class="text-muted"><?= ucfirst($tipo) ?> x <?= $item["noches"]; ?> noche/s</small>
												<br>
												<?php
												if ($item["adultos"] > 0) {
												?>
													<small class="text-muted">Adultos: <?= $item["adultos"] ?></small>
												<?php
												}
												if ($item["ninos"] > 0) {
												?>
													<small class="text-muted">Niños: <?= $item["ninos"] ?></small>
												<?php
												}
											} else {
												?>
												<h6 class="mb-0"><?= $item["nombre"]; ?></h6>
												<small class="text-muted"><?= $item["descripcion"] ?></small>
											<?php
											}
											?>
										</div>
										<div class="text-end">
											<span class="text-muted"><?= $item["precio"]; ?>€</span>
											<!-- boton quitar del carrito -->
											<button type="submit" class="btn btn-danger btn-sm" name="quitar" value="<?= $item["id_compra"] ?>">
												<i class="bi bi-trash-fill"></i>
											</button>
										</div>

									</li>
								<?php
								}
								?>
								<li class="list-group-item d-flex justify-content-between">
									<span>Total</span>
									<strong><?= $total; ?>€</strong>
								</li>
							</ul>
							<?php
							/* if the user is logged in */
							if (isset($_SESSION["email"])) {
								echo $warning;
							?>
								<button class="btn btn-primary mb-3 mb-md-0" type="submit" name="comprar">Comprar</button>
						<?php
							}
						}
						?>
					</div>
				</div>
			</div>
		</form>

		<?php include("templates/footer.php"); ?>
	</div>
	<script src="assets/js/form-validation.js"></script>
</body>