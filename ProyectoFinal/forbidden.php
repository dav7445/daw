<?php include("templates/head.php"); ?>

<?php header("HTTP/1.1 403 Forbidden"); ?>

<body>
	<div class="position-relative min-vh-100 d-flex flex-column justify-content-between">

		<?php include("templates/header_simple.php"); ?>

		<h1 class="display-4 text-danger text-center">403 Acceso denegado</h1>

		<?php include("templates/footer.php"); ?>
	</div>
</body>

<?php die(); ?>