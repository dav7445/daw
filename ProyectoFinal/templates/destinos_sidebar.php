<?php
/* get filters from url if there are any, if not, set them */
/* nombre, ciudad, pais, estrellas */
$filtro_nombre = isset($_GET['nombre']) ? $_GET['nombre'] : '';
$filtro_ciudad = isset($_GET['ciudad']) ? $_GET['ciudad'] : '';
$filtro_pais = isset($_GET['pais']) ? $_GET['pais'] : '';
$filtro_localizacion = isset($_GET['localizacion']) ? $_GET['localizacion'] : '';
$filtro_estrellas = isset($_GET['estrellas']) ? $_GET['estrellas'] : '';
$filtro_min = isset($_GET['min']) ? $_GET['min'] : '';
$filtro_max = isset($_GET['max']) ? $_GET['max'] : '';

if (isset($_POST["buscar"])) {
	$min = $_POST["min"];
	$max = $_POST["max"];

	if ($min != '' && $max != '' && $min > $max) {
		$aux = $min;
		$min = $max;
		$max = $aux;
	}

	$url = "destinos.php?nombre=" . $_POST["nombre"] . "&ciudad=" . $_POST["ciudad"] . "&pais=" . $_POST["pais"] . "&localizacion=" . $_POST["localizacion"] . "&estrellas=" . $_POST["estrellas"] . "&min=" . $min . "&max=" . $max;
	header("Location: $url");
}
?>

<form action="#" class="mb-0" method="post">
	<div class="row">
		<div class="mb-3">
			<label for="nombre" class="form-label">Nombre</label>
			<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Hotel" value="<?= $filtro_nombre ?>">
		</div>

		<div class="col-12 col-sm-6 col-md-12 mb-3">
			<label for="ciudad" class="form-label">Ciudad</label>
			<select class="form-select" id="ciudad" name="ciudad">
				<option value="">Todas</option>
				<?php
				$sql = "SELECT DISTINCT ciudad FROM hoteles";
				$query = mysqli_query($enlace, $sql);
				while ($row = mysqli_fetch_assoc($query)) {
					$ciudad = $row["ciudad"];
				?>
					<option value="<?= $ciudad; ?>" <?php if ($filtro_ciudad == $ciudad) { ?> selected <?php } ?>><?= $ciudad; ?></option>
				<?php
				}
				?>
			</select>
		</div>

		<div class="col-12 col-sm-6 col-md-12 mb-3">
			<label class="form-label">País</label>
			<select class="form-select" id="pais" name="pais">
				<option value="">Todos</option>
				<?php
				$sql = "SELECT DISTINCT pais FROM hoteles";
				$query = mysqli_query($enlace, $sql);
				while ($row = mysqli_fetch_assoc($query)) {
					$pais = $row["pais"];
				?>
					<option value="<?= $pais; ?>" <?php if ($filtro_pais == $pais) { ?> selected <?php } ?>><?= $pais; ?></option>
				<?php
				}
				?>
			</select>
		</div>

		<div class="col-12 col-sm-6 col-md-12 mb-3">
			<label class="form-label">Localización</label>
			<select class="form-select" id="localizacion" name="localizacion">
				<option value="">Todas</option>
				<?php
				$sql = "SELECT DISTINCT localizacion FROM hoteles";
				$query = mysqli_query($enlace, $sql);
				while ($row = mysqli_fetch_assoc($query)) {
					$localizacion = $row["localizacion"];
				?>
					<option value="<?= $localizacion; ?>" <?php if ($filtro_localizacion == $localizacion) { ?> selected <?php } ?>><?= ucfirst($localizacion); ?></option>
				<?php
				}
				?>
			</select>
		</div>

		<div class="col-12 col-sm-6 col-md-12 mb-3">
			<label class="form-label">Estrellas</label>
			<select class="form-select" id="estrellas" name="estrellas">
				<option value="">Todas</option>
				<?php
				$sql = "SELECT DISTINCT estrellas FROM hoteles";
				$query = mysqli_query($enlace, $sql);
				while ($row = mysqli_fetch_assoc($query)) {
					$estrellas = $row["estrellas"];
				?>
					<option value="<?= $estrellas; ?>" <?php if ($filtro_estrellas == $estrellas) { ?> selected <?php } ?>><?= $estrellas; ?></option>
				<?php
				}
				?>
			</select>
		</div>

		<div class="col-12 col-sm-6 col-md-12 mb-3">
			<label for="min" class="form-label">Precio Mínimo</label>
			<div class="input-group">
				<input type="number" class="form-control" id="min" name="min" min="0" value="<?= $filtro_min ?>">
				<span class="input-group-text">€</span>
			</div>
		</div>

		<div class="col-12 col-sm-6 col-md-12 mb-3">
			<label for="max" class="form-label">Precio Máximo</label>
			<div class="input-group">
				<input type="number" class="form-control" id="max" name="max" min="0" value="<?= $filtro_max ?>">
				<span class="input-group-text">€</span>
			</div>
		</div>

		<div class="mb-3">
			<button type="submit" name="buscar" class="btn btn-primary w-100">Buscar</button>
		</div>
	</div>
</form>