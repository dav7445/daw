<?php include("templates/user_details.php"); ?>

<header class="border-bottom topnavbar bg-light shadow-sm">
	<div class="container d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3">
		<a href="index.php" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
			<h3 class="mb-0">AsturViajes</h3>
		</a>
		<ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
			<li><a href="index.php" class="nav-link px-2<?= (basename($_SERVER['PHP_SELF']) == "index.php") ? " text-dark" : " text-secondary"; ?>">Inicio</a></li>

			<li><a href="destinos.php" class="nav-link px-2<?= (basename($_SERVER['PHP_SELF']) == "destinos.php") ? " text-dark" : " text-secondary"; ?>">Destinos</a></li>

			<li><a href="plans.php" class="nav-link px-2<?= (basename($_SERVER['PHP_SELF']) == "plans.php") ? " text-dark" : " text-secondary"; ?>">Premium</a></li>
		</ul>
		<div class="col-xl-3 text-end">
			<?php
			// Si no esta logueado, mostramos el boton de login
			if (!isset($_SESSION["rol"])) {
			?>
				<a href="signin.php" type="button" class="btn btn-outline-primary">Iniciar Sesión</a>
				<a href="signup.php" type="button" class="btn btn-primary">Registrarse</a>
			<?php
			} else {
				// Si esta logueado, mostramos el boton de logout y el nombre del usuario
			?>
				<div class="dropdown text-end">
					<a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser" data-bs-toggle="dropdown" aria-expanded="false">
						<img src="<?= $_SESSION["img"] ?>" alt="profile" width="32" height="32" class="rounded-circle" />
					</a>
					<ul class="dropdown-menu dropdown-menu-end text-small" aria-labelledby="dropdownUser">
						<li class="px-3">
							<div class="d-flex align-items-center position-relative">
								<div class="avatar me-2">
									<img class="avatar-img rounded-circle" src="<?= $_SESSION["img"] ?>" alt="avatar">
								</div>
								<div>
									<a class="h6 text-decoration-none fw-bold text-nowrap" href="ajustes.php?tab=perfil"><?= $_SESSION["nombre"] ?> <?= $_SESSION["apellidos"] ?></a>
									<p class="small m-0"><?= $_SESSION["rol_nombre"] ?></p>
								</div>
							</div>
						</li>
						<li>
							<hr class="dropdown-divider" />
						</li>
						<li>
							<a class="dropdown-item py-2" href="compra.php"><i class="bi bi-cart me-2"></i>Carrito</a>
						</li>
						<li>
							<a class="dropdown-item py-2" href="ajustes.php?tab=perfil"><i class="bi bi-gear me-2"></i>Ajustes</a>
						</li>
						<?php
						// Si es admin, mostramos el boton de admin
						if ($_SESSION["rol"] == 2) {
						?>
							<a class="dropdown-item py-2" href="admin.php"><i class="bi bi-shield me-2"></i>Admin</a>
						<?php
						}
						?>
						<li>
							<hr class="dropdown-divider" />
						</li>
						<li>
							<a class="dropdown-item text-danger py-2" href="logout.php"><i class="bi bi-power me-2"></i>Cerrar sesión</a>
						</li>
					</ul>
				</div>
			<?php
			}
			?>
		</div>
	</div>
</header>