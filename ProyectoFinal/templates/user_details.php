<?php
if (isset($_SESSION['email'])) {
	include("templates/conectar_bd_consultor.php");
	$sql = "SELECT nombre, apellidos, rol, picture, localidad, telefono FROM usuarios WHERE email = '" . $_SESSION['email'] . "'";
	$query = mysqli_query($enlace, $sql);
	if (mysqli_num_rows($query) == 0) {
		header("Location: logout.php");
	}
	$row = mysqli_fetch_array($query);
	$_SESSION["nombre"] = $row['nombre'];
	$_SESSION["apellidos"] = $row['apellidos'];
	$_SESSION["localidad"] = $row['localidad'];
	$_SESSION["telefono"] = $row['telefono'];
	$_SESSION["img"] = $row['picture'];
	if ($_SESSION["img"] == "") {
		$_SESSION["img"] = "assets/img/usuario/default.png";
	}
	$_SESSION["rol"] = $row['rol'];

	/* Asegurarse de que el usuario no tiene el premium expirado */
	if ($_SESSION["rol"] == 1) {
		$sql = "SELECT fecha_fin FROM premium WHERE email = '" . $_SESSION['email'] . "' ORDER BY id DESC LIMIT 1";
		$query = mysqli_query($enlace, $sql);
		if (mysqli_num_rows($query) == 0) {
			$_SESSION["rol"] = 0;
		} else {
			$row = mysqli_fetch_array($query);
			$fecha_fin = $row['fecha_fin'];
			$today = date("Y-m-d H:i:s");
			if ($fecha_fin < $today) {
				/* update rol in database */
				include("templates/admin/conectar_bd_admin.php");
				$_SESSION["rol"] = 0;
				$sql = "UPDATE usuarios SET rol = 0 WHERE email = '" . $_SESSION['email'] . "'";
				include("templates/conectar_bd_consultor.php");
			} else {
				$_SESSION["rol"] = 1;
			}
		}
	}

	if ($_SESSION["rol"] == 0) {
		$_SESSION["rol_nombre"] = "Estándar";
	} else if ($_SESSION["rol"] == 1) {
		$_SESSION["rol_nombre"] = "Premium";
	} else if ($_SESSION["rol"] == 2) {
		$_SESSION["rol_nombre"] = "Administrador";
	}
}
