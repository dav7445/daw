<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>AsturViajes</title>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/style.css" rel="stylesheet" />
	<link rel="stylesheet" href="assets/icons/bootstrap-icons.css" />
	<script src="assets/js/bootstrap.bundle.min.js"></script>
</head>
