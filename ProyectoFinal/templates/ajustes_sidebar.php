<?php include("templates/user_details.php"); ?>

<ul class="nav nav-pills flex-column mb-auto">

	<li class="nav-item">
		<small>Perfil</small>
	</li>

	<li class="nav-item">
		<a href="ajustes.php?tab=perfil" aria-current="page" class="nav-link<?= (isset($_GET['tab']) && $_GET['tab'] == 'perfil') ? " active" : " link-dark"; ?>"><i class="bi bi-person-fill me-2"></i>Perfil</a>
	</li>

	<li class="nav-item">
		<a href="ajustes.php?tab=compras" aria-current="page" class="nav-link<?= (isset($_GET['tab']) && $_GET['tab'] == 'compras') ? " active" : " link-dark"; ?>"><i class="bi bi-cart-fill me-2"></i>Compras</a>
	</li>

	<?php
	if ($_SESSION['rol'] == 2) {
	?>

		<li class="border-top my-3"></li>
		<li class="nav-item">
			<small>Admin</small>
		</li>
		<li class="nav-item">
			<a href="ajustes.php?tab=admin_hoteles" class="nav-link<?= (isset($_GET['tab']) && $_GET['tab'] == 'admin_hoteles') ? " active" : " link-dark"; ?>"><i class="bi bi-house-fill me-2"></i>Hoteles</a>
		</li>

		<li class="nav-item">
			<a href="ajustes.php?tab=admin_usuarios" class="nav-link<?= (isset($_GET['tab']) && $_GET['tab'] == 'admin_usuarios') ? " active" : " link-dark"; ?>"><i class="bi bi-people-fill me-2"></i>Usuarios</a>
		</li>

		<li class="nav-item">
			<a href="ajustes.php?tab=admin_basedatos" class="nav-link<?= (isset($_GET['tab']) && $_GET['tab'] == 'admin_basedatos') ? " active" : " link-dark"; ?>"><i class="bi bi-table me-2"></i>Base de Datos</a>
		</li>
	<?php
	}
	?>

	<li class="border-top my-3"></li>
	<li class="nav-item">
		<a href="logout.php" class="w-100 text-start btn btn-outline-danger border-0 ps-3"><i class="bi bi-power me-2"></i>Cerrar sesión</a>
	</li>
</ul>