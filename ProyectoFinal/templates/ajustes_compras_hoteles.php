<?php
$warning = "";

if (isset($_POST["cancelar_reserva"])) {
	include("templates/admin/conectar_bd_admin.php");
	$id_reserva = $_POST["cancelar_reserva"];
	$sql = "UPDATE hoteles_reservas SET cancelada = 1 WHERE id = $id_reserva";
	$query = mysqli_query($enlace, $sql);
	if ($query) {
		$warning = '<div class="alert alert-success" role="alert">Reserva cancelada correctamente</div>';
	} else {
		$warning = '<div class="alert alert-danger" role="alert">Ha ocurrido un error al cancelar la reserva</div>';
	}
}

?>

<div class="col-12">
	<h1 class="display-4">Reservas Hotel</h1>
	<hr>
	<?= $warning ?>
</div>

<?php

$today = date("Y-m-d");

$sql = "SELECT * FROM hoteles, hoteles_habitaciones, hoteles_reservas WHERE email = '$email' AND hoteles_reservas.hotel_id = hoteles.id AND hoteles_reservas.habitacion_id = hoteles_habitaciones.id";
$query = mysqli_query($enlace, $sql);

if (mysqli_num_rows($query) == 0) {
?>
	<div class="col-12">
		<div class='alert alert-info'>No hay reservas</div>
	</div>
	<?php
} else {
	while ($row = mysqli_fetch_assoc($query)) {
		$id_reserva = $row["id"];
		$hotel_id = $row["hotel_id"];
		$habitacion = $row["habitacion_id"];
		$nombre_hotel = $row["nombre"];
		$entrada = $row["entrada"];
		$salida = $row["salida"];
		$habitacion_tipo = $row["tipo"];
		$adultos = $row["adultos"];
		$ninos = $row["ninos"];
		$precio = $row["precio"];
		$cancelada = $row["cancelada"];
	?>
		<div class="col-12">
			<div class="card mb-3 <?= ($cancelada) ? "text-bg-light" : "border-dark" ?>">
				<h5 class="card-header d-flex justify-content-between">
					<div class="d-flex align-items-center">Reserva #<?= $id_reserva; ?><?= ($cancelada) ? " (Cancelada)" : "" ?></div>
					<?php
					if ($today <= $entrada && !$cancelada) {
					?>
						<div class="d-flex">
							<form action="#" class="mb-0" method="post">
								<button type="button" class="btn btn-sm btn-outline-danger" data-bs-toggle="modal" data-bs-target="#confirmar_hotel<?= $id_reserva; ?>">Cancelar Reserva</button>
								<div class="modal fade" id="confirmar_hotel<?= $id_reserva; ?>" tabindex="-1" aria-labelledby="modallabel_hotel<?= $id_reserva; ?>" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="modallabel_hotel<?= $id_reserva; ?>">Confirma que quieres cancelar la reserva #<?= $id_reserva; ?></h5>
												<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Volver</button>
												<button type="submit" name="cancelar_reserva" value="<?= $id_reserva; ?>" class="btn btn-outline-danger">Cancelar Reserva</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					<?php
					}
					?>
				</h5>
				<div class="card-body">
					<div class="row">

						<div class="col-md-4 d-flex align-items-end justify-content-md-end">
							<h1 class="display-5"><?= $precio; ?><small>€</small></h1>
						</div>

						<div class="col-md-8 order-md-first">
							<h5 class="card-title"><?= $nombre_hotel; ?></h5>
							<div class="card-text">
								<div class="row">
									<div class="col-12"><strong>Habitación:</strong> <?= ucfirst($habitacion_tipo); ?></div>
									<div class="col-6"><strong>Entrada:</strong> <?= $entrada; ?></div>
									<div class="col-6"><strong>Salida:</strong> <?= $salida; ?></div>
									<div class="col-6"><strong>Adultos:</strong> <?= $adultos; ?></div>
									<div class="col-6"><strong>Niños:</strong> <?= $ninos; ?></div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
<?php
	}
}
?>