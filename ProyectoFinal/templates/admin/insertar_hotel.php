<?php include("templates/admin/admin_access.php"); ?>

<?php
$warning = "";

if (isset($_POST['insertar'])) {
	$nombre = $_POST['nombre'];
	$descripcion = $_POST['descripcion'];
	$direccion = $_POST['direccion'];
	$ciudad = $_POST['ciudad'];
	$pais = $_POST['pais'];
	$localizacion = $_POST['localizacion'];
	$estrellas = $_POST['estrellas'];

	include("templates/admin/conectar_bd_admin.php");
	$sql = "INSERT INTO hoteles (nombre, descripcion, direccion, ciudad, pais, localizacion, estrellas) VALUES ('$nombre', '$descripcion', '$direccion', '$ciudad', '$pais', '$localizacion', '$estrellas')";
	$query = mysqli_query($enlace, $sql);
	if ($query) {
		$warning = "<div class='alert alert-success'>Hotel insertado correctamente</div>";
		$sql = "SELECT id FROM hoteles ORDER BY id DESC LIMIT 1";
		$query = mysqli_query($enlace, $sql);
		$row = mysqli_fetch_array($query);
		$id = $row['id'];
		$warning .= "<a class='btn btn-primary me-2' href='hotel.php?id=$id'>Ver</a><a class='btn btn-outline-primary' href='ajustes.php?tab=admin_hoteles&id=$id&accion=editar'>Editar</a>";
	} else {
		$warning = "<div class='alert alert-danger'>Error al insertar el hotel</div>";
	}
}
?>

<div class="col-12">
	<?= $warning ?>
	<h1 class="display-4">Insertar Hotel</h1>
	<hr>
</div>
<div class="col-12">
	<form action="#" method="post">
		<div class="row">

			<div class="mb-3">
				<h1 class="display-5">Información</h1>
			</div>

			<div class="mb-3">
				<label for="nombre">Nombre</label>
				<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
			</div>
			<div class="mb-3">
				<label for="descripcion">Descripción</label>
				<textarea class="form-control" id="descripcion" name="descripcion" rows="3" placeholder="Descripción" required></textarea>
			</div>
			<div class="mb-3 col-md-3">
				<label for="direccion">Dirección</label>
				<input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" required>
			</div>
			<div class="mb-3 col-md-3">
				<label for="ciudad">Ciudad</label>
				<input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="Ciudad" required>
			</div>
			<div class="mb-3 col-md-3">
				<label for="pais">País</label>
				<input type="text" class="form-control" id="pais" name="pais" placeholder="País" required>
			</div>
			<div class="mb-3 col-md-3">
				<label for="localizacion">Localización</label>
				<select class="form-select" id="localizacion" name="localizacion" required>
					<option value="playa">Playa</option>
					<option value="ciudad">Ciudad</option>
					<option value="montaña">Montaña</option>
					<option value="campo">Campo</option>
				</select>
			</div>
			<div class="mb-3">
				<label for="estrellas">Estrellas</label>
				<select class="form-select" id="estrellas" name="estrellas" required>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>
			</div>

			<div class="mb-3">
				<button type="submit" class="btn btn-primary" name="insertar">Insertar</button>
				<a href="ajustes.php?tab=admin_hoteles" class="btn btn-outline-secondary">Cancelar</a>
			</div>
		</div>
	</form>
</div>