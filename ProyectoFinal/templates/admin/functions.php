<?php
function eliminarCarpeta($path)
{
	if (is_dir($path) === true) {
		$archivos = array_diff(scandir($path), array('.', '..'));

		foreach ($archivos as $archivo) {
			eliminarCarpeta(realpath($path) . '/' . $archivo);
		}

		return rmdir($path);
	} else if (is_file($path) === true) {
		return unlink($path);
	}

	return false;
}