<?php include("templates/admin/admin_access.php"); ?>

<?php
if (isset($_GET["accion"]) && $_GET["tab"] = "admin_hoteles" && $_GET["accion"] == "insertar") {
	include("templates/admin/insertar_hotel.php");

} else if (isset($_GET["accion"]) && $_GET["tab"] = "admin_hoteles" && $_GET["accion"] == "editar") {
	include("templates/admin/editar_hotel.php");

} else if (isset($_GET["accion"]) && $_GET["tab"] = "admin_hoteles" && $_GET["accion"] == "borrar") {
	include("templates/admin/borrar_hotel.php");

} else {
	include("templates/admin/dashboard_hoteles.php");
}
