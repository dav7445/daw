<?php include("templates/admin/admin_access.php"); ?>

<?php
$warning = "";

$hotel_id = $_GET['id'];

if (isset($_POST['editar'])) {
	include("templates/admin/conectar_bd_admin.php");

	if ($_POST["editar"] == "informacion") {
		$nombre = $_POST['nombre'];
		$direccion = $_POST['direccion'];
		$descripcion = $_POST['descripcion'];
		$direccion = $_POST['direccion'];
		$ciudad = $_POST['ciudad'];
		$pais = $_POST['pais'];
		$localizacion = $_POST['localizacion'];
		$estrellas = $_POST['estrellas'];
		$sql = "UPDATE hoteles SET nombre='$nombre', direccion='$direccion', descripcion='$descripcion', ciudad='$ciudad', pais='$pais', localizacion='$localizacion', estrellas=$estrellas WHERE id=$hotel_id";
		$query = mysqli_query($enlace, $sql);
		if ($query) {
			$warning = "<div class='alert alert-success'>Información editada correctamente</div>";
		} else {
			$warning = "<div class='alert alert-danger'>Error al editar la información</div>";
		}
	}

	if ($_POST["editar"] == "habitaciones") {
		for ($j = 0; $j < $_POST['cantidad_habitaciones']; $j++) {
			$habitacion_cantidad = $_POST['hab_cantidad_id' . $j];
			$habitacion_tipo = $_POST['hab_tipo_id' . $j];
			$habitacion_precio = $_POST['hab_precio_id' . $j];
			$habitacion_capacidad = $_POST['hab_capacidad_id' . $j];
			$habitacion_descuento = $_POST['hab_descuento_id' . $j];
			$hab_id = $_POST['hab_id' . $j];

			$sql = "UPDATE hoteles_habitaciones SET tipo='$habitacion_tipo', disponibles=$habitacion_cantidad, precio=$habitacion_precio, capacidad=$habitacion_capacidad, descuento=$habitacion_descuento WHERE id = $hab_id";

			$query = mysqli_query($enlace, $sql);

			if (!$query) {
				$warning = "<div class='alert alert-danger'>Error al editar las habitaciones del hotel</div>";
				break;
			} else {
				$warning = "<div class='alert alert-success'>Habitaciones editadas correctamente</div>";
			}
		}
	}

	if ($_POST["editar"] == "imagenes") {
		for ($j = 0; $j < $_POST['cantidad_imagenes']; $j++) {
			$habitacion_tipo = $_POST['img_tipo_id' . $j];
			$img_id = $_POST['img_id' . $j];

			$sql = "UPDATE hoteles_img SET habitacion='$habitacion_tipo' WHERE id=$img_id";

			$query = mysqli_query($enlace, $sql);

			if (!$query) {
				$warning = "<div class='alert alert-danger'>Error al editar las imagenes del hotel</div>";
				break;
			} else {
				$warning = "<div class='alert alert-success'>Imagenes editadas correctamente</div>";
			}
		}
	}
}

if (isset($_POST['eliminar_habitacion'])) {
	include("templates/admin/conectar_bd_admin.php");
	$id = $_POST['eliminar_habitacion'];
	$sql = "DELETE FROM hoteles_habitaciones WHERE id=$id";
	$query = mysqli_query($enlace, $sql);
	if ($query) {
		$warning = "<div class='alert alert-success'>Habitación eliminada correctamente</div>";
	} else {
		$warning = "<div class='alert alert-danger'>Error al eliminar la habitación</div>";
	}
}

if (isset($_POST['eliminar_imagen'])) {
	include("templates/admin/conectar_bd_admin.php");
	$id = $_POST['eliminar_imagen'];
	$sql = "DELETE FROM hoteles_img WHERE id=$id";
	$query = mysqli_query($enlace, $sql);
	if ($query) {
		$warning = "<div class='alert alert-success'>Imagen eliminada correctamente</div>";
	} else {
		$warning = "<div class='alert alert-danger'>Error al eliminar la imagen</div>";
	}
}

if (isset($_POST['anadir'])) {
	include("templates/admin/conectar_bd_admin.php");

	$habitacion_cantidad = $_POST['hab_cantidad'];
	$habitacion_tipo = $_POST['hab_tipo'];
	$habitacion_precio = $_POST['hab_precio'];
	$habitacion_capacidad = $_POST['hab_capacidad'];
	$habitacion_descuento = $_POST['hab_descuento'];

	$sql = "INSERT INTO hoteles_habitaciones (hotel_id, tipo, disponibles, precio, capacidad, descuento) VALUES ($hotel_id, '$habitacion_tipo', $habitacion_cantidad, $habitacion_precio, $habitacion_capacidad, $habitacion_descuento)";
	$query = mysqli_query($enlace, $sql);

	if ($query) {
		$warning = "<div class='alert alert-success'>Habitación añadida correctamente</div>";
	} else {
		$warning = "<div class='alert alert-danger'>Error al añadir la habitación</div>";
	}
}

if (isset($_POST['subir'])) {
	$hash = bin2hex(random_bytes(16));

	$archivo_nombre = basename($_FILES["imagen_upload"]["name"]);
	$archivo_tipo = strtolower(pathinfo($archivo_nombre, PATHINFO_EXTENSION));

	$archivo_nombre = $hash . "." . $archivo_tipo;

	$archivo_carpeta = "assets/img/hotel/" . $hotel_id . "/";
	if (!file_exists($archivo_carpeta)) {
		mkdir($archivo_carpeta, 0777, true);
	}
	$archivo_ruta = $archivo_carpeta . $archivo_nombre;

	$habitacion = $_POST['habitacion'];

	$uploadOk = true;

	// Check if image file is a actual image or fake image
	if (isset($_POST["submit"])) {
		$check = getimagesize($_FILES["imagen_upload"]["tmp_name"]);
		if ($check) {
			$warning .= "<div class='alert alert-danger' role='alert'>El archivo no es una imagen.</div>";
			$uploadOk = false;
		}
	}

	// Check if file already exists
	if (file_exists($archivo_ruta)) {
		$warning .= "<div class='alert alert-danger' role='alert'>El archivo ya existe.</div>";
		$uploadOk = false;
	}

	// Check file size
	if ($_FILES["imagen_upload"]["size"] > 10 * 1000 * 1000) {
		$warning .= "<div class='alert alert-danger'>Fichero demasiado grande.</div>";
		$uploadOk = false;
	}

	// Allow certain file formats
	if ($archivo_tipo != "jpg" && $archivo_tipo != "png" && $archivo_tipo != "jpeg" && $archivo_tipo != "gif" && $archivo_tipo != "webp") {
		$warning .= "<div class='alert alert-danger'>Solo se permiten archivos JPG, JPEG, PNG, GIF o WEBP.</div>";
		$uploadOk = false;
	}

	// Check if $uploadOk is set to 0 by an error
	if (!$uploadOk) {
		$warning .= "<div class='alert alert-danger'>El archivo no ha sido subido.</div>";
		// if everything is ok, try to upload file
	} else {
		if (move_uploaded_file($_FILES["imagen_upload"]["tmp_name"], $archivo_ruta)) {
			include("templates/admin/conectar_bd_admin.php");
			$sql = "INSERT INTO hoteles_img (hotel_id, img, habitacion) VALUES ('$hotel_id', '$archivo_ruta', '$habitacion')";
			$query = mysqli_query($enlace, $sql);
			if (!$query) {
				$warning .= "<div class='alert alert-danger' role='alert'>Error al insertar en la tabla.</div>";
			} else {
				$warning .= "<div class='alert alert-success' role='alert'>Imagen subida correctamente.</div>";
			}
		} else {
			$warning .= "<div class='alert alert-danger' role='alert'>Error al subir la imagen.</div>";
		}
	}
}

$sql = "SELECT * FROM hoteles WHERE id = $hotel_id";
$query = mysqli_query($enlace, $sql);
$row = mysqli_fetch_array($query);
?>

<div class="col-12">
	<?= $warning; ?>
	<h1 class="display-4">Editando: <?= $row['nombre']; ?> (<?= $row["id"] ?>)</h1>
	<hr>
</div>
<div class="col-12">
	<form action="#" method="post" class="mb-0">
		<div class="row">
			<div class="mb-3">
				<h1 class="display-5">Información</h1>
			</div>

			<div class="mb-3">
				<label for="nombre">Nombre</label>
				<input type="text" class="form-control" id="nombre" name="nombre" placeholder="<?= $row['nombre']; ?>" value="<?= $row['nombre']; ?>" required />
			</div>
			<div class="mb-3">
				<label for="descripcion">Descripción</label>
				<textarea class="form-control" id="descripcion" name="descripcion" rows="3" placeholder="<?= $row['descripcion']; ?>" required><?= $row['descripcion']; ?></textarea>
			</div>
			<div class="mb-3 col-md-3">
				<label for="direccion">Dirección</label>
				<input type="text" class="form-control" id="direccion" name="direccion" placeholder="<?= $row['direccion']; ?>" value="<?= $row['direccion']; ?>" required />
			</div>
			<div class="mb-3 col-md-3">
				<label for="ciudad">Ciudad</label>
				<input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="<?= $row['ciudad']; ?>" value="<?= $row['ciudad']; ?>" required />
			</div>
			<div class="mb-3 col-md-3">
				<label for="pais">País</label>
				<input type="text" class="form-control" id="pais" name="pais" placeholder="<?= $row['pais']; ?>" value="<?= $row['pais']; ?>" required />
			</div>
			<div class="mb-3 col-md-3">
				<label for="localizacion">Localización</label>
				<select class="form-select" id="localizacion" name="localizacion" required>
					<option value="1" <?= ($row["localizacion"] == "playa") ? " selected" : ""; ?>>Playa</option>
					<option value="2" <?= ($row["localizacion"] == "ciudad") ? " selected" : ""; ?>>Ciudad</option>
					<option value="3" <?= ($row["localizacion"] == "montaña") ? " selected" : ""; ?>>Montaña</option>
					<option value="4" <?= ($row["localizacion"] == "campo") ? " selected" : ""; ?>>Campo</option>
				</select>
			</div>
			<div class="mb-3">
				<label for="estrellas">Estrellas</label>
				<select class="form-select" id="estrellas" name="estrellas" required>
					<option value="1" <?= ($row["estrellas"] == 1) ? " selected" : ""; ?>>1</option>
					<option value="2" <?= ($row["estrellas"] == 2) ? " selected" : ""; ?>>2</option>
					<option value="3" <?= ($row["estrellas"] == 3) ? " selected" : ""; ?>>3</option>
					<option value="4" <?= ($row["estrellas"] == 4) ? " selected" : ""; ?>>4</option>
					<option value="5" <?= ($row["estrellas"] == 5) ? " selected" : ""; ?>>5</option>
				</select>
			</div>

			<div>
				<button type="submit" class="btn btn-primary" name="editar" value="informacion">Editar información</button>
				<hr>
			</div>
		</div>
	</form>

	<form action="#" method="post" class="mb-0">
		<div class="row">
			<div class="mb-3">
				<h1 class="display-5">Habitaciones</h1>
			</div>

			<?php
			$sql = "SELECT * FROM hoteles_habitaciones WHERE hotel_id = $hotel_id";
			$query = mysqli_query($enlace, $sql);
			$i = 0;
			if (mysqli_num_rows($query) == 0) {
			?>
				<div class="mb-3">
					<div class="alert alert-warning" role="alert">No hay habitaciones en este hotel.</div>
				</div>
				<?php
			} else {
				while ($row = mysqli_fetch_array($query)) {
				?>

					<div class="mb-3 col-sm-12 col-md-2 col-lg-3">
						<label class="form-label">Tipo</label>
						<input type="text" class="form-control" name="hab_tipo_id<?= $i ?>" placeholder="<?= $row['tipo']; ?>" value="<?= $row['tipo']; ?>" required />
					</div>
					<div class="mb-3 col-sm-3 col-md-2">
						<label class="form-label">Cantidad</label>
						<input type="number" class="form-control" name="hab_cantidad_id<?= $i; ?>" min="0" placeholder="<?= $row['disponibles']; ?>" value="<?= $row['disponibles']; ?>" required />
					</div>
					<div class="mb-3 col-sm-3 col-md-2">
						<label class="form-label">Precio</label>
						<input type="text" class="form-control" name="hab_precio_id<?= $i ?>" min="0" placeholder="<?= $row['precio']; ?>" value="<?= $row['precio']; ?>" required />
					</div>
					<div class="mb-3 col-sm-3 col-md-2">
						<label class="form-label">Capacidad</label>
						<input type="number" class="form-control" name="hab_capacidad_id<?= $i ?>" min="0" placeholder="<?= $row['capacidad']; ?>" value="<?= $row['capacidad']; ?>" required />
					</div>
					<div class="mb-3 col-sm-3 col-md-2">
						<label class="form-label">Descuento</label>
						<input type="number" class="form-control" name="hab_descuento_id<?= $i ?>" min="0" max="100" placeholder="<?= $row['descuento']; ?>" value="<?= $row['descuento']; ?>" required />
					</div>
					<div class="mb-3 col-sm-3 col-md-2 col-lg-1 d-flex align-items-end">
						<button type="button" class="btn btn-danger btn-lg w-100" data-bs-toggle="modal" data-bs-target="#confirmar_hab<?= $row["id"] ?>"><i class="bi bi-trash-fill"></i></button>
						<div class="modal fade" id="confirmar_hab<?= $row["id"] ?>" tabindex="-1" aria-labelledby="modallabel_hab<?= $row["id"] ?>" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="modallabel_hab<?= $row["id"] ?>">Confirma que quieres eliminarlo</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
										<button type="submit" name="eliminar_habitacion" value="<?= $row["id"] ?>" class="btn btn-outline-danger">Borrar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" name="hab_id<?= $i ?>" value="<?= $row["id"] ?>">
					<div>
						<hr class="d-block d-md-none">
					</div>
			<?php
					$i++;
				}
			}
			?>

			<input type="hidden" name="cantidad_habitaciones" value="<?= $i ?>" />

			<div>
				<?php
				if ($i > 0) {
				?>
					<button type="submit" class="btn btn-primary" name="editar" value="habitaciones">Editar habitaciones</button>
				<?php
				}
				?>
				<hr>
			</div>
		</div>
	</form>

	<form action="#" method="post">
		<div class="row">
			<div class="mb-3">
				<h1 class="display-5">Añadir habitación</h1>
			</div>

			<div class="mb-3 col-sm-12 col-md-4">
				<label class="form-label">Tipo</label>
				<input type="text" class="form-control" name="hab_tipo" required />
			</div>
			<div class="mb-3 col-sm-3 col-md-2">
				<label class="form-label">Cantidad</label>
				<input type="number" class="form-control" name="hab_cantidad" min="0" required />
			</div>
			<div class="mb-3 col-sm-3 col-md-2">
				<label class="form-label">Precio</label>
				<input type="text" class="form-control" name="hab_precio" min="0" required />
			</div>
			<div class="mb-3 col-sm-3 col-md-2">
				<label class="form-label">Capacidad</label>
				<input type="number" class="form-control" name="hab_capacidad" min="0" required />
			</div>
			<div class="mb-3 col-sm-3 col-md-2">
				<label class="form-label">Descuento</label>
				<input type="number" class="form-control" name="hab_descuento" min="0" max="100" required />
			</div>
			<div>
				<button type="submit" class="btn btn-primary" name="anadir">Añadir</button>
				<hr>
			</div>
		</div>
	</form>

	<form action="#" method="post" class="mb-0">
		<div class="row">
			<div class="mb-3">
				<h1 class="display-5">Imágenes</h1>
			</div>

			<div class="col-12">
				<?php
				$sql = "SELECT * FROM hoteles_img WHERE hotel_id = $hotel_id";
				$query = mysqli_query($enlace, $sql);
				$i = 0;
				if (mysqli_num_rows($query) == 0) {
				?>
					<div class="mb-3">
						<div class="alert alert-warning" role="alert">No hay imágenes para este hotel.</div>
					</div>
				<?php
				} else {
				?>
					<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-1">
						<?php
						while ($row = mysqli_fetch_assoc($query)) {
							$id = $row["id"];
							$img = $row["img"];
							$habitacion = $row["habitacion"];
						?>
							<div class="col">
								<div class="card h-100">
									<img class="card-img-top" src="<?= $img; ?>">
									<div class="card-body">
										<div class="input-group">
											<span class="input-group-text">Tipo</span>
											<input type="text" class="form-control" name="img_tipo_id<?= $i ?>" placeholder="<?= $row['habitacion']; ?>" value="<?= $row['habitacion']; ?>" />
											<button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#confirmar_img<?= $row["id"] ?>"><i class="bi bi-trash-fill"></i></button>
										</div>
										<div class="modal fade" id="confirmar_img<?= $row["id"] ?>" tabindex="-1" aria-labelledby="modallabel_img<?= $row["id"] ?>" aria-hidden="true">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="modallabel_img<?= $row["id"] ?>">Confirma que quieres eliminarlo</h5>
														<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
														<button type="submit" name="eliminar_imagen" value="<?= $row["id"] ?>" class="btn btn-outline-danger">Borrar</button>
													</div>
												</div>
											</div>
										</div>
										<input type="hidden" name="img_id<?= $i ?>" value="<?= $row["id"] ?>">
									</div>
								</div>
							</div>
						<?php
							$i++;
						}
						?>
					</div>
				<?php
				}
				?>
			</div>

			<input type="hidden" name="cantidad_imagenes" value="<?= $i ?>" />

			<div>
				<?php
				if ($i > 0) {
				?>
					<button type="submit" class="btn btn-primary mt-3" name="editar" value="imagenes">Editar imágenes</button>
				<?php
				}
				?>
				<hr>
			</div>
		</div>
	</form>

	<form action="#" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="mb-3">
				<h1 class="display-5">Subir imagen</h1>
			</div>

			<div class="mb-3">
				<label for="imagen_upload" class="form-label">Subir imagen</label>
				<div class="input-group">
					<input class="form-control" type="file" id="imagen_upload" name="imagen_upload">
					<span class="input-group-text" for="habitacion">Tipo</span>
					<input class="form-control" type="text" id="habitacion" name="habitacion">
					<button type="submit" class="btn btn-primary" name="subir">Subir</button>
				</div>
			</div>
		</div>
	</form>
</div>