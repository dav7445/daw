<div class="col-12">
	<h1 class="display-4">Hoteles</h1>
	<hr>
</div>

<div class="col-12">
	<a href="ajustes.php?tab=admin_hoteles&accion=insertar" class="btn btn-outline-primary">Insertar Hotel</a>
</div>

<div class="col-12 mt-2">
	<?php
	$sql = "SELECT * FROM hoteles";
	$query = mysqli_query($enlace, $sql);
	while ($fila = mysqli_fetch_array($query)) {
	?>
		<div class="card mb-4">
			<div class="card-body">
				<h5 class="card-title"><?= $fila['nombre'] ?></h5>
				<p class="card-text"><?= $fila['descripcion'] ?></p>
				<a href="ajustes.php?tab=admin_hoteles&id=<?= $fila['id'] ?>&accion=editar" class="btn btn-primary">Editar</a>
				<a href="ajustes.php?tab=admin_hoteles&id=<?= $fila['id'] ?>&accion=borrar" class="btn btn-outline-danger">Borrar</a>
			</div>
		</div>
	<?php
	}
	?>
</div>