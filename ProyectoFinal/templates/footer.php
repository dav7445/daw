<footer class="border-top mt-4 bg-light py-md-4">
	<div class="container">
		<div class="d-flex flex-wrap justify-content-between align-items-center py-3">
			<p class="col-12 col-xl-4 mb-0 text-center text-xl-start text-muted">&copy; 2022 AsturViajes S.L.</p>
			<a href="index.php" class="d-none d-xl-flex link-dark text-decoration-none">AsturViajes</a>
			<ul class="nav col-12 col-xl-4 justify-content-center justify-content-xl-end">
				<li><a href="index.php" class="nav-link px-2 link-secondary">Inicio</a></li>
				<li><a href="destinos.php" class="nav-link px-2 link-secondary">Destinos</a></li>
				<li><a href="plans.php" class="nav-link px-2 link-secondary">Premium</a></li>
			</ul>
		</div>
	</div>
</footer>
