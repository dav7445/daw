<?php include("templates/head.php"); ?>

<?php
session_start();
include("templates/conectar_bd_consultor.php");

$hotel_id = $_GET["id"];

$warning = "";

$entrada = $salida = $adultos = $ninos = "";

/* if get accion is reservar */
if (isset($_POST["reservar"])) {

	/* calculate amount of nights based on entrada and salida dates */
	$entrada = $_POST["entrada"];
	$salida = $_POST["salida"];
	$noches = (strtotime($salida) - strtotime($entrada)) / (60 * 60 * 24);

	$adultos = $_POST["adultos"];
	$ninos = $_POST["ninos"];

	$id_habitacion = $_POST["reservar"];

	/* comprobar capacidad de la habitacion */
	$sql = "SELECT capacidad FROM hoteles_habitaciones WHERE id = $id_habitacion";
	$query = mysqli_query($enlace, $sql);
	$row = mysqli_fetch_array($query);
	$capacidad = $row["capacidad"];

	if ($noches <= 0) {
		$warning = "<div class='alert alert-danger' role='alert'><strong>¡Error!</strong> La fecha de salida debe ser posterior a la de entrada.</div>";

	} else if ($noches > 30) {
		$warning = "<div class='alert alert-danger' role='alert'><strong>¡Error!</strong> No puedes reservar por más de 30 días.</div>";

	} else if ($_POST["adultos"] + $_POST["ninos"] == 0) {
		$warning = "<div class='alert alert-danger' role='alert'><strong>¡Error!</strong> No puedes reservar 0 niños y 0 adultos.</div>";

	} else if ($_POST["adultos"] + $_POST["ninos"] > $capacidad) {
		$warning = "<div class='alert alert-danger' role='alert'><strong>¡Error!</strong> No puedes reservar más de " . $capacidad . " personas en esta habitación.</div>";

	} else {
		// comprueba los dias que tienen habitaciones libres (base de datos)
		$sql = "SELECT disponibles AS habitaciones_disponibles FROM hoteles_habitaciones WHERE id = $id_habitacion";
		$query = mysqli_query($enlace, $sql);
		$row = mysqli_fetch_array($query);
		$habitaciones_disponibles = intval($row["habitaciones_disponibles"]);

		$sql = "SELECT entrada, salida FROM hoteles_reservas WHERE habitacion_id = '$id_habitacion' AND cancelada = 0";
		$query = mysqli_query($enlace, $sql);
		$reservas = array();

		while ($row = mysqli_fetch_assoc($query)) {
			$reservas[] = $row;
		}

		if (!isset($_SESSION["cart"])) {
			$_SESSION["cart"] = array();
			$id_compra = 0;
		} else {
			$id_compra = count($_SESSION["cart"]);
		}

		/* push cart reservas to $reservas */
		foreach ($_SESSION["cart"] as $item) {
			if ($item["id"] != "premium") {
				$reserva_en_carrito = array(
					"entrada" => $item["fecha_entrada"],
					"salida" => $item["fecha_salida"]
				);
				$reservas[] = $reserva_en_carrito;
			}
		}

		$dias_reservados = array();
		$dia = $entrada;

		for ($i = 0; $i < $noches; $i++) {
			array_push($dias_reservados, [$habitaciones_disponibles, $dia]);
			$dia = date("Y-m-d", strtotime($dia . "+1 day"));
		}

		$todosDiasLibres = true;
		$diasNoLibres = array();
		for ($i = 0; $i < $noches; $i++) {
			foreach ($reservas as $reserva) {
				if (strtotime($dias_reservados[$i][1]) >= strtotime($reserva["entrada"]) && strtotime($dias_reservados[$i][1]) < strtotime($reserva["salida"])) {
					$dias_reservados[$i][0] -= 1;
					if ($dias_reservados[$i][0] == 0) {
						$todosDiasLibres = false;
						array_push($diasNoLibres, $dias_reservados[$i][1]);
					}
				}
			}
		}

		if (!$todosDiasLibres) {
			$warning = "<div class='alert alert-danger' role='alert'><strong>¡Error!</strong> No hay suficientes habitaciones libres en las siguientes fechas: ";
			for ($i = 0; $i < count($diasNoLibres); $i++) {
				$warning .= $diasNoLibres[$i];
				if ($i != count($diasNoLibres) - 1) {
					$warning .= ", ";
				}
			}
			$warning .= "</div>";
		} else {
			$sql = "SELECT * FROM hoteles, hoteles_habitaciones WHERE hoteles_habitaciones.hotel_id = hoteles.id AND hoteles_habitaciones.id = $id_habitacion";
			$query = mysqli_query($enlace, $sql);
			$row = mysqli_fetch_assoc($query);
			$name = $row["nombre"];
			$description = $row["descripcion"];
			$precio = $row["precio"];
			$descuento = $row["descuento"];

			$precio = $precio * $noches;
			$precio = $precio - ($precio * $descuento / 100);

			$reserva = array(
				"id_compra" => $id_compra,
				"id_producto" => $hotel_id,
				"nombre" => $name,
				"descripcion" => $description,
				"habitacion" => $id_habitacion,
				"precio" => $precio,
				"fecha_entrada" => $entrada,
				"fecha_salida" => $salida,
				"noches" => $noches,
				"adultos" => $adultos,
				"ninos" => $ninos
			);

			array_push($_SESSION["cart"], $reserva);

			header("Location: compra.php");
		}
	}
}
?>

<body>
	<div class="position-relative min-vh-100 d-flex flex-column justify-content-between">

		<?php include("templates/header.php"); ?>

		<div class="container py-3">
			<div class="row">
				<?php
				$sql = "SELECT * FROM hoteles WHERE id = $hotel_id";
				$query = mysqli_query($enlace, $sql);
				$row = mysqli_fetch_assoc($query);
				$name = $row["nombre"];
				$description = $row["descripcion"];
				$stars = $row["estrellas"];
				$hotel_premium = $row["premium"];
				?>
				<div class="col-12">
					<?php
					$sql = "SELECT * FROM hoteles_img WHERE hotel_id = $hotel_id";
					$query_img = mysqli_query($enlace, $sql);
					if (mysqli_num_rows($query_img) > 0) {
					?>
						<div id="carousel" class="carousel slide" data-bs-ride="carousel">
							<div class="carousel-inner rounded-3">
								<?php
								$i = 0;
								while ($row2 = mysqli_fetch_assoc($query_img)) {
									$img = $row2["img"];
								?>
									<div class="carousel-item<?= ($i == 0) ? ' active' : ''; ?>" style="height: 500px">
										<img class="d-block w-100 h-100 card-img-top" src="<?= $img; ?>">
									</div>
								<?php
									$i++;
								}
								?>
							</div>
							<button class="carousel-control-prev gradient-left rounded-3" type="button" data-bs-target="#carousel" data-bs-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="visually-hidden">Anterior</span>
							</button>
							<button class="carousel-control-next gradient-right rounded-3" type="button" data-bs-target="#carousel" data-bs-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="visually-hidden">Siguiente</span>
							</button>
						</div>
					<?php
					}
					?>
				</div>

				<div class="col-12">
					<!-- div with spaced between elements -->
					<div class="d-flex justify-content-between">
						<h1 class="display-4"><?= $name; ?></h1>
						<div class="d-flex align-items-end">
							<?php
							if (isset($_SESSION["rol"]) && $_SESSION["rol"] == 2) {
								/* get current url */
								$url = $_SERVER["REQUEST_URI"];
							?>
								<a href='ajustes.php?tab=admin_hoteles&id=<?= $hotel_id; ?>&accion=editar' class='btn btn-outline-primary'>Editar</a>
							<?php
							}
							?>
						</div>
					</div>

					<?php
					if ($hotel_premium) {
					?>
						<h4 class="mb-2"><span class='badge bg-primary'>Premium</span></h4>
					<?php
					}
					?>
					<div>
						<?php
						for ($i = 0; $i < $stars; $i++) {
						?>
							<i class="bi bi-star-fill"></i>
						<?php
						}
						?>
					</div>
					<p class="lead mb-0"><?= $description; ?></p>
				</div>

				<div class="col-12">
					<hr>
					<?= $warning; ?>
					<?php
					$array_reservas = array();

					/* if the user already has a reserve in this hotel, show it */
					if (isset($_SESSION["cart"])) {
						foreach ($_SESSION["cart"] as $reserva) {
							if ($reserva["id_producto"] == $hotel_id) {
								$sql = "SELECT tipo FROM hoteles_habitaciones WHERE id = " . $reserva["habitacion"];
								$query = mysqli_query($enlace, $sql);
								$row = mysqli_fetch_array($query);
								$tipo = $row["tipo"];
								array_push($array_reservas, array(
									"entrada" => $reserva["fecha_entrada"],
									"salida" => $reserva["fecha_salida"],
									"adultos" => $reserva["adultos"],
									"ninos" => $reserva["ninos"],
									"tipo" => $tipo,
									"carrito" => true,
									"cancelada" => false
								));
							}
						}
					}
					if (isset($_SESSION["email"])) {
						/* if the user has reservas in this hotel, show them */
						$sql = "SELECT habitacion_id, entrada, salida, adultos, ninos, cancelada, hoteles_habitaciones.tipo FROM hoteles_reservas, hoteles_habitaciones WHERE hoteles_habitaciones.id = hoteles_reservas.habitacion_id AND hoteles_reservas.hotel_id = " . $hotel_id . " AND email = '" . $_SESSION["email"] . "' GROUP BY hoteles_reservas.id";
						$query = mysqli_query($enlace, $sql);
						if (mysqli_num_rows($query) > 0) {
							while ($row = mysqli_fetch_array($query)) {
								$habitacion_id = $row["habitacion_id"];
								$entrada = $row["entrada"];
								$salida = $row["salida"];
								$adultos = $row["adultos"];
								$ninos = $row["ninos"];
								$cancelada = $row["cancelada"];
								$tipo = $row["tipo"];

								array_push($array_reservas, array(
									"entrada" => $entrada,
									"salida" => $salida,
									"adultos" => $adultos,
									"ninos" => $ninos,
									"tipo" => $tipo,
									"carrito" => false,
									"cancelada" => $cancelada
								));
							}
						}
					}
					if (count($array_reservas) > 0) {
					?>
						<h4 class="mb-2">Reservas en este hotel</h4>
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th scope="col">Entrada</th>
									<th scope="col">Salida</th>
									<th scope="col">Adultos</th>
									<th scope="col">Niños</th>
									<th scope="col">Tipo</th>
									<th scope="col">Estado</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach ($array_reservas as $reserva) {
								?>
									<tr class="<?= ($reserva['cancelada']) ? 'text-secondary' : '' ?>">
										<td><?= $reserva["entrada"]; ?></td>
										<td><?= $reserva["salida"]; ?></td>
										<td><?= $reserva["adultos"]; ?></td>
										<td><?= $reserva["ninos"]; ?></td>
										<td><?= ucfirst($reserva["tipo"]); ?></td>
										<td><?php
											if ($reserva["carrito"]) {
												echo "En carrito";
											} else {
												if ($reserva["cancelada"]) {
													echo "Cancelada";
												} else {
													echo "Confirmada";
												}
											}
											?>
										</td>
									</tr>
								<?php
								}
								?>
							</tbody>
						</table>

						<hr>
					<?php
					}
					?>
				</div>

				<form action="hotel.php?accion=reservar&id=<?= $hotel_id; ?>" method="POST">
					<div class="col-12">

						<div class="row">
							<div class="mb-3">
								<div class="input-group">
									<label class="input-group-text" for="entrada">Entrada</label>
									<input type="date" class="form-control" id="entrada" name="entrada" min="<?= date("Y-m-d") ?>" value="<?= $entrada ?>" required>

									<label class="input-group-text" for="salida">Salida</label>
									<input type="date" class="form-control" id="salida" name="salida" min="<?= date("Y-m-d", strtotime("+1 day")) ?>" value="<?= $salida ?>" required>
								</div>
							</div>

							<div class="mb-3">
								<div class="input-group">
									<label class="input-group-text" for="adultos">Adultos</label>
									<input type="number" class="form-control" id="adultos" name="adultos" min="0" max="20" placeholder="0" value="<?= $adultos ?>" required>

									<label class="input-group-text" for="ninos">Niños</label>
									<input type="number" class="form-control" id="ninos" name="ninos" min="0" max="20" placeholder="0" value="<?= $ninos ?>" required>
								</div>
							</div>
						</div>

					</div>

					<div class="col-12 row row-cols-1 row-cols-sm-2 row-cols-md-3 g-1">
						<?php
						$sql = "SELECT * FROM hoteles_habitaciones WHERE hotel_id = $hotel_id ORDER BY precio ASC";
						$query_habitaciones = mysqli_query($enlace, $sql);
						while ($row = mysqli_fetch_assoc($query_habitaciones)) {
							$habitacion_id = $row["id"];
							$disponibles = $row["disponibles"];
							$precio = $row["precio"];
							$tipo = $row["tipo"];
							$capacidad = $row["capacidad"];
							$descuento = $row["descuento"];
							$puede_reservar = true;
							if (($hotel_premium && (!isset($_SESSION["rol"]) || $_SESSION["rol"] != 1))) {
								$puede_reservar = false;
							}
							if ($disponibles > 0) {
						?>
								<div class="col">
									<div class="card h-100">
										<?php
										$sql = "SELECT * FROM hoteles_img WHERE hotel_id = $hotel_id AND habitacion = '$tipo'";
										$query_img = mysqli_query($enlace, $sql);
										$row2 = mysqli_fetch_assoc($query_img);
										if (empty($row2)) {
											$img = "assets/img/imagen_no_disponible.png";
										} else {
											$img = $row2["img"];
										}
										?>
										<img class="card-img-top" src="<?= $img; ?>">
										<div class="card-body">
											<h5 class="card-title mb-0"><?= ucfirst($tipo) ?></h5>
										</div>
										<ul class="list-group list-group-flush">
											<li class="list-group-item"><i class="bi bi-people-fill me-2"></i><?= $capacidad ?> huésped/es</li>
										</ul>
										<div class="card-body">
											<div class="card-text">
												<div class="d-flex justify-content-between flex-wrap">
													<?php
													if ($descuento > 0) {
													?>
														<p class="fs-4 lead d-flex align-items-center mb-0">
															<small class="text-decoration-line-through me-1"><?= $precio ?>€</small>
															<span class="fw-bold"><?= $precio - ($precio * $descuento / 100) ?>€</span><small>/noche</small>
														</p>
													<?php
													} else {
													?>
														<p class="fs-4 lead d-flex align-items-center mb-0"><span class="fw-bold"><?= $precio; ?>€</span><small>/noche</small></p>
													<?php
													}
													if ($puede_reservar) {
													?>
														<button type="submit" name="reservar" value="<?= $habitacion_id ?>" class="btn btn-primary">Reservar</button>
													<?php
													} else {
													?>
														<p class="text-danger d-flex align-items-center mb-0">Premium</p>
													<?php
													}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
						<?php
							}
						}
						?>
					</div>
				</form>
			</div>
		</div>

		<?php include("templates/footer.php"); ?>
	</div>
</body>