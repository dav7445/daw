<?php include("templates/head.php"); ?>

<?php
session_start();
include("templates/conectar_bd_consultor.php");

include("templates/user_details.php");

if (!isset($_SESSION["cart"])) {
	$_SESSION["cart"] = array();
	$id_compra = 0;
} else {
	$id_compra = count($_SESSION["cart"]);
}

/* if submit */
if (isset($_POST["premium"])) {
	/* push array to cart array */
	array_push($_SESSION["cart"], array(
		"id_compra" => $id_compra,
		"id_producto" => "premium",
		"tipo" => "premium",
		"precio" => "19",
		"nombre" => "Premium",
		"descripcion" => "Convertirse en usuario Premium"
	));
	header("Location: compra.php");
}
?>

<body>
	<div class="position-relative min-vh-100 d-flex flex-column justify-content-between">

		<?php include("templates/header.php"); ?>

		<div class="container py-3">
			<?php
			/* if the user is not logged in show an alert */
			if (!isset($_SESSION["email"])) {
			?>
				<div class='alert alert-warning' role='alert'><strong>¡Atención!</strong> Debes <a class="alert-link" href='signin.php'>iniciar sesión</a> antes de poder convertirte en usuario premium.</div>
			<?php
			}
			?>
			<div class="row row-cols-1 row-cols-md-2 g-3 mb-3 text-center">

				<div class="col">
					<div class="card rounded-3 h-100">
						<div class="card-header py-3">
							<h2 class="my-0 fw-normal">Gratis</h2>
						</div>
						<div class="card-body d-flex flex-column">
							<h1 class="card-title">0€</h1>
							<ul class="list-unstyled mt-3">
								<li>Ciudades más famosas</li>
								<li>Cientos de Hoteles y apartamentos</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="card rounded-3 shadow border-primary h-100">
						<div class="card-header py-3 text-white bg-primary border-primary">
							<h2 class="my-0 fw-normal">Premium</h2>
						</div>
						<div class="card-body d-flex flex-column">
							<h1 class="card-title">19€<small class="text-muted fw-light">/año</small></h1>
							<ul class="list-unstyled mt-3">
								<li>Todo lo del plan gratuito</li>
								<li>Destinos exclusivos</li>
							</ul>
							<form action="#" method="post" class="mb-auto">
								<?php
								if (isset($_SESSION["email"])) {
									/* if the user is already premium (role = 2) then show cancel button if not show buy button */
									if ($_SESSION["rol"] == 1) {
								?>
										<a href="ajustes.php?tab=compras" class="w-100 btn btn-lg btn-outline-danger">Cancelar Premium</a>
										<?php
									} else {

										$yaencarrito = false;
										/* if the cart is not empty */
										if (count($_SESSION["cart"]) > 0) {
											/* if premium is alrady in the cart change button to link */
											foreach ($_SESSION["cart"] as $item) {
												if ($item["id_producto"] == "premium") {
													$yaencarrito = true;
													break;
												}
											}
										}
										if ($yaencarrito) {
										?>
											<a href="compra.php" class="w-100 btn btn-lg btn-outline-primary">Ir a la compra</a>
										<?php
										} else {
										?>
											<button type="submit" name="premium" class="w-100 btn btn-lg btn-primary">Comprar</button>
								<?php
										}
									}
								}
								?>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include("templates/footer.php"); ?>
	</div>
</body>