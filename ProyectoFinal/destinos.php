<?php include("templates/head.php"); ?>

<?php
session_start();
include("templates/conectar_bd_consultor.php");
?>

<?php
/* get filters from url if there are any, if not, set them */
/* nombre, ciudad, pais, estrellas */
$filtro_nombre = isset($_GET['nombre']) ? $_GET['nombre'] : '';
$filtro_ciudad = isset($_GET['ciudad']) ? $_GET['ciudad'] : '';
$filtro_pais = isset($_GET['pais']) ? $_GET['pais'] : '';
$filtro_localizacion = isset($_GET['localizacion']) ? $_GET['localizacion'] : '';
$filtro_estrellas = isset($_GET['estrellas']) ? $_GET['estrellas'] : '';

if (isset($_POST["buscar"])) {
	$url = "destinos.php?nombre=" . $_POST["nombre"] . "&ciudad=" . $_POST["ciudad"] . "&pais=" . $_POST["pais"] . "&localizacion=" . $_POST["localizacion"] . "&estrellas=" . $_POST["estrellas"];
	header("Location: $url");
}
?>

<body>
	<div class="position-relative min-vh-100 d-flex flex-column justify-content-between">

		<?php include("templates/header.php"); ?>

		<div class="container py-3">

			<div class="row">
				<div class="col-md-2"><?php include("templates/destinos_sidebar.php"); ?></div>
				<div class="col-md-10"><?php include("templates/destinos_reservas_hotel.php"); ?></div>
			</div>

		</div>

		<?php include("templates/footer.php"); ?>
	</div>
</body>