<?php include("templates/head.php"); ?>

<?php
/* if user is already logged in redirect to index */
session_start();
include("templates/signin_forbidden.php");
?>

<?php
$loginErr = "";
if (isset($_POST["submit"])) {
	include("templates/conectar_bd_consultor.php");

	$email = mysqli_real_escape_string($enlace, $_POST["email"]);
	$password = mysqli_real_escape_string($enlace, $_POST["password"]);

	$sql = "SELECT email, password, rol FROM usuarios WHERE email = '$email'";
	$query = mysqli_query($enlace, $sql);

	if (mysqli_num_rows($query) > 0) {
		$row = mysqli_fetch_array($query);
		if (password_verify($password, $row["password"])) {
			$_SESSION["email"] = $email;
			$_SESSION["rol"] = $row["rol"];
			header("Location: index.php");
		} else {
			$loginErr = "Contraseña incorrecta";
		}
	} else {
		$loginErr = "Email no registrado, <a class='alert-link' href='signup.php'>registrate</a>";
	}
	mysqli_close($enlace);
}
?>

<body>
	<div class="position-relative min-vh-100 d-flex flex-column justify-content-between">

		<?php include("templates/header_simple.php"); ?>

		<div class="container col-xl-10 col-xxl-8 px-md-4 py-5">
			<div class="row align-items-center g-lg-5 py-5">
				<div class="col-lg-7 text-center text-lg-start">
					<h1 class="display-4 fw-bold lh-1 mb-3">AsturViajes</h1>
					<p class="col-lg-10 fs-4">El placer de viajar</p>
				</div>
				<div class="col-md-10 mx-auto col-lg-5">
					<form class="p-4 p-md-5 mb-2 border rounded-3 bg-light needs-validation" method="post" novalidate>
						<div class="row gy-3">
							<div class="col-12">
								<label for="email" class="form-label">Email</label>
								<input type="email" class="form-control" id="email" name="email" placeholder="" required />
								<div class="invalid-feedback">Introduce un email valido</div>
							</div>

							<div class="col-12">
								<label for="password" class="form-label">Contraseña</label>
								<input type="password" class="form-control" id="password" name="password" placeholder="" min="8" required />
								<div class="invalid-feedback">Introduce una contraseña valida</div>
							</div>

							<div class="col-12">
								<?php
								if ($loginErr != "") {
								?>
									<div class="alert alert-danger" role="alert"><?= $loginErr ?></div>
								<?php
								}
								?>
								<button class="w-100 btn btn-lg btn-primary" type="submit" name="submit">Iniciar Sesión</button>
							</div>
						</div>
					</form>
					<p class="text-center">¿No tienes una cuenta? <a href="signup.php" class="text-decoration-none">Registrate</a></p>
				</div>
			</div>
		</div>

		<?php include("templates/footer.php"); ?>
	</div>
	<script src="assets/js/form-validation.js"></script>
</body>

</html>