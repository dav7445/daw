# DAW

## Horario

<table>
	<thead align="center">
		<tr>
			<th></th>
			<th>Lunes</th>
			<th>Martes</th>
			<th>Miercoles</th>
			<th>Jueves</th>
			<th>Viernes</th>
		</tr>
	</thead>
	<tbody align="center">
		<tr>
			<td>8:30 - 9:25</td>
			<td rowspan="3">Despliegue</td>
			<td rowspan="3">Cliente</td>
			<td rowspan="2">Interfaces</td>
			<td rowspan="2">Servidor</td>
			<td rowspan="2">Interfaces</td>
		</tr>
		<tr>
			<td>9:25 - 10:20</td>
		</tr>
		<tr>
			<td>10:20 - 11:15</td>
			<td>Despliegue</td>
			<td>Empresa</td>
			<td>Empresa</td>
		</tr>
		<tr>
			<td>11:15 - 11:45</td>
			<td colspan="5"></td>
		</tr>
		<tr>
			<td>11:45 - 12:40</td>
			<td rowspan="3">Servidor</td>
			<td rowspan="2">Interfaces</td>
			<td>Despliegue</td>
			<td>Empresa</td>
			<td rowspan="2">Servidor</td>
		</tr>
		<tr>
			<td>12:40 - 13:35</td>
			<td rowspan="2">Cliente</td>
			<td rowspan="2">Cliente</td>
		</tr>
		<tr>
			<td>13:35 - 14:30</td>
			<td>Empresa</td>
			<td>Proyecto</td>
		</tr>
	</tbody>
</table>
