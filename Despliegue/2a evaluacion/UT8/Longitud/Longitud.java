
public class Longitud {
	private String dorsal;
	private int marca2019;
	private int marca2020;
	private int marca2021;

	public Longitud() {
		super();
	}

	public Longitud(String dorsal, int marca2019, int marca2020, int marca2021) {
		super();
		this.dorsal = dorsal;
		this.marca2019 = marca2019;
		this.marca2020 = marca2020;
		this.marca2021 = marca2021;
	}

	public String getDorsal() {
		return dorsal;
	}

	public void setDorsal(String dorsal) {
		this.dorsal = dorsal;
	}

	public int getMarca2019() {
		return marca2019;
	}

	public void setMarca2019(int marca2019) {
		this.marca2019 = marca2019;
	}

	public int getMarca2020() {
		return marca2020;
	}

	public void setMarca2020(int marca2020) {
		this.marca2020 = marca2020;
	}

	public int getMarca2021() {
		return marca2021;
	}

	public void setMarca2021(int marca2021) {
		this.marca2021 = marca2021;
	}

	@Override
	public String toString() {
		return "Longitud [dorsal=" + dorsal + ", marca2019=" + marca2019 + ", marca2020=" + marca2020 + ", marca2021="
				+ marca2021 + "]";
	}
}
