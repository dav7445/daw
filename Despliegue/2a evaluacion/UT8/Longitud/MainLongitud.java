import java.util.Scanner;

public class MainLongitud {

	public static void main(String[] args) {
		menu();
	}

	public static void menu() {
		Scanner teclado = new Scanner(System.in);
		GestionLongitud longi = new GestionLongitud();
		longi.rellenar();
		int op;
		do {
			System.out.println("****MENU****");
			System.out.println("1-Visualizar");
			System.out.println("2-Actualizar");
			System.out.println("3-Mejor marca");
			System.out.println("4-Media marcas");
			System.out.println("5-Salir");
			System.out.print("Introduzca una opcion: ");
			op = teclado.nextInt();
			switch (op) {
				case 1:
					longi.visualizar();
					break;
				case 2:
					longi.actualizar();
					break;
				case 3:
					longi.mejormarca();
					break;
				case 4:
					longi.mediamarca();
					break;
				case 5:
					System.out.println("FIN OPERACIONES");
					break;
			}
		} while (op != 5);
	}
}
