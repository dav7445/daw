import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class GestionLongitud {
	ArrayList<Longitud> longi;

	public GestionLongitud() {
		longi = new ArrayList<Longitud>();
	}

	public void rellenar() {
		longi.add(new Longitud("101", 10, 12, 0));
		longi.add(new Longitud("121", 12, 14, 0));
		longi.add(new Longitud("141", 15, 17, 0));
		longi.add(new Longitud("161", 14, 16, 0));
		longi.add(new Longitud("181", 16, 18, 0));
	}

	public void visualizar() {
		for (Longitud lon : longi) {
			System.out.println(lon.toString());
			System.out.println();
		}
		System.out.println();
	}

	public void actualizar() {
		Scanner teclado = new Scanner(System.in);
		Iterator<Longitud> longt = longi.iterator();
		Longitud lo;

		String dorsal = " ";
		int marca2021, marca2020 = 0, marca2019 = 0;
		for (int i = 0; i < longi.size(); i++) {
			System.out.print("Introducir la marca 2021 " + (i + 1) + ": ");
			marca2021 = teclado.nextInt();
			teclado.nextLine();
			Longitud l = new Longitud(dorsal, marca2019, marca2020, marca2021);
			longi.get(i).setMarca2021(marca2021);
		}

	}

	public void mejormarca() {
		int mayor = -1, pos = 0;
		Iterator<Longitud> longt = longi.iterator();
		Longitud lo;
		while (longt.hasNext()) {
			lo = longt.next();
			if (lo.getMarca2019() > mayor) {
				mayor = lo.getMarca2019();
			}
			if (lo.getMarca2020() > mayor) {
				mayor = lo.getMarca2020();
			}
			if (lo.getMarca2021() > mayor) {
				mayor = lo.getMarca2021();
			}

			System.out.println("La mejor marca del dorsal " + lo.getDorsal() + " es " + mayor);
		}
	}

	public void mediamarca() {
		Scanner teclado = new Scanner(System.in);
		DecimalFormat df = new DecimalFormat("#.00");
		String dorsal;
		boolean dor = false;
		double media = 0;
		int pos = 0;
		System.out.print("Introducir dorsal del participante: ");
		dorsal = teclado.nextLine();
		for (int i = 0; i < longi.size(); i++) {
			if (longi.get(i).getDorsal().equalsIgnoreCase(dorsal)) {
				System.out.println("El dorsal introducido esta en la lista");
				dor = true;
				pos = i;
			}
		}

		if (dor == true) {
			double suma = 0;
			suma = longi.get(pos).getMarca2019() + longi.get(pos).getMarca2020() + longi.get(pos).getMarca2021();
			media = suma / 3;
			System.out.println(
					"La media de las marcas del dorsal " + longi.get(pos).getDorsal() + " es " + df.format(media));
		}

		else if (dor == false) {
			System.out.println("El dorsal introducido no existe");
		}
		System.out.println();
	}
}
