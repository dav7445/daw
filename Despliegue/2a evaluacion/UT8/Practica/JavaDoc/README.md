# DespliegueJavaDoc

[Repositorio](https://gitlab.com/dav7445/desplieguejavadoc/-/blob/main/README.md)

## Autor
David Velasco

## Biografía
Soy **David**, nací el _24 de Agosto del 2000_ y estudio en el CIFP de Avilés.

## Finalidad de la aplicación
Gestión, control y venta de productos.

## Tecnologías
Java

## Contenido
```
.
├──Articulo
|  ├──Articulo.java
|  ├──GestionArticulo.java
|  └──MainArticulo.java
└──JavaDoc Articulo
```
![image.png](./image.png)

## Finalidad del respositorio
Control de versiones de la aplicación.
